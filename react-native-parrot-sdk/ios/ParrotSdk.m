#import "ParrotSdk.h"
#import "react_native_parrot_sdk-Swift.h"

@interface ParrotSdk ()
@property (nonatomic) HelloDrone* helloDrone;

@end

@implementation ParrotSdk

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(sampleMethod:(NSString *)stringArgument numberParameter:(nonnull NSNumber *)numberArgument callback:(RCTResponseSenderBlock)callback)
{
    // TODO: Implement some actually useful functionality
	callback(@[[NSString stringWithFormat: @"numberArgument: %@ stringArgument: %@", numberArgument, stringArgument]]);
}

NSString * const onDroneModelEvent = @"onDroneModel";
NSString * const onConnectionEvent = @"onConnection";
NSString * const onGPSTelemetryEvent = @"onGPSTelemetry";
NSString * const onSpeedometerEvent = @"onSpeedometer";

- (NSArray<NSString *> *)supportedEvents
{
    return @[onDroneModelEvent, onGPSTelemetryEvent, onConnectionEvent, onSpeedometerEvent];
}

RCT_EXPORT_METHOD(start)
{
    self.helloDrone = [HelloDrone new];

    [self.helloDrone autoConnectionFacilityWithDroneModelCompletion:^(NSString * _Nonnull model) {
        [self sendEventWithName:onDroneModelEvent body:@{@"success": @YES, @"model": model}];
    } connectionCompletion:^(NSString * _Nonnull connectionStatus) {
        [self sendEventWithName:onConnectionEvent body:@{@"success": @YES, @"status": connectionStatus}];
    } gpsCompletion:^(double latitude, double longitude, double ) {
        [self sendEventWithName:onGPSTelemetryEvent body:@{@"success": @YES, @"latitude": @(latitude) , @"longitude": @(longitude) , @"": @()}];
        
    } speedometerCompletion:^(double velocityX, double velocityY, double velocityZ) {
        [self sendEventWithName:onSpeedometerEvent body:@{@"success": @YES, @"velocityX": @(velocityX) , @"velocityY": @(velocityY) , @"velocityZ": @(velocityZ)}];
    }];
}

RCT_EXPORT_METHOD(stop)
{
    [self.helloDrone stop];
}
@end
