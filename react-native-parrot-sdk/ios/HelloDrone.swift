//
//  HelloDrone.swift
//  ParrotSdk
//
//  Created by Joseph Rene on 2020-02-16.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation
// import GroundSdk library.
import GroundSdk

@objc(HelloDrone)
open class HelloDrone : NSObject{
    /// Ground SDk instance.
    private let groundSdk = GroundSdk()
    /// Reference to auto connection.
    private var autoConnectionRef: Ref<AutoConnection>?
    
    // Drone:
    /// Current drone instance.
    private var drone: Drone?
    
    /// Reference to the current drone state.
    private var droneStateRef: Ref<DeviceState>?
    
    /// Reference to the current drone battery info instrument.
    private var droneGPSRef: Ref<Gps>?
    
    private var droneSpeedometerRef : Ref<Speedometer>?
    
    private var connectionCompletion: ((_ connectionStatus:String) -> Void)?
    
    private var droneModelCompletion: ((_ model:String) -> Void)?
    
    private var gpsCompletion: ((_ latitude:Double, _ longitude:Double, _ altitude:Double) -> Void)?
    
    private var speedometerCompletion: ((_ velocityX:Double, _ velocityY:Double, _ velocityZ:Double) -> Void)?
    
    
    public func getVersion() ->String
    {
        return AppInfoCore.sdkVersion
    }
    
    @objc
    public func autoConnectionFacility(droneModelCompletion:@escaping ( _ model:String) -> Void ,connectionCompletion: @escaping ( _ connectionStatus:String) -> Void, gpsCompletion: @escaping (_ latitude:Double, _ longitude:Double, _ altitude:Double) -> Void, speedometerCompletion:  @escaping (_ velocityX:Double, _ velocityY:Double, _ velocityZ:Double) -> Void)
    {
        self.droneModelCompletion = droneModelCompletion;
        self.connectionCompletion = connectionCompletion;
        self.gpsCompletion = gpsCompletion;
        self.speedometerCompletion = speedometerCompletion;
        // Monitor the auto connection facility.
        // Keep the reference to be notified on update.
        autoConnectionRef = groundSdk.getFacility(Facilities.autoConnection)
        { [weak self] autoConnection in
            // Called when the auto connection facility is available and when it changes.

            if let self = self, let autoConnection = autoConnection {
                // Start auto connection.
                if (autoConnection.state != AutoConnectionState.started) {
                    autoConnection.start()
                }

                // If the drone has changed.
                if (self.drone?.uid != autoConnection.drone?.uid) {
                    if (self.drone != nil) {
                        // Stop to monitor the old drone.
                        self.stopDroneMonitors()
                    }

                    // Monitor the new drone.
                    self.drone = autoConnection.drone
                    if (self.drone != nil) {
                        self.droneModelCompletion?(self.drone?.description ?? "");
                        self.startDroneMonitors()
                    }
                }
            }
        }
    }
    
    /// Stops drone monitors.
    @objc
    public func stop() {
        autoConnectionRef = nil
        self.stopDroneMonitors()
    }

    private func stopDroneMonitors() {
        // Forget references linked to the current drone to stop their monitoring.
        droneStateRef = nil
        droneGPSRef = nil
        droneSpeedometerRef = nil
    }
    
    /// Starts drone monitors.
    private func startDroneMonitors() {
        // Monitor drone state.
        monitorDroneState()
        monitorGPS()
        
    }
    
    /// Monitor current drone state.
    private func monitorDroneState() {
        // Monitor current drone state.
        droneStateRef = drone?.getState { [weak self] state in
            // Called at each drone state update.

            if let self = self, let state = state {
                switch state.connectionState {
                    case .disconnected:
                        self.connectionCompletion?("DISCONNECTED");
                    case .connecting:
                        self.connectionCompletion?("CONNECTING");
                    case .connected:
                        self.connectionCompletion?("CONNECTED");
                    case .disconnecting:
                        self.connectionCompletion?("DISCONNECTING");
                    default:
                        self.connectionCompletion?("Unknown");
                }                
            }
        }

    }
    
    /// Monitors current drone battery level.
    private func monitorGPS() {
        // Monitor the battery info instrument.
        droneGPSRef = drone?.getInstrument(Instruments.gps) { [weak self] gps in
            // Called when the battery info instrument is available and when it changes.

            if let self = self, let gps = gps {
                // Update drone battery level view.
                if let lastKnownLocation = gps.lastKnownLocation {
                    let altitude = lastKnownLocation.altitude
                    let latitude = lastKnownLocation.coordinate.latitude
                    let longitude = lastKnownLocation.coordinate.longitude
                    self.gpsCompletion?(latitude, longitude, altitude)
//                    self.droneBatteryTxt.text = "\(batteryInfo.batteryLevel)%"
                }
                
            }
        }
        
        droneSpeedometerRef = drone?.getInstrument(Instruments.speedometer){
            [weak self] speedometer in
            if let self = self, let speedometer = speedometer {
                let velocityZ = speedometer.forwardSpeed
                let velocityX = speedometer.rightSpeed
                let velocityY = speedometer.downSpeed
                
                self.speedometerCompletion?(velocityX, velocityY, velocityZ)
            }
        }
    }
}
