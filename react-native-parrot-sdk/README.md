# react-native-parrot-sdk

## Getting started

`$ npm install react-native-parrot-sdk --save`

### Mostly automatic installation

`$ react-native link react-native-parrot-sdk`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-parrot-sdk` and add `ParrotSdk.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libParrotSdk.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainApplication.java`
  - Add `import com.reactlibrary.ParrotSdkPackage;` to the imports at the top of the file
  - Add `new ParrotSdkPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-parrot-sdk'
  	project(':react-native-parrot-sdk').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-parrot-sdk/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-parrot-sdk')
  	```


## Usage
```javascript
import ParrotSdk from 'react-native-parrot-sdk';

// TODO: What to do with the module?
ParrotSdk;
```
