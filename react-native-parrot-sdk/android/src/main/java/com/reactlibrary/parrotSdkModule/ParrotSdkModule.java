package com.reactlibrary.parrotSdkModule;

import android.os.Looper;

import androidx.annotation.Nullable;

import com.facebook.react.ReactActivity;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import com.parrot.drone.groundsdk.GroundSdk;
import com.parrot.drone.groundsdk.ManagedGroundSdk;
import com.parrot.drone.groundsdk.Ref;
import com.parrot.drone.groundsdk.device.DeviceState;
import com.parrot.drone.groundsdk.device.Drone;
import com.parrot.drone.groundsdk.device.instrument.Gps;
import com.parrot.drone.groundsdk.device.instrument.Speedometer;
import com.parrot.drone.groundsdk.facility.AutoConnection;

public class ParrotSdkModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    private final static String onDroneModelEvent = "onDroneModel";
    private final static String onConnectionEvent = "onConnection";
    private final static String onGPSTelemetryEvent = "onGPSTelemetry";
    private final static String onSpeedometerEvent = "onSpeedometer";

    /** GroundSdk instance. */
    private GroundSdk groundSdk;
    private Ref<AutoConnection> autoConnectionRef;
    private Ref<Gps> gpsRef;
    private Ref<Speedometer> speedometerRef;


    private Drone drone;

    /** Reference to the current drone state. */
    private Ref<DeviceState> droneStateRef;

    public ParrotSdkModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "ParrotSdk";
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    @ReactMethod
    public void sampleMethod(String stringArgument, int numberArgument, Callback callback) {
        // TODO: Implement some actually useful functionality
        callback.invoke("Received numberArgument: " + numberArgument + " stringArgument: " + stringArgument);
    }

    @ReactMethod
    public void start(){
        runOnUiQueue(new Runnable() {
            @Override
            public void run() {
                groundSdk = ManagedGroundSdk.obtainSession(getActivity());

                autoConnectionRef = groundSdk.getFacility(AutoConnection.class, new Ref.Observer<AutoConnection>() {
                    @Override
                    public void onChanged(@Nullable AutoConnection autoConnection) {

                        if (autoConnection.getStatus() != AutoConnection.Status.STARTED) {
                            autoConnection.start();
                        }

                        // If the drone has changed.
                        if (drone == null || autoConnection.getDrone().getUid().equals(drone.getUid())) {
                            if(drone != null) {
                                // Stop monitoring the old drone.
                                stopDroneMonitors();
                            }

                            // Monitor the new drone.
                            drone =  autoConnection.getDrone();
                            if(drone != null) {

                                if(drone.getModel() != null)
                                {
                                    WritableMap params = Arguments.createMap();
                                    params.putBoolean("success", true);
                                    params.putString("model", drone.getModel().toString());
                                    sendEvent(reactContext, onDroneModelEvent, params);
                                }

                                startDroneMonitors();
                            }
                        }
                    }
                });
            }
        });
    }

    @ReactMethod
    public void stop(){
        if(autoConnectionRef != null){
            autoConnectionRef.close();
            autoConnectionRef = null;
        }
        stopDroneMonitors();
    }

    private void runOnUiQueue(final Runnable runnable)
    {
        if(Looper.getMainLooper().getThread() == Thread.currentThread())
        {
            runnable.run();
        }else {
            reactContext.runOnUiQueueThread(runnable);
        }
    }

    private String connectionStateToString(DeviceState.ConnectionState connectionState) {
        switch (connectionState) {
            case DISCONNECTED:
                return "DISCONNECTED";
            case CONNECTING:
                return "CONNECTING";
            case CONNECTED:
                return "CONNECTED";
            case DISCONNECTING:
                return "DISCONNECTING";
            default:
                return "Unknown";
        }
    }

    /**
     * Starts drone monitors.
     */
    private void startDroneMonitors() {
        // Monitor current drone state.
        droneStateRef = drone.getState(new Ref.Observer<DeviceState>() {
            @Override
            public void onChanged(@Nullable DeviceState obj) {
                WritableMap params = Arguments.createMap();
                params.putBoolean("success", true);
                params.putString("status", connectionStateToString(obj.getConnectionState()));
                sendEvent(reactContext, onConnectionEvent, params);
            }
        });

        gpsRef = drone.getInstrument(Gps.class, new Ref.Observer<Gps>() {
            @Override
            public void onChanged(@Nullable Gps obj) {
                if(obj.lastKnownLocation() != null)
                {
                    WritableMap params = Arguments.createMap();
                    params.putBoolean("success", true);
                    params.putDouble("latitude", obj.lastKnownLocation().getLatitude());
                    params.putDouble("longitude",  obj.lastKnownLocation().getLongitude());
                    params.putDouble("altitude",  obj.lastKnownLocation().getAltitude());
                    sendEvent(reactContext, onConnectionEvent, params);
                }

            }
        });

        speedometerRef = drone.getInstrument(Speedometer.class, new Ref.Observer<Speedometer>() {
            @Override
            public void onChanged(@Nullable Speedometer obj) {
                WritableMap params = Arguments.createMap();
                params.putBoolean("success", true);
                params.putDouble("velocityX", obj.getRightSpeed());
                params.putDouble("velocityY",  obj.getDownSpeed());
                params.putDouble("velocityZ",  obj.getForwardSpeed());
                sendEvent(reactContext, onConnectionEvent, params);
            }
        });
    }

    /**
     * Stops drone monitors.
     */
    private void stopDroneMonitors() {
        if(droneStateRef != null){
            droneStateRef.close();
            droneStateRef = null;
        }

        if(gpsRef != null){
            gpsRef.close();
            gpsRef = null;
        }

        if(speedometerRef != null){
            speedometerRef.close();
            speedometerRef = null;
        }
    }

    public ReactActivity getActivity() {
        return (ReactActivity)getCurrentActivity();
    }
}
