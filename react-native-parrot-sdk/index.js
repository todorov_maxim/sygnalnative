import { NativeModules } from 'react-native';

const { ParrotSdk } = NativeModules;

export default ParrotSdk;
