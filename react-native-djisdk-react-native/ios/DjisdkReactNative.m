#import "DjisdkReactNative.h"
#import <DJISDK/DJISDK.h>

@interface DjisdkReactNative ()<DJIAppActivationManagerDelegate, DJISDKManagerDelegate, DJIFlightControllerDelegate>
@property (nonatomic) DJIAppActivationState activationState;
@property (nonatomic) DJIAppActivationAircraftBindingState aircraftBindingState;
@property (nonatomic) DJIBaseProduct* product;

@end

@implementation DjisdkReactNative

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(sampleMethod:(NSString *)stringArgument numberParameter:(nonnull NSNumber *)numberArgument callback:(RCTResponseSenderBlock)callback)
{
    // TODO: Implement some actually useful functionality
    callback(@[[NSString stringWithFormat: @"numberArgument: %@ stringArgument: %@", numberArgument, stringArgument]]);
}

NSString * const onStatusUpdateEvent = @"onStatusUpdate";
NSString * const onLoginEvent = @"onLogin";
NSString * const onLogoutEvent = @"onLogout";
NSString * const onDownloadProgressEvent = @"onDownloadProgress";
NSString * const onTelemetryAircraftEvent = @"onTelemetryAircraft";

- (NSArray<NSString *> *)supportedEvents
{
    return @[onStatusUpdateEvent, onDownloadProgressEvent, onTelemetryAircraftEvent, onLoginEvent, onLogoutEvent];
}

RCT_EXPORT_METHOD(start)
{
    [self registerApp];
}

RCT_EXPORT_METHOD(stop)
{
    self.activationState = DJIAppActivationStateUnknown;
    self.aircraftBindingState = DJIAppActivationAircraftBindingStateUnknown;
    self.product = nil;
}


RCT_EXPORT_METHOD(login)
{
    [[DJISDKManager userAccountManager] logIntoDJIUserAccountWithAuthorizationRequired:NO withCompletion:^(DJIUserAccountState state, NSError * _Nullable error) {
        if (error) {
            
            [self sendEventWithName:onLoginEvent body:@{@"success": @NO, @"msg":error.description}];
        }else{
            [self sendEventWithName:onLoginEvent body:@{@"success": @YES, @"msg":@"Login Success"
            }];
            
        }
    }];
}

RCT_EXPORT_METHOD(logout)
{
    [[DJISDKManager userAccountManager] logOutOfDJIUserAccountWithCompletion:^(NSError * _Nullable error) {
        if (error) {
            [self sendEventWithName:onLogoutEvent body:@{@"success": @NO, @"msg":error.description}];
        }else{
            [self sendEventWithName:onLogoutEvent body:@{@"success": @YES, @"msg":@"Logout Success"
            }];
        }
    }];
}

- (void)registerApp
{
    [DJISDKManager registerAppWithDelegate:self];
}

-(void)updateStatusBasedOn:(DJIBaseProduct*) newConnectedProduct{
    
    self.product = newConnectedProduct;
    if (newConnectedProduct)
    {
        if([newConnectedProduct isKindOfClass:[DJIAircraft class]] ){
            DJIAircraft* aircraft = (DJIAircraft*)newConnectedProduct;
            
            DJIFlightController * flightController = [aircraft flightController];
            if(flightController){
                flightController.delegate = self;
            }
        }
    }
}

- (void)flightController:(DJIFlightController *_Nonnull)fc didUpdateState:(DJIFlightControllerState *_Nonnull)state{
    
    CLLocationCoordinate2D coordinate = state.aircraftLocation.coordinate;
    [self sendEventWithName:onTelemetryAircraftEvent body:@{@"success": @YES, @"latitude":@(coordinate.latitude),
                                                            @"longitude":@(coordinate.longitude),
                                                            @"altitude":@(state.altitude),
                                                            @"velocityZ":@(state.velocityZ),
                                                            @"velocityX":@(state.velocityX),
                                                            @"velocityY":@(state.velocityY),
                                                            @"model":self.product.model,
    }];
}

- (void)appRegisteredWithError:(NSError * _Nullable)error {
    NSString* message = @"Register App Successed!";
    
    if (error) {
        message = @"Register App Failed! Please enter your App Key in the plist file and check the network.";
        
        [self sendEventWithName:onStatusUpdateEvent body:@{@"success": @NO, @"msg": message, @"activationState":DJIAppActivationState_toString[DJIAppActivationStateUnknown],@"aircraftBindingState":DJIAppActivationAircraftBindingState_toString[DJIAppActivationAircraftBindingStateUnknown]
        }];
        
    }else
    {
        NSLog(@"registerAppSuccess");
        
        [DJISDKManager appActivationManager].delegate = self;
        [DJISDKManager startConnectionToProduct];
        
        self.activationState = [DJISDKManager appActivationManager].appActivationState;
        self.aircraftBindingState = [DJISDKManager appActivationManager].aircraftBindingState;
        [self updateStatusBasedOn:[DJISDKManager product]];
        
        [self sendEventWithName:onStatusUpdateEvent body:@{@"success": @YES, @"msg": message, @"activationState":DJIAppActivationState_toString[self.activationState],@"aircraftBindingState":DJIAppActivationAircraftBindingState_toString[self.aircraftBindingState]
        }];
    }
}

NSString * const DJIAppActivationState_toString[] = {
    [DJIAppActivationStateNotSupported] = @"NotSupported",
    [DJIAppActivationStateLoginRequired] = @"LoginRequired",
    [DJIAppActivationStateActivated] = @"Activated",
    [DJIAppActivationStateUnknown] = @"Unknown"
};

NSString * const DJIAppActivationAircraftBindingState_toString[] = {
    [DJIAppActivationAircraftBindingStateInitial] = @"Initial",
    [DJIAppActivationAircraftBindingStateUnbound] = @"Unbound",
    [DJIAppActivationAircraftBindingStateUnboundButCannotSync] = @"UnboundButCannotSync",
    [DJIAppActivationAircraftBindingStateBound] = @"Bound",
    [DJIAppActivationAircraftBindingStateNotRequired] = @"NotRequired",
    [DJIAppActivationAircraftBindingStateNotSupported] = @"NotSupported",
    [DJIAppActivationAircraftBindingStateUnknown] = @"Unknown",
};

- (void)didUpdateDatabaseDownloadProgress:(nonnull NSProgress *)progress {
    
    [self sendEventWithName:onDownloadProgressEvent body:@{@"success": @YES, @"completedUnitCount":@(progress.completedUnitCount),
                                                           @"totalUnitCount":@(progress.totalUnitCount)
    }];
}

- (void)productConnected:(DJIBaseProduct *_Nullable)product
{
    [self updateStatusBasedOn:product];
}

- (void)productDisconnected{
    [self updateStatusBasedOn:nil];
}

-(void)manager:(DJIAppActivationManager *)manager didUpdateAppActivationState:(DJIAppActivationState)appActivationState
{
    
    self.activationState = appActivationState;
    [self sendEventWithName:onStatusUpdateEvent body:@{@"success": @YES, @"msg": @"", @"activationState":DJIAppActivationState_toString[self.activationState],@"aircraftBindingState":DJIAppActivationAircraftBindingState_toString[self.aircraftBindingState]
    }];
}

-(void)manager:(DJIAppActivationManager *)manager didUpdateAircraftBindingState:(DJIAppActivationAircraftBindingState)aircraftBindingState{
    
    
    self.aircraftBindingState = aircraftBindingState;
    
    [self sendEventWithName:onStatusUpdateEvent body:@{@"success": @YES, @"msg": @"", @"activationState":DJIAppActivationState_toString[self.activationState],@"aircraftBindingState":DJIAppActivationAircraftBindingState_toString[self.aircraftBindingState]
    }];
    
}

@end
