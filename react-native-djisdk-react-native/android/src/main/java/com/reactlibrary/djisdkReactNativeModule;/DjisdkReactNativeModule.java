package com.reactlibrary.djisdkReactNativeModule;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.facebook.react.ReactActivity;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.modules.core.PermissionListener;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import dji.common.error.DJIError;
import dji.common.error.DJISDKError;
import dji.common.flightcontroller.FlightControllerState;
import dji.common.realname.AircraftBindingState;
import dji.common.realname.AppActivationState;
import dji.sdk.base.BaseComponent;
import dji.sdk.base.BaseProduct;
import dji.sdk.flightcontroller.FlightController;
import dji.sdk.products.Aircraft;
import dji.sdk.sdkmanager.DJISDKInitEvent;
import dji.sdk.sdkmanager.DJISDKManager;

public class DjisdkReactNativeModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    private final static String onStatusUpdateEvent = "onStatusUpdate";
    private final static String onLoginEvent = "onLogin";
    private final static String onLogoutEvent = "onLogout";
    private final static String onDownloadProgressEvent = "onDownloadProgress";
    private final static String onTelemetryAircraftEvent = "onTelemetryAircraft";


    private static BaseProduct mProduct;
    private FlightController mFlightController = null;
    private String mAircraftBindingState = "Unknown";
    private String mAppActivationState = "Unknown";

//    private Handler mHandler;

    private static final String[] REQUIRED_PERMISSION_LIST = new String[]{
            Manifest.permission.VIBRATE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.WAKE_LOCK,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CHANGE_WIFI_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
    };
    private List<String> missingPermission = new ArrayList<>();
    private AtomicBoolean isRegistrationInProgress = new AtomicBoolean(false);
    private static final int REQUEST_PERMISSION_CODE = 12345;


    public DjisdkReactNativeModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "DjisdkReactNative";
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    @ReactMethod
    public void sampleMethod(String stringArgument, int numberArgument, Callback callback) {
        // TODO: Implement some actually useful functionality
        callback.invoke("Received numberArgument: " + numberArgument + " stringArgument: " + stringArgument);
    }

    @ReactMethod
    public void start() {

        // When the compile and target version is higher than 22, please request the following permission at runtime to ensure the SDK works well.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();
        }
    }

    @ReactMethod
    public void stop(){
        mFlightController = null;
        if(mProduct != null){
            mProduct.destroy();
            mProduct = null;
        }
        mAircraftBindingState = "Unknown";
        mAppActivationState = "Unknown";
    }

    @ReactMethod
    public void login() {

    }

    @ReactMethod
    public void logout() {

    }



    public ReactActivity getActivity() {
        return (ReactActivity) getCurrentActivity();
    }

    /**
     * Checks if there is any missing permissions, and
     * requests runtime permission if needed.
     */
    private void checkAndRequestPermissions() {
        // Check for permissions
        for (String eachPermission : REQUIRED_PERMISSION_LIST) {
            if (ContextCompat.checkSelfPermission(reactContext, eachPermission) != PackageManager.PERMISSION_GRANTED) {
                missingPermission.add(eachPermission);
            }
        }
        // Request for missing permissions
        if (missingPermission.isEmpty()) {
            startSDKRegistration();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getActivity().requestPermissions(missingPermission.toArray(new String[missingPermission.size()]),
                    REQUEST_PERMISSION_CODE, new PermissionListener() {

                        @Override
                        public boolean onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

                            if (requestCode == REQUEST_PERMISSION_CODE) {
                                for (int i = grantResults.length - 1; i >= 0; i--) {
                                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                                        missingPermission.remove(permissions[i]);
                                    }
                                }
                                // If there is enough permission, we will start the registration
                                if (missingPermission.isEmpty()) {
                                    startSDKRegistration();
                                } else {

                                    WritableMap params = Arguments.createMap();
                                    params.putBoolean("success", false);
                                    params.putString("msg", "Missing permissions!!!");
                                    params.putString("activationState", mAppActivationState);
                                    params.putString("aircraftBindingState", mAircraftBindingState);
                                    sendEvent(reactContext, onStatusUpdateEvent, params);

//                                    showToast("Missing permissions!!!");
                                }
                            }
                            return false;
                        }
                    });
        }

    }

    private String appActivationStateToString(AppActivationState appActivationState) {
        switch (appActivationState) {
            case NOT_SUPPORTED:
                return "NotSupported";
            case LOGIN_REQUIRED:
                return "LoginRequired";
            case ACTIVATED:
                return "Activated";
            case UNKNOWN:
                return "Unknown";
            default:
                return "Unknown";
        }
    }

    private String aircraftBindingStateToString(AircraftBindingState aircraftBindingState) {
        switch (aircraftBindingState) {
            case NOT_SUPPORTED:
                return "NotSupported";
            case INITIAL:
                return "Initial";
            case BOUND:
                return "Bound";
            case NOT_REQUIRED:
                return "NotRequired";
            case UNBOUND:
                return "Unbound";
            case UNBOUND_BUT_CANNOT_SYNC:
                return "UnboundButCannotSync";
            case UNKNOWN:
                return "Unknown";
            default:
                return "Unknown";
        }
    }

    private void startSDKRegistration() {
        if (isRegistrationInProgress.compareAndSet(false, true)) {
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {

                    DJISDKManager.getInstance().registerApp(reactContext, new DJISDKManager.SDKManagerCallback() {
                        @Override
                        public void onRegister(DJIError djiError) {
                            if (djiError == DJISDKError.REGISTRATION_SUCCESS) {

                                WritableMap params = Arguments.createMap();
                                params.putBoolean("success", true);
                                params.putString("msg", "Register Success");
                                params.putString("activationState", mAppActivationState);
                                params.putString("aircraftBindingState", mAircraftBindingState);
                                sendEvent(reactContext, onStatusUpdateEvent, params);

                                DJISDKManager.getInstance().startConnectionToProduct();

                                if( DJISDKManager.getInstance().getAppActivationManager() != null){
                                    DJISDKManager.getInstance().getAppActivationManager().addAircraftBindingStateListener(new AircraftBindingState.AircraftBindingStateListener() {
                                        @Override
                                        public void onUpdate(AircraftBindingState aircraftBindingState) {
                                            mAircraftBindingState = aircraftBindingStateToString(aircraftBindingState);

                                            WritableMap params = Arguments.createMap();
                                            params.putBoolean("success", true);
                                            params.putString("msg", "");
                                            params.putString("activationState", mAppActivationState);
                                            params.putString("aircraftBindingState", mAircraftBindingState);
                                            sendEvent(DjisdkReactNativeModule.this.reactContext, onStatusUpdateEvent, params);
                                        }
                                    });

                                    DJISDKManager.getInstance().getAppActivationManager().addAppActivationStateListener(new AppActivationState.AppActivationStateListener() {
                                        @Override
                                        public void onUpdate(AppActivationState appActivationState) {
                                            mAppActivationState = appActivationStateToString(appActivationState);


                                            WritableMap params = Arguments.createMap();
                                            params.putBoolean("success", true);
                                            params.putString("msg", "");
                                            params.putString("activationState", mAppActivationState);
                                            params.putString("aircraftBindingState", mAircraftBindingState);
                                            sendEvent(DjisdkReactNativeModule.this.reactContext, onStatusUpdateEvent, params);
                                        }
                                    });
                                }

                            } else {

                                WritableMap params = Arguments.createMap();
                                params.putBoolean("success", false);
                                params.putString("msg", "Register sdk fails, please check the bundle id and network connection!");
                                params.putString("activationState", mAppActivationState);
                                params.putString("aircraftBindingState", mAircraftBindingState);
                                sendEvent(reactContext, onStatusUpdateEvent, params);
                            }
                        }

                        @Override
                        public void onProductDisconnect() {
                            mProduct = null;
                        }

                        @Override
                        public void onProductConnect(BaseProduct baseProduct) {
                            mProduct = baseProduct;
                            initFlightController();
                        }

                        @Override
                        public void onComponentChange(BaseProduct.ComponentKey componentKey, BaseComponent oldComponent,
                                                      BaseComponent newComponent) {

                            if (newComponent != null) {
                                newComponent.setComponentListener(new BaseComponent.ComponentListener() {

                                    @Override
                                    public void onConnectivityChange(boolean isConnected) {
//                                        Log.d(TAG, "onComponentConnectivityChanged: " + isConnected);
//                                        notifyStatusChange();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onInitProcess(DJISDKInitEvent djisdkInitEvent, int i) {

                        }

                        @Override
                        public void onDatabaseDownloadProgress(long l, long l1) {

                            WritableMap params = Arguments.createMap();
                            params.putBoolean("success", true);
                            params.putDouble("completedUnitCount", l);
                            params.putDouble("totalUnitCount", l1);
                            sendEvent(reactContext, onDownloadProgressEvent, params);
                        }

                    });
                }
            });
        }
    }

    private void initFlightController() {

        if (isFlightControllerSupported()) {
            mFlightController = ((Aircraft) DJISDKManager.getInstance().getProduct()).getFlightController();
            mFlightController.setStateCallback(new FlightControllerState.Callback() {
                @Override
                public void onUpdate(FlightControllerState
                                             djiFlightControllerCurrentState) {
                    double droneLocationLat = djiFlightControllerCurrentState.getAircraftLocation().getLatitude();
                    double droneLocationLng = djiFlightControllerCurrentState.getAircraftLocation().getLongitude();
                    double droneLocationAltitude = djiFlightControllerCurrentState.getAircraftLocation().getAltitude();


                    WritableMap params = Arguments.createMap();
                    params.putBoolean("success", true);
                    params.putDouble("latitude", droneLocationLat);
                    params.putDouble("longitude", droneLocationLng);
                    params.putDouble("altitude", droneLocationAltitude);
                    params.putDouble("velocityZ", djiFlightControllerCurrentState.getVelocityZ());
                    params.putDouble("velocityX", djiFlightControllerCurrentState.getVelocityX());
                    params.putDouble("velocityY", djiFlightControllerCurrentState.getVelocityY());
                    params.putString("model", mProduct.getModel().name());
                    sendEvent(reactContext, onTelemetryAircraftEvent, params);


                }
            });
        }
    }


    private boolean isFlightControllerSupported() {
        return DJISDKManager.getInstance().getProduct() != null &&
                DJISDKManager.getInstance().getProduct() instanceof Aircraft &&
                ((Aircraft) DJISDKManager.getInstance().getProduct()).getFlightController() != null;
    }
}
