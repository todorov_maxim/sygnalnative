require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "react-native-djisdk-react-native"
  s.version      = package["version"]
  s.summary      = package["description"]
  s.description  = <<-DESC
                  react-native-djisdk-react-native
                   DESC
  s.homepage     = "https://github.com/github_account/react-native-djisdk-react-native"
  s.license      = "MIT"
  # s.license    = { :type => "MIT", :file => "FILE_LICENSE" }
  s.authors      = { "Your Name" => "yourname@email.com" }
  s.platforms    = { :ios => "9.0", :tvos => "10.0" }
  s.source       = { :git => "https://github.com/github_account/react-native-djisdk-react-native.git", :tag => "#{s.version}" }

  s.source_files = "ios/**/*.{h,m,swift}"
  s.requires_arc = true

  s.dependency "React"
    s.dependency 'DJI-SDK-iOS', '~> 4.12'
    s.dependency 'DJI-UXSDK-iOS', '~> 4.12'
    s.dependency 'DJIWidget', '~> 1.6.2'
  # s.dependency "..."
end

