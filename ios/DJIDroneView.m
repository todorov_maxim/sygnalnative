//
//  RCTView+DJIDroneView.m
//  DroneApp
//
//  Created by Ammar Ahmed on 3/30/1399 AP.
//

#import "DJIDroneView.h"
#import <DJISDK/DJISDK.h>
#import <DJIUXSDK/DJIUXSDK.h>
#import <React/RCTUtils.h>

@interface DJIDroneView ()<DJIAppActivationManagerDelegate, DJISDKManagerDelegate, DJIFlightControllerDelegate>

@property (nonatomic, nullable) UIViewController *topController;
@property (nonatomic) DJIAppActivationState activationState;
@property (nonatomic) DJIAppActivationAircraftBindingState aircraftBindingState;
@property (nonatomic) DJIBaseProduct* product;

@end


@implementation DJIDroneView



- (instancetype)initWithFrame:(CGRect)frame {
  if ((self = [super initWithFrame:frame])) {
    
    [DJISDKManager registerAppWithDelegate:self];
    _djiViewController = [[DUXDefaultLayoutViewController alloc] init];
    DUXFPVViewController *fpvC =  (DUXFPVViewController*) _djiViewController.contentViewController;
    if (fpvC != nil) {
        [fpvC.fpvView setShowCameraDisplayName:NO];
    }
  }
  
  return self;
}



- (void)dealloc {
  [self destroyViewControllerRelationship];
}


- (void)didMoveToWindow {
  UIViewController *controller = self.dji_parentViewController;
  if (controller == nil || self.window == nil || self.topController != nil) {
    return;
  }
  
  
  
  self.topController = self.djiViewController;
  
  UIView *topControllerView = self.topController.view;
  topControllerView.translatesAutoresizingMaskIntoConstraints = NO;
  
  [self addSubview:topControllerView];
  [controller addChildViewController:self.topController];
  [self.topController didMoveToParentViewController:controller];
  
  [NSLayoutConstraint activateConstraints:
   @[[topControllerView.topAnchor constraintEqualToAnchor:self.topAnchor],
     [topControllerView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor],
     [topControllerView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor],
     [topControllerView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor],
   ]];
  
}

- (void)destroyViewControllerRelationship {
  if (self.topController.parentViewController) {
    [self.topController willMoveToParentViewController:nil];
    [self.topController removeFromParentViewController];
  }
}


- (UIViewController *)dji_parentViewController {
  UIResponder *parentResponder = self;
  while ((parentResponder = parentResponder.nextResponder)) {
    if ([parentResponder isKindOfClass:UIViewController.class]) {
      return (UIViewController *)parentResponder;
    }
  }
  return nil;
}

- (void)removeFromSuperview {
  
  [self.djiViewController dismissViewControllerAnimated:NO completion:NULL];
  [super removeFromSuperview];
}

- (void)showAlertViewWithMessage:(NSString *)message
{
  dispatch_async(dispatch_get_main_queue(), ^{
    UIAlertController* alertViewController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertViewController addAction:okAction];
    UIViewController *rootViewController = [[UIApplication sharedApplication] keyWindow].rootViewController;
    [rootViewController presentViewController:alertViewController animated:YES completion:nil];
  });
  
}

#pragma mark - DJISDKManagerDelegate

- (void)appRegisteredWithError:(NSError *)error
{
  NSString* message = @"Register App Successed!";
  
  if (!error) {
   
    [DJISDKManager appActivationManager].delegate = self;
    [DJISDKManager startConnectionToProduct];
    self.activationState = [DJISDKManager appActivationManager].appActivationState;
    self.aircraftBindingState = [DJISDKManager appActivationManager].aircraftBindingState;
    [self updateStatusBasedOn:[DJISDKManager product]];
    if (self.onStateChanged) {
      self.onStateChanged(@{@"success": @YES, @"msg": message, @"activationState":DJIAppActivationState_toString[self.activationState],@"state":DJIAppActivationAircraftBindingState_toString[self.aircraftBindingState],@"model":self.product? self.product.model : @""
           });
    }
    
    if (_djiViewController != nil) {
      DUXFPVViewController *fpvC =  (DUXFPVViewController*) _djiViewController.contentViewController;
      if (fpvC != nil) {
          [fpvC.fpvView setShowCameraDisplayName:NO];
      }
    }
   
    
  }else
  {
    if (self.onStateChanged) {
      
        message = @"Register App Failed! Please enter your App Key in the plist file and check the network.";
     
      self.onStateChanged(@{@"success": @NO, @"msg": message, @"activationState":DJIAppActivationState_toString[DJIAppActivationStateUnknown],@"state":DJIAppActivationAircraftBindingState_toString[DJIAppActivationAircraftBindingStateUnknown],@"model":self.product? self.product.model : @""
      });
      
      
    }
    
  }
}

-(void)updateStatusBasedOn:(DJIBaseProduct*) newConnectedProduct{
  
  self.product = newConnectedProduct;
  if (newConnectedProduct)
  {
    if([newConnectedProduct isKindOfClass:[DJIAircraft class]] ){
      DJIAircraft* aircraft = (DJIAircraft*)newConnectedProduct;
      
      if (aircraft && aircraft.model) {
        if (self.onStateChanged) {
          self.onStateChanged(@{@"success": @YES, @"msg": @"connection",  @"activationState":DJIAppActivationState_toString[self.activationState],@"state":DJIAppActivationAircraftBindingState_toString[self.aircraftBindingState],@"model":self.product? self.product.model : @""
                   });
        }
        if (_djiViewController != nil) {
          DUXFPVViewController *fpvC =  (DUXFPVViewController*) _djiViewController.contentViewController;
          if (fpvC != nil) {
              [fpvC.fpvView setShowCameraDisplayName:NO];
          }
        }
       
      }
      
      DJIFlightController * flightController = [aircraft flightController];
      if(flightController){
        
        
        
        flightController.delegate = self;
      }
    }
  }
}

- (void)flightController:(DJIFlightController *_Nonnull)fc didUpdateState:(DJIFlightControllerState *_Nonnull)state{
  
  CLLocationCoordinate2D coordinate = state.aircraftLocation.coordinate;
  if (self.onTelemetryUpdated) {
    self.onTelemetryUpdated(@{@"success": @YES, @"latitude":@(coordinate.latitude),
                              @"longitude":@(coordinate.longitude),
                              @"absoluteAltitude":@(state.altitude),
                              @"forwardSpeed":@(state.velocityZ),
                              @"rightSpeed":@(state.velocityX),
                              @"downSpeed":@(state.velocityY),
                              @"model":self.product? self.product.model : @""
    });
  }
  
}



- (void)didUpdateDatabaseDownloadProgress:(nonnull NSProgress *)progress {
  
  if (self.onDownloadProgress) {
    self.onDownloadProgress(@{@"success": @YES, @"completedUnitCount":@(progress.completedUnitCount),
                              @"totalUnitCount":@(progress.totalUnitCount)
    });
  }
}

- (void)encodeWithCoder:(nonnull NSCoder *)coder {
  
}

- (void)traitCollectionDidChange:(nullable UITraitCollection *)previousTraitCollection {
  
}

- (void)preferredContentSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
  
}

- (void)systemLayoutFittingSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
  
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
  
}

- (void)willTransitionToTraitCollection:(nonnull UITraitCollection *)newCollection withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
  
}

- (void)didUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context withAnimationCoordinator:(nonnull UIFocusAnimationCoordinator *)coordinator {
  
}

- (void)setNeedsFocusUpdate {
  
}

- (BOOL)shouldUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context {
  
  return false;
}

- (void)updateFocusIfNeeded {
  
}

NSString * const DJIAppActivationState_toString[] = {
  [DJIAppActivationStateNotSupported] = @"NotSupported",
  [DJIAppActivationStateLoginRequired] = @"LoginRequired",
  [DJIAppActivationStateActivated] = @"Activated",
  [DJIAppActivationStateUnknown] = @"Unknown"
};

NSString * const DJIAppActivationAircraftBindingState_toString[] = {
  [DJIAppActivationAircraftBindingStateInitial] = @"Initial",
  [DJIAppActivationAircraftBindingStateUnbound] = @"Unbound",
  [DJIAppActivationAircraftBindingStateUnboundButCannotSync] = @"UnboundButCannotSync",
  [DJIAppActivationAircraftBindingStateBound] = @"Bound",
  [DJIAppActivationAircraftBindingStateNotRequired] = @"NotRequired",
  [DJIAppActivationAircraftBindingStateNotSupported] = @"NotSupported",
  [DJIAppActivationAircraftBindingStateUnknown] = @"Unknown",
};

- (void)productConnected:(DJIBaseProduct *_Nullable)product
{
  [self updateStatusBasedOn:product];
}

- (void)productDisconnected{
  [self updateStatusBasedOn:nil];
}

-(void)manager:(DJIAppActivationManager *)manager didUpdateAppActivationState:(DJIAppActivationState)appActivationState
{
  
  self.activationState = appActivationState;
  if (self.onStateChanged) {
    self.onStateChanged( @{@"success": @YES, @"msg": @"", @"activationState":DJIAppActivationState_toString[self.activationState],@"state":DJIAppActivationAircraftBindingState_toString[self.aircraftBindingState],@"model":self.product? self.product.model : @""
    });
    if (_djiViewController != nil) {
      DUXFPVViewController *fpvC =  (DUXFPVViewController*) _djiViewController.contentViewController;
      if (fpvC != nil) {
          [fpvC.fpvView setShowCameraDisplayName:NO];
      }
    }
  }
  
}

-(void)manager:(DJIAppActivationManager *)manager didUpdateAircraftBindingState:(DJIAppActivationAircraftBindingState)aircraftBindingState{
  
  
  self.aircraftBindingState = aircraftBindingState;
  if (self.onStateChanged) {
    self.onStateChanged( @{@"success": @YES, @"msg": @"", @"activationState":DJIAppActivationState_toString[self.activationState],@"state":DJIAppActivationAircraftBindingState_toString[self.aircraftBindingState],@"model":self.product? self.product.model : @""
    });
    if (_djiViewController != nil) {
      DUXFPVViewController *fpvC =  (DUXFPVViewController*) _djiViewController.contentViewController;
      if (fpvC != nil) {
          [fpvC.fpvView setShowCameraDisplayName:NO];
      }
    }
  }
  
  
}

@end
