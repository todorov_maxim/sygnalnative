//
//  RCTViewManager+DJIDroneViewManager.m
//  DroneApp
//
//  Created by Ammar Ahmed on 3/30/1399 AP.
//

#import "DJIDroneView.h"
#import "DJIDroneViewManager.h"

#if __has_include(<React/RCTBridge.h>)
#import <React/RCTBridge.h>
#import <React/RCTUIManager.h>
#import <React/RCTEventDispatcher.h>
#import <React/RCTViewManager.h>
#else
#import "RCTBridge.h"
#import "RCTUIManager.h"
#import "RCTViewManager.h"
#import "RCTEventDispatcher.h"
#endif


@implementation DJIDroneViewManager 

RCT_EXPORT_MODULE(DJIDroneView);

DJIDroneView *djiDroneView;

-(UIView*)view
{
    djiDroneView = [[DJIDroneView alloc] init];
   
    return djiDroneView;
}

RCT_EXPORT_VIEW_PROPERTY(onStateChanged, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onDownloadProgress, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onTelemetryUpdated, RCTBubblingEventBlock)

@end
