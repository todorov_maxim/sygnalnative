//
//  RCTViewManager+DJIDroneViewManager.h
//  DroneApp
//
//  Created by Ammar Ahmed on 3/30/1399 AP.
//

#if __has_include(<React/RCTViewManager.h>)
#import <React/RCTViewManager.h>
#else
#import "RCTViewManager.h"
#endif

@interface DJIDroneViewManager: RCTViewManager

@end

