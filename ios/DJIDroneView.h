//
//  RCTView+DJIDroneView.h
//  DroneApp
//
//  Created by Ammar Ahmed on 3/30/1399 AP.
//


#import <DJIUXSDK/DJIUXSDK.h>
#import <UIKit/UIKit.h>
#import <React/RCTComponent.h>

@interface DJIDroneView: UIView

@property (nonatomic, readonly) DUXDefaultLayoutViewController *djiViewController;

@property (nonatomic, copy) RCTBubblingEventBlock onDownloadProgress;
@property (nonatomic, copy) RCTBubblingEventBlock onStateChanged;
@property (nonatomic, copy) RCTBubblingEventBlock onTelemetryUpdated;

@end

