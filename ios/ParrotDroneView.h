//
//  UIView+ParrotDroneView.h
//  DroneApp
//
//  Created by Ammar Ahmed on 4/6/1399 AP.
//

#import <UIKit/UIKit.h>
#import <React/RCTComponent.h>

@interface ParrotDroneView: UIView

@property (nonatomic, copy) RCTBubblingEventBlock onDroneFound;
@property (nonatomic, copy) RCTBubblingEventBlock onDroneConnected;
@property (nonatomic, copy) RCTBubblingEventBlock onLocationChanged;
@property (nonatomic, copy) RCTBubblingEventBlock onSpeedChanged;

@property (nonatomic, copy) RCTBubblingEventBlock onDroneLanded;
@property (nonatomic, copy) RCTBubblingEventBlock onDroneFlying;
@property (nonatomic, copy) RCTBubblingEventBlock onThrowTakeoff;

@property (nonatomic, copy) RCTBubblingEventBlock onReturnHome;
@property (nonatomic, copy) RCTBubblingEventBlock onReturnHomeIdle;

@property (nonatomic) BOOL returnHome; 
@property (nonatomic) NSString* manualPiloting;


@end


