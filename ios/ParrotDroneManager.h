//
//  UIView+ParrotDroneManager.h
//  DroneApp
//
//  Created by Ammar Ahmed on 4/6/1399 AP.
//

#if __has_include(<React/RCTViewManager.h>)
#import <React/RCTViewManager.h>
#else
#import "RCTViewManager.h"
#endif


@interface ParrotDroneManager:RCTViewManager

@end


