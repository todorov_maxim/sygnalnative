//
//  UIView+ParrotDroneManager.m
//  DroneApp
//
//  Created by Ammar Ahmed on 4/6/1399 AP.
//

#import "ParrotDroneManager.h"
#import "ParrotDroneView.h"

#if __has_include(<React/RCTBridge.h>)
#import <React/RCTBridge.h>
#import <React/RCTUIManager.h>
#import <React/RCTEventDispatcher.h>
#import <React/RCTViewManager.h>
#else
#import "RCTBridge.h"
#import "RCTUIManager.h"
#import "RCTViewManager.h"
#import "RCTEventDispatcher.h"
#endif

@implementation ParrotDroneManager:RCTViewManager


RCT_EXPORT_MODULE(ParrotDroneView);

ParrotDroneView *parrotDroneView;

-(UIView*)view
{
    parrotDroneView = [[ParrotDroneView alloc] init];
   
    return parrotDroneView;
}

RCT_EXPORT_VIEW_PROPERTY(returnHome, BOOL);
RCT_EXPORT_VIEW_PROPERTY(manualPiloting, NSString);

RCT_EXPORT_VIEW_PROPERTY(onDroneFound, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onDroneConnected, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onLocationChanged, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onSpeedChanged, RCTBubblingEventBlock)

RCT_EXPORT_VIEW_PROPERTY(onDroneLanded, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onDroneFlying, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onThrowTakeoff, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onReturnHome, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onReturnHomeIdle, RCTBubblingEventBlock)
@end
