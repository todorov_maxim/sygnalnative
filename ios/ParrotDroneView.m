//
//  UIView+ParrotDroneView.m
//  DroneApp
//
//  Created by Ammar Ahmed on 4/6/1399 AP.
//

#import "ParrotDroneView.h"
#import <GroundSdk/GroundSdk-Swift.h>
#import <AVFoundation/AVFoundation.h>
#import <VideoToolbox/VideoToolbox.h>
#import <CoreLocation/CLLocation.h>


@implementation ParrotDroneView

GroundSdk *groundSdk;
GSDrone *drone;

GSFacilityRef *autoConnectionRef;
GSInstrumentRef *speedStateRef;
GSInstrumentRef *locationStateRef;
GSDeviceStateRef *connectionStateRef;
GSPeripheralRef *streamServerRef;
GSCameraLiveRef *liveCameraRef;
GSStreamView *streamView;
GSPilotingItfRef *pilotingItfRef;
GSPilotingItfRef *returnHomeRef;

- (instancetype)initWithFrame:(CGRect)frame {
  if ((self = [super initWithFrame:frame])) {
    groundSdk = [[GroundSdk alloc] init];
    
    [self addStreamView];
    
  }
  return self;
}


- (void)addStreamView {
  
  
  streamView = [[GSStreamView alloc] initWithFrame:self.bounds];
  streamView.backgroundColor = [UIColor blackColor];
  self.autoresizesSubviews = YES;
  self.clipsToBounds = YES;
  streamView.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
                                 UIViewAutoresizingFlexibleHeight);
  [self addSubview:streamView];
  [self autoConnection];
}

- (void)autoConnection {
  
  autoConnectionRef = [groundSdk getFacility:GSFacilities.autoConnection
                                    observer:^(id<GSAutoConnection> _Nullable autoConnection) {
    
    if (autoConnection.state != GSAutoConnectionStateStarted) {
      [autoConnection start];
    }
    
    
    // If the drone has changed.
    if (autoConnection.drone != nil) {
      
      
      if ((drone == nil ) | (autoConnection.drone != nil &&  drone.uid != autoConnection.drone.uid)) {
        if (drone != nil) {
          // Stop to monitor the old drone.
          [self stopDroneMonitors];
        }
        
        drone = autoConnection.drone;
        if (drone != nil) {
          if (self.onDroneFound) {
            self.onDroneFound(@{
              @"success":@YES,
              @"model":drone.name,
              @"uid":drone.uid,
              @"state":ParrotConnectionState_toString[drone.state.connectionState]
            });
          }
          
          [self startDroneMonitors];
        }
      }
    } else {
      if (self.onDroneFound) {
        [self stopDroneMonitors];
        self.onDroneFound(@{
          @"success":@NO,
          @"model":@"",
          @"uid":@"",
          @"state":@"DISCONNECTED"
          
        });
      }
    }
    
  }];
}

- (void)takeOffLandRef {
  pilotingItfRef = [drone getPilotingItf:GSPilotingItfs.manualCopter observer:^(id<GSManualCopterPilotingItf> _Nullable it) {
    if (it == nil) return;
    
    switch (it.smartTakeOffLandAction) {
      case GSSmartTakeOffLandActionTakeOff:
        
        if (self.onDroneLanded != nil) {
          
          self.onDroneLanded(@{});
          
        }
        break;
      case GSSmartTakeOffLandActionLand:
        
        if (self.onDroneFlying != nil) {
          
          self.onDroneFlying(@{});
          
        }
        
        break;
      case GSSmartTakeOffLandActionThrownTakeOff:
        if (self.onDroneLanded != nil) {
          
          self.onDroneLanded(@{});
          
        }
        break;
      default:
        break;
    }
    
  }];
}

- (void)returnHomeRef {
  pilotingItfRef = [drone getPilotingItf:GSPilotingItfs.returnHome observer:^(id<GSReturnHomePilotingItf> _Nullable it) {
    if (it == nil) return;
    switch (it.state) {
      case GSActivablePilotingItfStateActive:
        
        if (self.onReturnHome != nil) {
          
          self.onReturnHome(@{});
          
        }
        break;
        
      case GSActivablePilotingItfStateUnavailable:
      case GSActivablePilotingItfStateIdle:
        
        if (self.onReturnHomeIdle != nil) {
          
          self.onReturnHomeIdle(@{});
          
        }
        
        break;
      default:
        break;
    }
    
  }];
}



- (void)setReturnHome:(BOOL)returnHome {
  id<GSManualCopterPilotingItf> returnHomeitf = (id<GSManualCopterPilotingItf>)
   [drone getPilotingItf:GSPilotingItfs.returnHome];
   if (returnHome) {
     BOOL res = [returnHomeitf activate];
   } else {
     BOOL res = [returnHomeitf deactivate];
   }
}


- (void)setManualPiloting:(NSString *)manualPiloting {
  if (drone != nil ) {
    
    id<GSManualCopterPilotingItf> pilotingItf = (id<GSManualCopterPilotingItf>)
    [drone getPilotingItf:GSPilotingItfs.manualCopter];
    if (pilotingItf != nil) {
      [pilotingItf smartTakeOffLand];
    }
  }
}


-(void)connectionState {
  
  connectionStateRef = [drone getStateRef:^(GSDeviceState * _Nullable state) {
    if (state == nil) return;
    if (self.onDroneFound) {
      self.onDroneFound(@{
        @"success":@YES,
        @"model":drone.name,
        @"uid":drone.uid,
        @"state":ParrotConnectionState_toString[state.connectionState],
      });
    }
  }];
}

-(void)speedState {
  speedStateRef = [drone getInstrument:GSInstruments.speedometer observer:^(id<GSSpeedometer> _Nullable speed) {
    if (speed == nil) return;
    if (self.onSpeedChanged) {
      self.onSpeedChanged(@{
        @"success":@YES,
        @"rightSpeed":[NSNumber numberWithDouble:speed.getGroundSpeed],
        @"forwardSpeed":[NSNumber numberWithDouble:speed.getGroundSpeed],
        @"downSpeed":[NSNumber numberWithDouble:speed.getGroundSpeed],
      });
    }
  }];
}

-(void)locationState {
  locationStateRef = [drone getInstrument:GSInstruments.gps observer:^(id<GSGps> _Nullable gps) {
    if (gps == nil) return;
    
    id<GSAltimeter> altimeter = (id<GSAltimeter>)[drone getInstrument:GSInstruments.altimeter];
    
    if (self.onLocationChanged) {
      self.onLocationChanged(@{
        @"success":@YES,
        @"latitude":[NSNumber numberWithDouble:gps.lastKnownLocation.coordinate.latitude],
        @"longitude":[NSNumber numberWithDouble:gps.lastKnownLocation.coordinate.longitude],
        @"absoluteAltitude":[altimeter getAbsoluteAltitude],
        @"verticalSpeed":[altimeter getVerticalSpeed],
        @"relativeAltitude":[altimeter getGroundRelativeAltitude],
        @"takeOffRelativeAltitude":[altimeter getTakeoffRelativeAltitude]
      });
    }
  }];
}

-(void)liveStream {
  
  /**
   This will show you an error when you are building it. id<GSStreamServer>  Jump to defination of this protocol and then add following
    
   
          public protocol GSStreamServer:   Peripheral // Inherit Peripheral
   
   */
  
  streamServerRef = [drone getPeripheral:GSPeripherals.streamServer observer:^(id<GSStreamServer>   _Nullable streamServer) {
    if (streamServer == nil) return;
    [streamServer setEnabled:true];
    
    liveCameraRef = [streamServer liveWithObserver:^(id<GSCameraLive> _Nullable liveStream) {
      
      [streamView setStreamWithStream:liveStream];
      BOOL play = [liveStream play];
      
    }];
    
  }];
  
  
  
}



NSString * const ParrotConnectionState_toString[] = {
  [GSDeviceConnectionStateConnected] = @"Connected",
  [GSDeviceConnectionStateConnecting] = @"Connecting",
  [GSDeviceConnectionStateDisconnected] = @"Disconnected",
  [GSDeviceConnectionStateDisconnecting] = @"Disconnecting"
};




-(void)startDroneMonitors {
  [self connectionState];
  [self speedState];
  [self locationState];
  [self liveStream];
  [self takeOffLandRef];
  [self returnHomeRef];
 
}

-(void)stopDroneMonitors{
  connectionStateRef = nil;
  speedStateRef = nil;
  locationStateRef = nil;
  liveCameraRef = nil;
  returnHomeRef = nil;
  pilotingItfRef = nil;
  
}


@end

