//
//  Parrot.swift
//  DroneApp
//
//  Created by Ammar Ahmed on 3/19/1399 AP.
//
import Foundation
import GroundSdk

@objc(Parrot)
class Parrot: NSObject {
  
  private let groundSdk = GroundSdk();
  private var autoConnectionRef: Ref<AutoConnection>?
  private var drone: Drone?
  
  @objc
  static func requiresMainQueueSetup() -> Bool {
    return true
  }
  
  override init() {
    super.init()
    // Monitor the auto connection facility.
    // Keep the reference to be notified on update.
    autoConnectionRef = groundSdk.getFacility(Facilities.autoConnection) { [weak self] autoConnection in
      // Called when the auto connection facility is available and when it changes.
      
      if let self = self, let autoConnection = autoConnection {
        // Start auto connection.
        if (autoConnection.state != AutoConnectionState.started) {
          autoConnection.start()
        }
        
        // If the drone has changed.
        if (self.drone?.uid != autoConnection.drone?.uid) {
          if (self.drone != nil) {
          }
          // Monitor the new drone.
          self.drone = autoConnection.drone
          
          if (self.drone != nil) {
            
          }
        }
      }
    }
  }
  
  
  
  @objc
  func getDrone(
    _ resolve: RCTPromiseResolveBlock,
    rejecter reject: RCTPromiseRejectBlock
  ) -> Void {
    
    if (drone != nil) {
      
      let uid = drone?.uid;
      let name = drone?.name;
      let model = drone?.model.description;
      let state = drone?.state.connectionState.description;
      
      let dic:[String:Any] = [
        "uid":uid ?? "null value",
        "name":name ?? "null value",
        "model":model ?? "null value",
        "state":state ?? "null value"
      ];
      resolve(dic)
      
    } else {
      let error = NSError(domain: "", code: 200, userInfo: nil)
      reject("Error", "Parrot Drone not found", error)
      
    }
  }
  
  @objc
  func getBatteryInfo(
    _ resolve: RCTPromiseResolveBlock,
    rejecter reject: RCTPromiseRejectBlock
  ) -> Void {
    
    if (drone != nil) {
      
      let health = drone?.getInstrument(Instruments.batteryInfo)?.batteryHealth
      let batteryLevel = drone?.getInstrument(Instruments.batteryInfo)?.batteryLevel
      let charging = drone?.getInstrument(Instruments.batteryInfo)?.isCharging
      let dic:[String:Any] = [
        "health":health ?? "null value",
        "level":batteryLevel ?? "null value",
        "charging":charging ?? "null value",
      ];
      resolve(dic)
      
    } else {
      let error = NSError(domain: "", code: 200, userInfo: nil)
      reject("Error", "Parrot Drone not found", error)
      
    }
  }
  
  @objc
  func getFlightInfo(
    _ resolve: RCTPromiseResolveBlock,
    rejecter reject: RCTPromiseRejectBlock
  ) -> Void {
    
    if (drone != nil) {
      
      let lastFlight = drone?.getInstrument(Instruments.flightMeter)?.lastFlightDuration
      let totalFlights = drone?.getInstrument(Instruments.flightMeter)?.totalFlights
      let totalFlightDuration = drone?.getInstrument(Instruments.flightMeter)?.totalFlightDuration
      let dic:[String:Any] = [
        "lastFlight":lastFlight ?? "null value",
        "totalFlights":totalFlights ?? "null value",
        "totalFlightDuration":totalFlightDuration ?? "null value",
      ];
      resolve(dic)
      
    } else {
      let error = NSError(domain: "", code: 200, userInfo: nil)
      reject("Error", "Parrot Drone not found", error)
      
    }
  }
  
  @objc
  func getAltitudeInfo(
    _ resolve: RCTPromiseResolveBlock,
    rejecter reject: RCTPromiseRejectBlock
  ) -> Void {
    
    if (drone != nil) {
      
      let absoluteAltitude = drone?.getInstrument(Instruments.altimeter)?.absoluteAltitude
      let groundRelativeAltitude = drone?.getInstrument(Instruments.altimeter)?.groundRelativeAltitude
      let verticalSpeed = drone?.getInstrument(Instruments.altimeter)?.verticalSpeed
      let takeoffRelativeAltitude = drone?.getInstrument(Instruments.altimeter)?.takeoffRelativeAltitude
      let dic:[String:Any] = [
        "absoluteAltitude":absoluteAltitude ?? "null value",
        "groundRelativeAltitude":groundRelativeAltitude ?? "null value",
        "verticalSpeed":verticalSpeed ?? "null value",
        "takeoffRelativeAltitude":takeoffRelativeAltitude ?? "null value",
      ];
      resolve(dic)
      
    } else {
      let error = NSError(domain: "", code: 200, userInfo: nil)
      reject("Error", "Parrot Drone not found", error)
      
    }
  }
  
  @objc
  func getLocationInfo(
    _ resolve: RCTPromiseResolveBlock,
    rejecter reject: RCTPromiseRejectBlock
  ) -> Void {
    
    if (drone != nil) {
      
      let latitude = drone?.getInstrument(Instruments.gps)?.lastKnownLocation?.coordinate.latitude
      let longitude = drone?.getInstrument(Instruments.gps)?.lastKnownLocation?.coordinate.longitude
      let dic:[String:Any] = [
        "latitude":latitude ?? "null value",
        "longitude":longitude ?? "null value",
      ];
      resolve(dic)
      
    } else {
      let error = NSError(domain: "", code: 200, userInfo: nil)
      reject("Error", "Parrot Drone not found", error)
      
    }
  }
  
  @objc
  func getSpeedInfo(
    _ resolve: RCTPromiseResolveBlock,
    rejecter reject: RCTPromiseRejectBlock
  ) -> Void {
    
    if (drone != nil) {
      
      let downSpeed = drone?.getInstrument(Instruments.speedometer)?.downSpeed
      let eastSpeed = drone?.getInstrument(Instruments.speedometer)?.eastSpeed
      let forwardSpeed = drone?.getInstrument(Instruments.speedometer)?.forwardSpeed
      let groundSpeed = drone?.getInstrument(Instruments.speedometer)?.groundSpeed
      let northSpeed = drone?.getInstrument(Instruments.speedometer)?.northSpeed
      let rightSpeed = drone?.getInstrument(Instruments.speedometer)?.rightSpeed
      let dic:[String:Any] = [
        "downSpeed":downSpeed ?? "null value",
        "eastSpeed":eastSpeed ?? "null value",
        "forwardSpeed":forwardSpeed ?? "null value",
        "groundSpeed":groundSpeed ?? "null value",
        "northSpeed":northSpeed ?? "null value",
        "rightSpeed":rightSpeed ?? "null value",
        
      ];
      resolve(dic)
      
    } else {
      let error = NSError(domain: "", code: 200, userInfo: nil)
      reject("Error", "Parrot Drone not found", error)
      
    }
  }
  
  @objc
  func getRadioInfo(
    _ resolve: RCTPromiseResolveBlock,
    rejecter reject: RCTPromiseRejectBlock
  ) -> Void {
    
    if (drone != nil) {
      
      let linkSignalQuality = drone?.getInstrument(Instruments.radio)?.linkSignalQuality
      let rssi = drone?.getInstrument(Instruments.radio)?.rssi
      
      let dic:[String:Any] = [
        "linkSignalQuality":linkSignalQuality ?? "null value",
        "rssi":rssi ?? "null value",
      ];
      resolve(dic)
      
    } else {
      let error = NSError(domain: "", code: 200, userInfo: nil)
      reject("Error", "Parrot Drone not found", error)
      
    }
  }
  
  
}
