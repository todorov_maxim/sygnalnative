export const roundFloat = num => {
  let decimals = 2;
  var newValue = Number(Math.round(num + 'e' + decimals) + 'e-' + decimals);
  if (isNaN(newValue)) {
    newValue = 0;
  }
  return newValue;
};

export const delay = ms => new Promise(yea => setTimeout(yea, ms));


export const getElevation = (elevation) => {
  return {
    elevation,
    shadowColor: 'black',
    shadowOffset: { width: 0.3 * elevation, height: 0.5 * elevation },
    shadowOpacity: 0.2,
    shadowRadius: 0.7 * elevation,
  };
};