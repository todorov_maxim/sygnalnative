import {StyleSheet, Dimensions, Platform} from 'react-native';

const {width: viewportWidth, height: viewportHeight} = Dimensions.get('window');

function wp(percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideHeight = viewportHeight * 0.8;
const slideWidth = wp(80);
const itemHorizontalMargin = wp(2);

// export const sliderWidth = viewportWidth;
// export const itemWidth = slideWidth + itemHorizontalMargin * 2;
export const sliderWidth = viewportWidth;
export const itemWidth = viewportWidth;
export const mainColor = '#107dac';

const MainStyle = StyleSheet.create({
  bigBlue: {
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 30,
  },
  labelColorRed: {
    color: 'red',
  },
  blueBack: {
    backgroundColor: '#0957a2',
  },
  lightBlueBack: {
    backgroundColor: '#235889',
  },
  labelColor: {
    // color: 'black',
  },
  stepColor: {
    color: '#107dac',
  },
  btnText: {
    color: '#fff',
    padding: 6,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  normalButton: {
    backgroundColor: '#24C4FF',
    color: 'white',
  },
  linkButton: {
    backgroundColor: '#73a2c3',
    color: 'white',
  },
  logoContainer: {
    flex: 1,
    flexGrow: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#107dac',
  },
  buttonItems: {
    backgroundColor: '#107dac',
    marginVertical: 10,
    marginHorizontal: 10,
    height: 100,
  },
  marginV10H32: {
    marginVertical: 10,
    marginHorizontal: 32,
  },
  marginV10: {
    marginVertical: 10,
  },
  marginV20: {
    marginVertical: 20,
  },
  marginV50: {
    marginVertical: 50,
  },
  marginT10: {
    marginTop: 10,
  },
  marginT20: {
    marginTop: 20,
  },
  marginT30: {
    marginTop: 30,
  },
  rowFront: {
    alignItems: 'center',
    backgroundColor: '#FFF',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    height: 50,
    paddingHorizontal: 20,
  },
  rowBack: {
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    height: 50,
    paddingLeft: 15,
  },
  slide: {
    width: sliderWidth,
    height: '100%',
    paddingHorizontal: 0,
    // other styles for the item container
  },
  slideInnerContainer: {
    width: itemWidth,
    flex: 1,
    flexDirection: 'column',
  },
});

export default MainStyle;
