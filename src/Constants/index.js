export const SessionRanges = [
  {label: '500m', value: 500},
  {label: '1Km', value: 1000},
  {label: '3Km', value: 3000},
  {label: '5Km', value: 5000},
  {label: '10Km', value: 10000},
  {label: '20Km', value: 20000},
  {label: '50Km', value: 50000},
  {label: '100Km', value: 100000},
  {label: 'No Limit', value: 0},
];

export const Oxygen = 'Oxygen';

export const GasNames = [
  {label: Oxygen, value: Oxygen},
  {label: 'Carbon Monoxide (CO)', value: 'CO'},
  {label: 'Methane', value: 'Methane'},
  {label: 'Chlorine (CL2)', value: 'CL2'},
  {label: 'Hydrogen Sulfide (H2S)', value: 'H2S'},
  {label: 'Sulfur Dioxide (SO2)', value: 'SO2'},
  {label: 'Nitric Oxide (NO)', value: 'NO'},
  {label: 'Chlorine Dioxide (CLO2)', value: 'CLO2'},
  {label: 'Hydrogen Cyanide (HCN)', value: 'HCN'},
  {label: 'Hydrogen Chloride (HCL)', value: 'HCL'},
  {label: 'Phosphine (PH3)', value: 'PH3'},
  {label: 'Nitrogen Oxide (NO2)', value: 'NO2'},
  {label: 'Hydrogen (H2)', value: 'H2'},
  {label: 'Ammonia (NH3)', value: 'NH3'},
  {label: 'Carbon Dioxide (CO2)', value: 'CO2'},
  {label: 'Gamma', value: 'Gamma'},
  {label: 'VOC', value: 'VOC'},
  {label: 'PID', value: 'PID'},
  {label: 'LEL', value: 'LEL'},
];

export const GasScales = [
  {label: '%'},
  {label: '%LEL'},
  {label: 'PPM'},
  {label: 'PPB'},
  {label: '%VOL'},
  {label: 'μSv'},
  {label: 'μSv/h'},
  {label: 'μRem'},
  {label: 'mRem'},
];

export const DJI = 'DJI';
export const PARROT = 'PARROT';

export const DroneManufactures = [DJI, PARROT];
//export const DroneManufactures = [DJI];
