import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableHighlight,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

export default class GradientButton extends Component {
  render = () => {
    // Get the shadow settings and give them default values
    const {
      setting: {
        btnWidth = 0,
        btnHeight = 0,
        fontSize = 18,
        shadowHeight = 100
      },
      onPressButton,
      btnText,
    } = this.props;

    const styles = StyleSheet.create({
      button: {
        width: btnWidth,
        height: btnHeight,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: btnHeight / 2,
      },
      text: {
        fontSize: fontSize,
        color: '#fff',
      },
      imageView: {
        width: btnWidth,
        height: shadowHeight,
        position: 'absolute',
        top: -8,
        left: -15,
      },
      linearGradient: { position: 'relative' }
    });

    // Return a view ,whose background is a svg picture
    return (
      <View>
        <View
          style={styles.imageView}
        >
          <Image
            source={require('../../img/healer/shadow.png')}
            style={{
              width: btnWidth + 30,
              height: (btnWidth + 30) * 145 / 660,
            }}
          />
        </View>
        <LinearGradient
          start={{x: 0.2, y: 0.4}} end={{x: 1.0, y: 1.0}}
          colors={['rgb(16,125,172)', 'rgb(24,154,211)']}
          style={[styles.button, styles.linearGradient]}>
          <TouchableHighlight
            underlayColor={'rgb(105,105,105)'}
            style={styles.button}
            onPress={onPressButton}>
            <Text style={styles.text}>{btnText}</Text>
          </TouchableHighlight>
        </LinearGradient>
      </View>
    );
  }
}
