/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, Dimensions, TouchableOpacity} from 'react-native';

import {scale, verticalScale} from 'react-native-size-matters';

let screenWidth = Dimensions.get('window').width;

export class ProductCard extends Component {
  constructor(props) {
    super(props);
    this.state = {color: '#900c3f'};
  }
  render() {
    const {title, subTitle, price, image, buttonText} = this.props;
    return (
      <View
        style={{
          backgroundColor: '#f8f9fa',
          alignSelf: 'center',
          margin: 10,
          flexDirection: 'column',
          width: screenWidth - 40,
          borderWidth: 0.2,
          borderRadius: 12,
          elevation: 2,
          shadowColor: '#777',
          shadowOpacity: 0.16,
          shadowRadius: 3,
          shadowOffset: {
            height: 1,
            width: 0,
          },
        }}>
        <View
          style={{
            backgroundColor: '#f8f9fa',
            borderRadius: 12,
            flex: 3,
            height: 150,
            justifyContent: 'center',
            alignItems: 'flex-start',
          }}>
          <Text
            style={{color: '#000', fontSize: scale(25), marginLeft: scale(12)}}>
            {title}
          </Text>

          <Text
            style={{
              color: '#777',
              fontSize: scale(10),
              margin: scale(12),
              textAlign: 'left',
              width: screenWidth - 200,
            }}>
            {subTitle}
          </Text>
          <Text
            style={{
              color: '#000',
              fontSize: scale(15),
              fontWeight: 'bold',
              margin: scale(12),
            }}>
            {price}
          </Text>
        </View>

        <Image
          borderRadius={12}
          source={image}
          style={{
            width: screenWidth - 45,
            height: verticalScale(50),
            resizeMode: 'contain',
          }}
        />
        {this.props.onClickButton ? (
          <TouchableOpacity
            onPress={() => this.props.onClickButton()}
            style={[
              {
                justifyContent: 'center',
                borderColor: '#eee',
                alignItems: 'center',
                alignSelf: 'flex-end',
                paddingLeft: 20,
                paddingRight: 20,
                height: scale(40),
                margin: 30,
                shadowRadius: 5,
                borderRadius: scale(0),
                backgroundColor: 'rgb(16,125,172)',
              },
            ]}>
            <Text
              style={{
                color: '#ffffff',
                fontSize: 12,
                fontWeight: '900',
              }}>
              {buttonText}
            </Text>
          </TouchableOpacity>
        ) : null}
      </View>
    );
  }
}
