import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import {colors} from '../styles/variables';

const DroneModelItem = ({model}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.model}>{model}</Text>
      <Image
        source={require('../../img/camera-drone.png')}
        style={styles.image}
      />
    </View>
  );
};

export default DroneModelItem;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 10,
    borderColor: colors.white,
    padding: 2.5,
    paddingHorizontal: 10,
  },
  model: {
    fontSize: 16,
    alignSelf: 'flex-start',
    color: colors.white,
  },
  image: {
    width: 16,
    height: 16,
    marginLeft: 10,
  },
});
