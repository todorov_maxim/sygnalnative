import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {colors} from '../styles/variables';

const DroneParamItem = ({name, value, extraStyle = {}, innerStyle = {}}) => {
  return (
    <View style={[styles.container, extraStyle]}>
      <Text style={styles.valueText}>{value}</Text>
      <Text style={[styles.nameText, innerStyle]}>{name}</Text>
    </View>
  );
};

export default DroneParamItem;

const styles = StyleSheet.create({
  container: {
    height: 80,
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  valueText: {
    textAlign: 'center',
    fontSize: 16,
    alignSelf: 'center',
    color: colors.white,
  },
  nameText: {
    fontSize: 12,
    alignSelf: 'center',
    color: colors.white,
    position: 'absolute',
    bottom: 0,
  },
});
