import uuid from 'react-native-uuid';
import {decorate, observable, action, computed} from 'mobx';
import {Oxygen} from '../Constants';

class GasItem {
  // static count = 0;
  key = '';
  gasName = '';
  gasScale = '';
  value = 0;
  maxValue = 0;
  lowValue = 0;
  minAlarm = 0;
  maxAlarm = 0;
  timestamp = 0;

  constructor(options) {
    this.key = options ? options.key || uuid.v4() : uuid.v4();
    this.gasName = options ? options.gasName || '' : '';
    this.gasScale = options ? options.gasScale || '' : '';
    this.value = options ? options.value || 0 : 0;
    this.maxValue = options ? options.maxValue || 0 : 0;
    this.lowValue = options ? options.lowValue || 0 : 0;
    this.minAlarm = options ? options.minAlarm || 0 : 0;
    this.maxAlarm = options ? options.maxAlarm || 0 : 0;
    this.timestamp = options ? options.timestamp || Date.now() : Date.now();
  }

  setGasName = gasName => {
    this.gasName = gasName;
    if (this.isOxygen) {
      this.minAlarm = 19.5;
      this.maxAlarm = 23.0;
    } else {
      this.minAlarm = 0;
      this.maxAlarm = 0;
    }
  };
  setGasScale = gasScale => {
    this.gasScale = gasScale;
  };

  tempValue = newValue => {
    this.value = newValue;
  };

  setValue = newValue => {
    this.value = newValue;
    this.lowValue = Math.min(this.lowValue, this.computedValue);
    this.maxValue = Math.max(this.maxValue, this.computedValue);
  };
  setMinAlarm = minAlarm => {
    this.minAlarm = minAlarm;
  };
  setMaxAlarm = maxAlarm => {
    this.maxAlarm = maxAlarm;
  };
  get isOxygen() {
    return this.gasName == Oxygen;
  }
  setTimestamp = timestamp => {
    this.timestamp = timestamp;
  };

  get isAlarmTriggered() {
    return GasItem.checkAlarmTriggered(
      this.minAlarm,
      this.maxAlarm,
      this.value,
    );
  }

  get computedValue() {
    return parseFloat(parseFloat(this.value + 0).toFixed(2));
  }

  get isValueNaN() {
    return isNaN(this.value);
  }

  static checkAlarmTriggered(minAlarm, maxAlarm, value) {
    return minAlarm > value || maxAlarm < value;
  }

  resetPeek = () => {
    this.lowValue = this.computedValue;
    this.maxValue = this.computedValue;
  };
}

decorate(GasItem, {
  gasName: observable,
  gasScale: observable,
  value: observable,
  maxValue: observable,
  lowValue: observable,
  minAlarm: observable,
  maxAlarm: observable,
  setGasName: action,
  setGasScale: action,
  setValue: action,
  tempValue: action,
  setMinAlarm: action,
  setMaxAlarm: action,
  setTimestamp: action,
  isOxygen: computed,
  isAlarmTriggered: computed,
  computedValue: computed,
  isValueNaN: computed,
  resetPeek: action,
});

export default GasItem;
