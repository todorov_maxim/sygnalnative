import uuid from 'react-native-uuid';
import {decorate, observable, action, toJS} from 'mobx';
import {sha512} from 'react-native-sha512';
import firebaseHelper from '../Services/FirebaseHelper';
import {Oxygen, DroneManufactures, DJI} from '../Constants';

import locationHelper from '../Services/LocationHelper';
import {getDistance} from 'geolib';
import SecureStorage, {
  ACCESS_CONTROL,
  ACCESSIBLE,
  AUTHENTICATION_TYPE,
} from 'react-native-secure-storage';

const config = {
  accessible: ACCESSIBLE.ALWAYS,
};

const SaveKey = 'JoinSessionSetup';

class JoinSessionSetup {
  key = '';
  selectedSession = null;
  password = '';
  userName = '';
  range = 500;
  availableSessions = [];
  loading = false;
  pwdHash = '';
  gasItems = [];
  // latitude = null;
  // longitude = null;
  // watchId = null;
  distance = Number.MAX_SAFE_INTEGER;
  isDrone = false;
  droneManufactures = DroneManufactures;
  selectedDroneManufacture = DJI;

  constructor(options) {
    this.key = options ? options.key : uuid.v4();
  }

  setSelectedSession = selectedSession => {
    this.selectedSession = selectedSession;
  };
  setPassword = password => {
    this.password = password;
  };
  setUserName = userName => {
    this.userName = userName;
  };

  setRange = newRange => {
    this.range = newRange;
    this.getAvailableSessions();
  };

  setLoading = loading => {
    this.loading = loading;
  };

  setIsDrone = isDrone => {
    this.isDrone = isDrone;
  };

  isDJIDrone = () => {
    return this.selectedDroneManufacture === DJI;
  };

  setDroneManufactures = droneManufactures => {
    this.droneManufactures = droneManufactures;
  };
  setSelectedDroneManufacture = selectedDroneManufacture => {
    this.selectedDroneManufacture = selectedDroneManufacture;
  };

  setAvailableSessions = availableSessions => {
    this.availableSessions = availableSessions;
    this.distance = Number.MAX_SAFE_INTEGER;
  };
  toDbSession = theValue => {
    return {
      name: theValue.name,
      range: theValue.range,
      latitude: theValue.latitude,
      longitude: theValue.longitude,
      pwdHash: theValue.pwdHash,
      stamp: theValue.stamp,
    };
  };
  getAvailableSessions = async () => {
    try {
      this.availableSessions = [];
      firebaseHelper.getAllSessions(snapshot => {
        const theValue = snapshot.val();
        var availableSessions = [];
        for (const key in theValue) {
          if (theValue.hasOwnProperty(key)) {
            const element = theValue[key];
            const session = this.toDbSession(element);
            const distance = this.computeSessionDistance(session);
            if (
              (session.range == 0 || distance <= session.range) &&
              (this.range == 0 || distance <= this.range)
            )
              availableSessions.push(session);
          }
        }
        this.availableSessions = availableSessions;
      });
    } catch (e) {
      this.availableSessions = [];
    }
  };

  isNameDuplicate = async (sessionName, uid) => {
    let snapshot = await firebaseHelper.getAllSessionsOnce();
    if (snapshot.hasChild(sessionName)) {
      snapshot = await firebaseHelper.getSessionsByName(sessionName);

      let values = snapshot.val();
      if (values.uid == uid) return false;
      return true;
    }
    return false;
  };

  joinSession = async (resolve, reject) => {
    try {
      this.setLoading(true);
      this.pwdHash = await sha512(this.password);

      if (this.selectedSession === null) {
        throw 'no selectedSession';
      }
      if (
        locationHelper.latitude === null ||
        locationHelper.longitude === null
      ) {
        throw 'no GPS location';
      }
      this.computeDistance();
      if (this.pwdHash !== this.selectedSession.pwdHash) {
        throw 'invalid password';
      } else if (!this.userName) {
        throw 'invalid username';
      } else if (
        !this.isDrone &&
        this.selectedSession.range !== 0 &&
        this.distance > this.selectedSession.range
      ) {
        throw 'invalid range';
      } else {
        try {
          let snapshot = await firebaseHelper.getAllGasItems(
            this.selectedSession.name,
          );
          const theValue = snapshot.val();
          let gasItems = [];
          for (const key in theValue) {
            if (theValue.hasOwnProperty(key)) {
              const element = theValue[key];
              const statValue = Oxygen == element.gasName ? 20.9 : 0;
              gasItems.push({
                key: element.key,
                gasName: element.gasName,
                gasScale: element.gasScale,
                value: statValue,
                lowValue: statValue,
                maxValue: statValue,
                minAlarm: element.minAlarm,
                maxAlarm: element.maxAlarm,
                timestamp: element.timestamp,
              });
            }
          }
          this.gasItems = gasItems;
        } catch (e) {
          throw 'error loading gas';
        }
        try {
          let theObject = this;
          await firebaseHelper.joinSession(theObject);
          this.setLoading(false);
          resolve();
        } catch (e) {
          throw 'error creating user';
        }
      }
    } catch (error) {
      this.setLoading(false);
      reject(error);
    }
  };

  /*startLocationUpdates = (onSuccess, errorCallback) => {
    var isSuccessCalled = false;
    this.removeLocationUpdates();
    this.watchId = locationHelper.startLocationUpdates(
      position => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        if (!isSuccessCalled) {
          isSuccessCalled = true;
          onSuccess && onSuccess();
        }
      },
      error => {
        errorCallback(error);
      },
    );
  };

  removeLocationUpdates = () => {
    if (this.watchId !== null) {
      locationHelper.removeLocationUpdates(this.watchId);
      this.watchId = null;
    }
  };*/

  computeDistance = () => {
    if (!locationHelper.latitude || !locationHelper.longitude) return 0;

    this.distance = getDistance(
      {latitude: locationHelper.latitude, longitude: locationHelper.longitude},
      {
        latitude: this.selectedSession.latitude,
        longitude: this.selectedSession.longitude,
      },
    );
  };

  computeSessionDistance = session => {
    if (!locationHelper.latitude || !locationHelper.longitude) return 0;

    return getDistance(
      {latitude: locationHelper.latitude, longitude: locationHelper.longitude},
      {
        latitude: session.latitude,
        longitude: session.longitude,
      },
    );
  };

  save = async () => {
    let saveObject = {
      key: this.key,
      userName: this.userName,
      password: this.password,
      version: 1,
    };

    try {
      await SecureStorage.setItem(SaveKey, JSON.stringify(saveObject), config);
    } catch (error) {
      // Error saving data
    }
  };

  load = async () => {
    try {
      let saveObjectString = await SecureStorage.getItem(SaveKey, config);
      if (saveObjectString) {
        var saveObject = JSON.parse(saveObjectString);
        if (saveObject && saveObject.version == 1) {
          // gasItems = [];
          this.key = saveObject.key;
          this.userName = saveObject.userName;
          this.password = saveObject.password;
        }
      }
    } catch (error) {
      console.log('error', error);
    }
  };
}
decorate(JoinSessionSetup, {
  selectedSession: observable,
  password: observable,
  userName: observable,
  range: observable,
  availableSessions: observable,
  loading: observable,
  pwdHash: observable,
  gasItems: observable,
  // latitude: observable,
  // longitude: observable,
  distance: observable,
  isDrone: observable,
  droneManufactures: observable,
  selectedDroneManufacture: observable,
  setSelectedSession: action,
  setPassword: action,
  setUserName: action,
  setRange: action,
  setAvailableSessions: action,
  getAvailableSessions: action,
  joinSession: action,
  // startLocationUpdates: action,
  // removeLocationUpdates: action,
  computeDistance: action,
  computeSessionDistance: action,
  setLoading: action,
  setIsDrone: action,
  setDroneManufactures: action,
  setSelectedDroneManufacture: action,
});

export default JoinSessionSetup;
