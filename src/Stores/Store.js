// importing observables and decorate
import {decorate, observable, action} from 'mobx';
import {Linking} from 'react-native';

import SessionSetup from './SessionSetup';
import JoinSessionSetup from './JoinSessionSetup';
import HostGameViewModel from './HostGameViewModel';
import UserManagerViewModel from './UserManagerViewModel';
import IncidentViewModel from './IncidentViewModel';
import LoginScreenViewModel from './LoginScreenViewModel';
import LicenseViewModel from './LicenseViewModel';
import HostMapViewModel from './HostMapViewModel';

class Store {
  // observable to save search query
  text = 'Testing Store';
  text2 = 'Testing Store2';
  // action to update text
  updateText = text => {
    this.text = text;
  };

  // observable to save image response from api
  data = null;

  // observables can be modifies by an action only
  setData = data => {
    this.data = data;
  };

  openWebsite = () => {
    const link = 'https://www.sygnalapp.com';
    Linking.canOpenURL(link).then(supported => {
      if (supported) {
        Linking.openURL(link);
      }
    });
  };

  sessionSetup = new SessionSetup();

  joinSessionSetup = new JoinSessionSetup();

  //TODO need to clean up
  hostGameViewModel = new HostGameViewModel();
  userManagerViewModel = new UserManagerViewModel();
  incidentViewModel = new IncidentViewModel();
  loginScreenViewModel = new LoginScreenViewModel();
  licenseViewModel = new LicenseViewModel();
  hostMapViewModel = new HostMapViewModel();
}

// another way to decorate variables with observable
decorate(Store, {
  text: observable,
  text2: observable,
  sessionSetup: observable,
  joinSessionSetup: observable,
  hostGameViewModel: observable,
  userManagerViewModel: observable,
  incidentViewModel: observable,
  loginScreenViewModel: observable,
  licenseViewModel: observable,
  hostMapViewModel: observable,
  updateText: action,
  data: observable,
  searchImage: action,
  setData: action,
  openWebsite: action,
});

// export class
export default new Store();
