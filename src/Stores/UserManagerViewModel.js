import {decorate, observable, action} from 'mobx';
import firebaseHelper from '../Services/FirebaseHelper';
import UserGameViewModel from './UserGameViewModel';
import locationHelper from '../Services/LocationHelper';
import {Vibration} from 'react-native';
import Sound from 'react-native-sound';

import GasItem from './GasItem';
import UserInfoRecord from './UserInfoRecord';

class UserManagerViewModel {
  usersData = null;
  sessionName;
  userKey;
  showPeek = false;
  droneConnectionStatus = '';

  deleteUserDataOnDisconnect = null;
  deleteUserInfoRecordOnDisconnect = null;

  setUserKey = userKey => {
    this.userKey = userKey;
  };

  setSessionName = sessionName => {
    this.sessionName = sessionName;
  };

  setDroneConnectionStatus = droneConnectionStatus => {
    this.droneConnectionStatus = droneConnectionStatus;
  };

  registerToUserDataUpdateCallback = null;
  registerToUserDataUpdate = sessionDeletedAction => {
    this.registerToUserDataUpdateCallback = snapshot =>
      this.onValueChange(snapshot, sessionDeletedAction);
    firebaseHelper.registerToUserDataUpdate(
      this.sessionName,
      this.userKey,
      this.registerToUserDataUpdateCallback,
    );
  };
  unRegisterToUserDataUpdate = () => {
    firebaseHelper.unRegisterToUserDataUpdate(
      this.sessionName,
      this.userKey,
      this.registerToUserDataUpdateCallback,
    );
  };
  onValueChange = (snapshot, sessionDeletedAction) => {
    const theValue = snapshot.val();
    if (theValue !== null) {
      if (theValue.hasOwnProperty('Info')) {
        var gas = [];
        var isAlarmTriggered = false;
        for (const gasItemKey in theValue.Gas) {
          const gasItem = theValue.Gas[gasItemKey];
          isAlarmTriggered =
            isAlarmTriggered ||
            GasItem.checkAlarmTriggered(
              gasItem.minAlarm,
              gasItem.maxAlarm,
              gasItem.value,
            );
          gas.push(gasItem);
        }
        gas = gas.sort((a, b) => a.timestamp > b.timestamp);
        const userGameViewModel = new UserGameViewModel({
          key: this.userKey,
          Info: theValue.Info,
          Gas: gas,
        });
        this.usersData = userGameViewModel;

        if (isAlarmTriggered) {
          this.triggerGasAlarm();
        } else {
          this.stopGasAlarm();
        }
      }
    } else {
      this.stopGasAlarm();
      sessionDeletedAction();
    }
  };

  alarmSound = null;
  triggerGasAlarm = () => {
    if (this.alarmSound !== null) {
      return;
    }
    Vibration.vibrate([0, 2000], true);

    Sound.setCategory('Playback');
    // Load the sound file 'whoosh.mp3' from the app bundle
    // See notes below about preloading sounds within initialization code below.
    this.alarmSound = new Sound(require('../Sounds/alarm.m4a'), error => {
      if (error) {
        console.log('failed to load the sound', error);
        return;
      }
      // loaded successfully
      console.log(
        'duration in seconds: ' +
          this.alarmSound.getDuration() +
          'number of channels: ' +
          this.alarmSound.getNumberOfChannels(),
      );

      this.alarmSound.setNumberOfLoops(-1);

      // Play the sound with an onEnd callback
      this.alarmSound.play(success => {
        if (success) {
          console.log('successfully finished playing');
        } else {
          console.log('playback failed due to audio decoding errors');
        }
      });
    });
  };

  stopGasAlarm = () => {
    Vibration.cancel();
    this.stopSound();
  };

  stopSound = () => {
    if (this.alarmSound != null) {
      // Stop the sound and rewind to the beginning
      this.alarmSound.stop();
      // Release the audio player resource
      this.alarmSound.release();
      this.alarmSound = null;
    }
  };

  startLocationUpdates = () => {
    // this.removeLocationUpdates();
    locationHelper.setLocationUpdate(
      'UserManagerViewModel',
      ({latitude, longitude}) => {
        if (this.usersData !== null) {
          this.usersData.Info.setLatitude(latitude);
          this.usersData.Info.setLongitude(longitude);

          firebaseHelper.updateUserInfo(
            this.sessionName,
            this.userKey,
            this.usersData.Info,
          );

          firebaseHelper.updateUserInfoRecord(
            this.sessionName,
            this.userKey,
            new UserInfoRecord(this.usersData.Info),
          );
        }
      },
    );
  };

  removeLocationUpdates = () => {
    // if (this.watchId !== null) {
    locationHelper.removeUpdateCallback('UserManagerViewModel');
    // this.watchId = null;
    // }
  };

  onStatusUpdateSubscription = null;
  onLoginSubscription = null;
  onLogoutSubscription = null;
  onDownloadProgressSubscription = null;
  onTelemetryAircraftSubscription = null;
  onDroneModelSubscription = null;
  onConnectionSubscription = null;
  onGPSTelemetrySubscription = null;
  onSpeedometerSubscription = null;

  updateSpeedInfo = speed => {
    try {
      if (this.usersData != null) {
        this.usersData.Info.setVelocityX(
          speed.rightSpeed ? speed.rightSpeed : 0,
        );
        this.usersData.Info.setVelocityY(speed.downSpeed ? speed.downSpeed : 0);
        this.usersData.Info.setVelocityZ(
          speed.forwardSpeed ? speed.forwardSpeed : 0,
        );

        firebaseHelper.updateUserInfo(
          this.sessionName,
          this.userKey,
          this.usersData.Info,
        );
        firebaseHelper.updateUserInfoRecord(
          this.sessionName,
          this.userKey,
          new UserInfoRecord(this.usersData.Info),
        );
      }
    } catch (e) {
      console.log('error', e);
    }
  };

  updateTelemetryInfo = location => {
    try {
      if (this.usersData != null) {
        this.usersData.Info.setLatitude(
          location.latitude ? location.latitude : 0,
        );
        this.usersData.Info.setLongitude(
          location.longitude ? location.longitude : 0,
        );

        this.usersData.Info.setAltitude(
          location.absoluteAltitude ? location.absoluteAltitude : 0,
        );
        firebaseHelper.updateUserInfo(
          this.sessionName,
          this.userKey,
          this.usersData.Info,
        );
        firebaseHelper.updateUserInfoRecord(
          this.sessionName,
          this.userKey,
          new UserInfoRecord(this.usersData.Info),
        );
      }
    } catch (e) {
      console.log('error', e);
    }
  };

  updateDroneInfo = drone => {
    try {
      if (!drone) return;
      if (this.usersData !== null) {
        if (
          drone.model === this.usersData.Info.droneModel ||
          this.usersData.Info.droneConnectionStatus == drone.state
        )
          return;
        this.usersData.Info.setDroneModel(drone.model ? drone.model : '');
        this.usersData.Info.setDroneConnectionStatus(
          drone.state ? drone.state : 'UNKNOWN',
        );
        firebaseHelper.updateUserInfo(
          this.sessionName,
          this.userKey,
          this.usersData.Info,
        );
        firebaseHelper.updateUserInfoRecord(
          this.sessionName,
          this.userKey,
          new UserInfoRecord(this.usersData.Info),
        );
      }
    } catch (e) {
      console.log(e);
    }
  };

  // TODO: Why does this useless chunk of code exist? Fix the core issue!
  removeDJIDroneData = () => {
    //DjisdkReactNative.stop();
    // this.onStatusUpdateSubscription.remove();
    // this.onStatusUpdateSubscription.remove();
    //this.onLoginSubscription.remove();
    //this.onLogoutSubscription.remove();
    //this.onDownloadProgressSubscription.remove();
    //this.onTelemetryAircraftSubscription.remove();
  };

  removeParrotDroneData = () => {
    clearInterval(this.onDroneModelSubscription);
    clearInterval(this.onGPSTelemetrySubscription);
    clearInterval(this.onSpeedometerSubscription);
    this.onDroneModelSubscription = null;
    this.onGPSTelemetrySubscription = null;
    this.onSpeedometerSubscription = null;
  };

  onDisconnectDeleteUserData = () => {
    if (this.deleteUserDataOnDisconnect) {
      this.deleteUserDataOnDisconnect.cancel();
      this.deleteUserDataOnDisconnect = null;
    }
    if (this.deleteUserInfoRecordOnDisconnect) {
      this.deleteUserInfoRecordOnDisconnect.cancel();
      this.deleteUserInfoRecordOnDisconnect = null;
    }

    this.deleteUserDataOnDisconnect = firebaseHelper.onDisconnectDeleteUserData(
      this.sessionName,
      this.userKey,
    );

    this.deleteUserInfoRecordOnDisconnect = firebaseHelper.onDisconnectDeleteUserInfoRecord(
      this.sessionName,
      this.userKey,
    );
  };

  deleteUserData = () => {
    if (this.deleteUserDataOnDisconnect) {
      this.deleteUserDataOnDisconnect.cancel();
      this.deleteUserDataOnDisconnect = null;
    }
    if (this.deleteUserInfoRecordOnDisconnect) {
      this.deleteUserInfoRecordOnDisconnect.cancel();
      this.deleteUserInfoRecordOnDisconnect = null;
    }

    return firebaseHelper.deleteUserData(this.sessionName, this.userKey);
  };

  registerToUserMessageUpdateCallback = null;
  registerToUserMessageUpdate = displayMsgCallback => {
    this.registerToUserMessageUpdateCallback = snapshot =>
      this.messageOnValueChange(snapshot, displayMsgCallback);

    return firebaseHelper.registerToUserMessageUpdate(
      this.sessionName,
      this.userKey,
      this.registerToUserMessageUpdateCallback,
    );
  };

  unRegisterToUserMessageUpdate = () => {
    return firebaseHelper.unRegisterToUserMessageUpdate(
      this.sessionName,
      this.userKey,
      this.registerToUserMessageUpdateCallback,
    );
  };

  messageMapSeen = new Map();
  messageOnValueChange = (snapshot, displayMsgCallback) => {
    const theValue = snapshot.val();
    if (theValue !== null) {
      for (const msgKey in theValue) {
        if (!this.messageMapSeen.has(msgKey)) {
          this.messageMapSeen.set(msgKey);
          displayMsgCallback(msgKey, theValue[msgKey].message);
        }
      }
    }
  };

  removeUserMessage = messageKey => {
    return firebaseHelper.removeUserMessage(
      this.sessionName,
      this.userKey,
      messageKey,
    );
  };

  toggleShowPeek = () => {
    this.showPeek = !this.showPeek;
  };

  resetPeek = () => {
    if (this.usersData && this.usersData.Gas) {
      this.usersData.Gas.forEach(gasItem => {
        gasItem.resetPeek();
      });
    }
  };
}
decorate(UserManagerViewModel, {
  sessionName: observable,
  usersData: observable,
  userKey: observable,
  messageMapSeen: observable,
  showPeek: observable,
  droneConnectionStatus: observable,
  deleteUserDataOnDisconnect: observable,
  deleteUserInfoRecordOnDisconnect: observable,
  registerToUserDataUpdate: action,
  unRegisterToUserDataUpdate: action,
  onValueChange: action,
  setSessionName: action,
  setUserKey: action,
  startLocationUpdates: action,
  removeLocationUpdates: action,
  triggerGasAlarm: action,
  stopGasAlarm: action,
  stopSound: action,
  deleteUserData: action,
  registerToUserMessageUpdate: action,
  unRegisterToUserMessageUpdate: action,
  removeUserMessage: action,
  toggleShowPeek: action,
  setDroneConnectionStatus: action,
  onDisconnectDeleteUserData: action,
  resetPeek: action,
  updateSpeedInfo: action,
  updateTelemetryInfo: action,
  updateDroneInfo: action,
});

export default UserManagerViewModel;
