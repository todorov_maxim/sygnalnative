import {decorate, observable, action} from 'mobx';

class UserInfo {
  key = '';
  userName = '';
  latitude = 0;
  longitude = 0;
  altitude = 0;
  velocityX = 0;
  velocityY = 0;
  velocityZ = 0;
  droneModel = '';
  droneConnectionStatus = '';
  timestamp = 0;
  isDrone = false;

  constructor(options) {
    this.key = options ? options.key || '' : '';
    this.userName = options ? options.userName || '' : '';
    this.latitude = options ? options.latitude || 0 : 0;
    this.longitude = options ? options.longitude || 0 : 0;
    this.altitude = options ? options.altitude || 0 : 0;
    this.velocityX = options ? options.velocityX || 0 : 0;
    this.velocityY = options ? options.velocityY || 0 : 0;
    this.velocityZ = options ? options.velocityZ || 0 : 0;
    this.droneModel = options ? options.droneModel || 'UNKNOWN' : 'UNKNOWN';
    this.droneConnectionStatus = options
      ? options.droneConnectionStatus || 'DISCONNECTED'
      : 'DISCONNECTED';
    this.timestamp = options ? options.timestamp || Date.now() : Date.now();
    this.isDrone = options ? options.isDrone || false : false;
  }

  setUserName = userName => {
    this.userName = userName;
    this.timestamp = Date.now();
  };
  setLatitude = latitude => {
    this.latitude = latitude;
    this.timestamp = Date.now();
  };
  setLongitude = longitude => {
    this.longitude = longitude;
    this.timestamp = Date.now();
  };
  setTimestamp = timestamp => {
    this.timestamp = timestamp;
  };
  setAltitude = altitude => {
    this.altitude = altitude;
    this.timestamp = Date.now();
  };
  setVelocityX = velocityX => {
    this.velocityX = velocityX;
    this.timestamp = Date.now();
  };
  setVelocityY = velocityY => {
    this.velocityY = velocityY;
    this.timestamp = Date.now();
  };
  setVelocityZ = velocityZ => {
    this.velocityZ = velocityZ;
    this.timestamp = Date.now();
  };
  setDroneModel = droneModel => {
    this.droneModel = droneModel;
    this.timestamp = Date.now();
  };
  setDroneConnectionStatus = droneConnectionStatus => {
    this.droneConnectionStatus = droneConnectionStatus;
    this.timestamp = Date.now();
  };
  setIsDrone = isDrone => {
    this.isDrone = isDrone;
  };
}
decorate(UserInfo, {
  userName: observable,
  latitude: observable,
  longitude: observable,
  altitude: observable,
  velocityX: observable,
  velocityY: observable,
  velocityZ: observable,
  droneModel: observable,
  droneConnectionStatus: observable,
  timestamp: observable,
  isDrone: observable,
  setUserName: action,
  setLatitude: action,
  setLongitude: action,
  setTimestamp: action,
  setAltitude: action,
  setVelocityX: action,
  setVelocityY: action,
  setVelocityZ: action,
  setDroneModel: action,
  setDroneConnectionStatus: action,
  setIsDrone: action,
});

export default UserInfo;
