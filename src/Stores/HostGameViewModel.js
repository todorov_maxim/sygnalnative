import {decorate, observable, action, computed} from 'mobx';
import firebaseHelper from '../Services/FirebaseHelper';
import UserGameViewModel from './UserGameViewModel';
import UserInfoRecord from './UserInfoRecord';

import MapView, {Marker, Polyline} from 'react-native-maps';

class HostGameViewModel {
  usersData = [];
  activeUserData = 0;
  sessionName;
  showSendMgsDialog = false;
  messageToSend = '';

  showUserPath = false;
  userInfoRecordUpdateCallBacksMap = {};
  userInfoRecordUpdateMap = {};

  userInfoRecordColors = {};
  static colorsCount = 0;
  colors = ['#A93226', '#9B59B6', '#2980B9', '#1ABC9C', '#16A085', '#F1C40F'];

  userPolylineMap = {};

  removeFromSessionListOnDisconnect = null;
  removeFromSessionRecordsOnDisconnect = null;

  setUserPolylineMap = userPolylineMap => {
    this.userPolylineMap = userPolylineMap;
  };

  getUserInfoRecordColors = userkey => {
    let color = this.userInfoRecordColors[userkey];
    return color;
  };

  setShowUserPath = showUserPath => {
    this.showUserPath = showUserPath;
  };

  setUserInfoRecordUpdateMap = userInfoRecordUpdateMap => {
    this.userInfoRecordUpdateMap = userInfoRecordUpdateMap;
  };

  setSessionName = sessionName => {
    this.sessionName = sessionName;
  };
  registerToAllUserDataUpdate = () => {
    firebaseHelper.registerToAllUserDataUpdate(
      this.sessionName,
      this.onUserDataValueChange,
    );
  };
  unRegisterToAllUserDataUpdate = () => {
    firebaseHelper.unRegisterToAllUserDataUpdate(
      this.sessionName,
      this.onUserDataValueChange,
    );
  };
  onUserDataValueChange = snapshot => {
    const theValue = snapshot.val();
    const usersData = [];
    for (const theKey in theValue) {
      if (theValue.hasOwnProperty(theKey)) {
        const element = theValue[theKey];
        const gas = [];
        for (const gasItemKey in element.Gas) {
          const gasItem = element.Gas[gasItemKey];
          gas.push(gasItem);
        }

        // I Hate you javascript! Find me a way to preserve the order of Objects without having to do this?
        //  Or can we which im not aware of?

        if (gas) {
          gas.sort(function(a, b) {
            return a.timestamp - b.timestamp;
          });
        }

        let userGameViewModel = new UserGameViewModel({
          key: theKey,
          Info: element.Info,
          Gas: gas,
        });
        usersData.push(userGameViewModel);
      }
    }
    this.usersData = usersData;

    this.updateUserInfoRecordWatch();
  };

  updateUserInfoRecordWatch = () => {
    const oldUpdateCallBacksMap = this.userInfoRecordUpdateCallBacksMap;
    const userInfoRecordUpdateCallBacksMap = {};
    this.usersData.forEach(element => {
      if (oldUpdateCallBacksMap[element.key]) {
        userInfoRecordUpdateCallBacksMap[element.key] =
          oldUpdateCallBacksMap[element.key];
        delete oldUpdateCallBacksMap[element.key];
      } else {
        userInfoRecordUpdateCallBacksMap[element.key] = element.key;
        firebaseHelper.registerToUserInfoRecordUpdate(
          this.sessionName,
          element.key,
          this.onUserInfoRecordChildAdded,
        );
      }
    });

    for (const theKey in oldUpdateCallBacksMap) {
      firebaseHelper.unRegisterToUserInfoRecordUpdate(
        this.sessionName,
        theKey,
        this.onUserInfoRecordChildAdded,
      );
    }

    this.userInfoRecordUpdateCallBacksMap = userInfoRecordUpdateCallBacksMap;
  };

  onUserInfoRecordChildAdded = (snapshot, prevChildKey) => {
    var theValue = snapshot.val();
    var userInfoRecordUpdateMap = this.userInfoRecordUpdateMap;
    if (theValue !== null) {
      const userInfoRecord = new UserInfoRecord(theValue);
      if (userInfoRecordUpdateMap[userInfoRecord.key]) {
        userInfoRecordUpdateMap[userInfoRecord.key].push(userInfoRecord);
      } else {
        userInfoRecordUpdateMap[userInfoRecord.key] = [];

        this.userInfoRecordColors[userInfoRecord.key] = this.colors[
          HostGameViewModel.colorsCount
        ];
        HostGameViewModel.colorsCount =
          (HostGameViewModel.colorsCount + 1) % this.colors.length;
        userInfoRecordUpdateMap[userInfoRecord.key].push(userInfoRecord);
      }

      const userPolylineMap = this.userPolylineMap;
      if (!userPolylineMap[userInfoRecord.key]) {
        userPolylineMap[userInfoRecord.key] = [];
      }
      if (userInfoRecord.latitude != 0 && userInfoRecord.longitude != 0) {
        userPolylineMap[userInfoRecord.key].push({
          latitude: userInfoRecord.latitude,
          longitude: userInfoRecord.longitude,
        });
      }
      this.userPolylineMap = userPolylineMap;
      this.userInfoRecordUpdateMap = userInfoRecordUpdateMap;
    }
  };

  registerToAllSessionGasUpdateCallback = null;
  registerToAllSessionGasUpdate = sessionDeletedAction => {
    this.registerToAllSessionGasUpdateCallback = snapshot =>
      this.onSessionGasValueChange(snapshot, sessionDeletedAction);

    firebaseHelper.registerToAllSessionGasUpdate(
      this.sessionName,
      this.registerToAllSessionGasUpdateCallback,
    );
  };

  unRegisterToAllSessionGasUpdate = () => {
    firebaseHelper.unRegisterToAllSessionGasUpdate(
      this.sessionName,
      this.registerToAllSessionGasUpdateCallback,
    );
  };

  onSessionGasValueChange = (snapshot, sessionDeletedAction) => {
    const theValue = snapshot.val();
    var keysLength = 0;
    if (theValue !== null) {
      var keys = Object.keys(theValue);
      keysLength = keys.length;
    }

    if (keysLength <= 0) {
      sessionDeletedAction();
    }
  };

  get usersDataArray() {
    return this.usersData.slice();
  }

  get activeUserGameViewModel() {
    if (this.usersData) {
      return this.usersData[this.activeUserData];
    } else {
      return null;
    }
  }

  updateUserGasItem = (userGameViewModel, gasItem) => {
    if (!gasItem.isValueNaN) {
      return firebaseHelper.updateUserGasItem(
        this.sessionName,
        userGameViewModel.key,
        gasItem,
      );
    }
  };

  deleteSessionOnDisconnect = () => {
    if (this.removeFromSessionListOnDisconnect) {
      this.removeFromSessionListOnDisconnect.cancel();
      this.removeFromSessionListOnDisconnect = null;
    }
    if (this.removeFromSessionRecordsOnDisconnect) {
      this.removeFromSessionRecordsOnDisconnect.cancel();
      this.removeFromSessionRecordsOnDisconnect = null;
    }

    this.removeFromSessionListOnDisconnect = firebaseHelper.onDisconnectRemoveFromSessionList(
      this.sessionName,
    );
    this.removeFromSessionRecordsOnDisconnect = firebaseHelper.onDisconnectRemoveFromSessionRecords(
      this.sessionName,
    );
  };

  deleteSession = () => {
    if (this.removeFromSessionListOnDisconnect) {
      this.removeFromSessionListOnDisconnect.cancel();
      this.removeFromSessionListOnDisconnect = null;
    }
    if (this.removeFromSessionRecordsOnDisconnect) {
      this.removeFromSessionRecordsOnDisconnect.cancel();
      this.removeFromSessionRecordsOnDisconnect = null;
    }
    return firebaseHelper.deleteSession(this.sessionName);
  };

  addUserMessage = userKey => {
    let theMessage = this.messageToSend;
    this.messageToSend = '';
    return firebaseHelper.addUserMessage(this.sessionName, userKey, theMessage);
  };

  setShowSendMgsDialog = showSendMgsDialog => {
    this.showSendMgsDialog = showSendMgsDialog;
  };

  setMessageToSend = messageToSend => {
    this.messageToSend = messageToSend;
  };
}
decorate(HostGameViewModel, {
  sessionName: observable,
  usersData: observable,
  activeUserData: observable,
  showSendMgsDialog: observable,
  messageToSend: observable,
  userInfoRecordUpdateMap: observable,
  showUserPath: observable,
  userPolylineMap: observable,
  userInfoRecordColors: observable,
  removeFromSessionListOnDisconnect: observable,
  removeFromSessionRecordsOnDisconnect: observable,
  registerToUserDataUpdate: action,
  unRegisterToUserDataUpdate: action,
  onUserDataValueChange: action,
  setSessionName: action,
  usersDataArray: computed,
  registerToAllSessionGasUpdate: action,
  unRegisterToAllSessionGasUpdate: action,
  onSessionGasValueChange: action,
  deleteSession: action,
  addUserMessage: action,
  activeUserGameViewModel: computed,
  setShowSendMgsDialog: action,
  setMessageToSend: action,
  setUserInfoRecordUpdateMap: action,
  onUserInfoRecordChildAdded: action,
  setShowUserPath: action,
  getUserInfoRecordColors: action,
  setUserPolylineMap: action,
  deleteSessionOnDisconnect: action,
});

export default HostGameViewModel;
