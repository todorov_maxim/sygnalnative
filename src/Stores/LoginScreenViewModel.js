import {decorate, observable, action, toJS} from 'mobx';

import auth from '@react-native-firebase/auth';
import firebaseHelper from '../Services/FirebaseHelper';
import cryptlexHelper from '../Services/CryptlexHelper';
import Toast from 'react-native-root-toast';
import crashlytics from '@react-native-firebase/crashlytics';

class LoginScreenViewModel {
  email = '';
  password = '';
  confirmPassword = '';
  loading = false;
  firebaseUser = null;
  userAuthData = null;
  onAuthStateChangedSubscriber = null;
  userStateChangedCallback = null;
  isActivated = true; //Disable License activation
  licenseId = null;
  activationTimer = 0;
  trialDuration = 86400000; // 24 hrs in milliseconds
  createdAt = -1;

  setUserAuthData = userAuthData => {
    this.userAuthData = userAuthData;
  };

  onAuthStateChanged = user => {
    if (user) {
      this.firebaseUser = user;
      this.registerToAuthDataCallback();
    } else {
      this.unRegisterToAuthData();
      this.firebaseUser = null;
    }
  };

  registerToAuthDataCallback = snapshot => {
    if (snapshot) {
      const theValue = snapshot.val();
      if (theValue !== null) {
        this.setUserAuthData(theValue);
      } else {
        this.setUserAuthData(null);
      }
    } else {
      this.setUserAuthData(null);
    }
  };

  registerToAuthData = () => {
    this.unRegisterToAuthData();
    if (this.firebaseUser) {
      firebaseHelper.registerToAuthData(
        this.firebaseUser.uid,
        this.registerToAuthDataCallback,
      );
    }
  };

  unRegisterToAuthData = () => {
    if (this.firebaseUser) {
      firebaseHelper.unRegisterToAuthData(
        this.firebaseUser.uid,
        this.registerToAuthDataCallback,
      );
    }
  };

  startListeningToStateChanged = () => {
    if (this.onAuthStateChangedSubscriber) {
      this.onAuthStateChangedSubscriber();
    }
    this.onAuthStateChangedSubscriber = auth().onAuthStateChanged(
      this.onAuthStateChanged,
    );
  };

  stopListenToStateChanged = () => {
    this.onAuthStateChangedSubscriber();
  };

  register = () => {
    return new Promise(async (resolve, reject) => {
      try {
        if (!this.email && !this.password) {
          reject('invalid email or password');
        } else if (this.password !== this.confirmPassword) {
          reject('Confirm password does not match');
        } else {
          this.setLoading(true);
          const userCredential = await auth().createUserWithEmailAndPassword(
            this.email,
            this.password,
          );
          const userData = userCredential.user;
          const userAuthData = {
            uid: userData.uid,
            email: userData.email,
            providerId: userData.providerId,
            isAnonymous: userData.isAnonymous,
            emailVerified: userData.emailVerified,
            license: null,
            isTrialLicense: false,
            createdAt: Date.now(),
          };
          this.setUserAuthData(userAuthData);
          await firebaseHelper.setAuthData(userAuthData);
          await auth().currentUser.sendEmailVerification();
          this.setFirebaseUser(userCredential.user);
          resolve(userCredential);
        }
      } catch (error) {
        reject(error.message);
      } finally {
        this.setLoading(false);
      }
    });
  };

  sendEmailVerification = async () => {
    try {
      await auth().currentUser.sendEmailVerification();
    } catch (e) {
      console.log('error', e);
    }
  };

  signIn = () => {
    return new Promise(async (resolve, reject) => {
      try {
        if (!this.email && !this.password) {
          reject('invalid email or password');
        } else {
          this.setLoading(true);
          const userCredential = await auth().signInWithEmailAndPassword(
            this.email,
            this.password,
          );
          const userData = userCredential.user;
          crashlytics().log('User signed in.');
          await crashlytics().setUserId(userData.uid);
          await crashlytics().setAttribute('email', String(userData.email));
          const userAuthData = {
            uid: userData.uid,
            email: userData.email,
            providerId: userData.providerId,
            isAnonymous: userData.isAnonymous,
            emailVerified: userData.emailVerified,
          };
          await firebaseHelper.setAuthData(userAuthData);
          this.setFirebaseUser(userCredential.user);
          resolve(userCredential);
        }
      } catch (error) {
        console.log(error.message);
        reject(error.message);
      } finally {
        this.setLoading(false);
      }
    });
  };

  signOut = () => {
    return new Promise(async (resolve, reject) => {
      try {
        this.setLoading(false);
        await auth().signOut();
        resolve();
      } catch (error) {
        console.log(error.message);
        reject(error.message);
      } finally {
        this.setLoading(false);
      }
    });
  };

  resetPassword = () => {
    return new Promise((resolve, reject) => {
      if (!this.email) {
        reject('invalid email');
      } else {
        this.setLoading(true);
        auth()
          .sendPasswordResetEmail(this.email)
          .then(
            () => {
              this.setLoading(false);
              resolve();
            },
            error => {
              this.setLoading(false);
              console.log(error.message);
              reject(error.message);
            },
          );
      }
    });
  };

  setFirebaseUser = firebaseUser => {
    this.firebaseUser = firebaseUser;
  };

  setEmail = email => {
    this.email = email;
  };

  setPassword = password => {
    this.password = password;
  };

  setConfirmPassword = confirmPassword => {
    this.confirmPassword = confirmPassword;
  };

  setLoading = loading => {
    this.loading = loading;
  };

  goOnline = () => {
    return firebaseHelper.goOnline();
  };

  isOnTrial = () => {
    return Date.now() - this.createdAt < this.trialDuration;
  };

  checkActivation = async cb => {
    const {uid} = this.firebaseUser;
    if (this.createdAt == -1) {
      try {
        this.createdAt = await firebaseHelper.getUserCreatedAt(uid);
      } catch (e) {
        console.log('error', e);
      }
    }
    const activateResult = await cryptlexHelper.checkActivation(uid);
    this.isActivated = activateResult.status;
    this.licenseId = activateResult.licenseId;
    if (!this.isActivated) this.deActivation();
    cb && cb(this.isActivated);
  };

  startCheckActivation = async () => {
    if (!this.isActivated) return;

    this.activationTimer && clearInterval(this.activationTimer);
    this.checkActivation();
    this.activationTimer = setInterval(() => {
      this.checkActivation();
    }, 1000 * 60 * 5); //1 minute
  };

  setActivation = () => {
    this.isActivated = true;
    this.startCheckActivation();
  };

  deActivation = (isShowToast = true) => {
    this.isActivated = false;
    this.licenseId = null;
    this.activationTimer && clearInterval(this.activationTimer);
    this.activationTimer = 0;

    if (isShowToast) {
      if (this.isOnTrial())
        Toast.show('Trial license is now active for 24hrs.');
      else Toast.show('License is deactivated. Please activate in Purchase.');
    }
  };
}
decorate(LoginScreenViewModel, {
  email: observable,
  password: observable,
  confirmPassword: observable,
  loading: observable,
  firebaseUser: observable,
  userAuthData: observable,
  setEmail: action,
  setPassword: action,
  setConfirmPassword: action,
  setLoading: action,
  setFirebaseUser: action,
  startListeningToStateChanged: action,
  stopListenToStateChanged: action,
  register: action,
  signIn: action,
  resetPassword: action,
  sendEmailVerification: action,
  setUserAuthData: action,
  signOut: action,
  goOnline: action,
  checkActivation: action,
  startCheckActivation: action,
  setActivation: action,
  deActivation: action,
});
export default LoginScreenViewModel;
