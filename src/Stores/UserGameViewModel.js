import {decorate, observable, action, computed} from 'mobx';
import firebaseHelper from '../Services/FirebaseHelper';
import uuid from 'react-native-uuid';
import UserInfo from './UserInfo';
import GasItem from './GasItem';

class UserGameViewModel {
  key = '';
  Info = null;
  Gas = [];

  constructor(options) {
    this.key = options ? options.key || uuid.v4() : uuid.v4();
    this.Info = new UserInfo(options ? options.Info : undefined);
    if (typeof options !== 'undefined' && typeof options.Gas !== 'undefined') {
      // gas = [];
      options.Gas.forEach(element => {
        this.Gas.push(new GasItem(element));
      });
      // this.setGasArray(gas);
    } else {
      this.Gas.push(new GasItem(options.Gas));
      // this.setGasArray([new GasItem(options.Gas)]);
    }
  }

  setInfo = Info => {
    this.Info = Info;
  };
  setGasArray = Gas => {
    this.Gas = Gas;
  };

  get gasArray() {
    return this.Gas.slice();
  }
}
decorate(UserGameViewModel, {
  Info: observable,
  Gas: observable,
  setInfo: action,
  setGasArray: action,
  gasArray: computed,
});

export default UserGameViewModel;
