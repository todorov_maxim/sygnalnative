import {decorate, observable, action, toJS} from 'mobx';
import firebaseHelper from '../Services/FirebaseHelper';

import {Linking} from 'react-native';

// import auth from '@react-native-firebase/auth';
// import firebaseHelper from '../Services/FirebaseHelper';

class LicenseViewModel {
  selectedLicenseSku = null;
  availableLicenseSku = [];
  loading = false;
  selectedAction = null;
  availableAction = ['Purchase License', 'Activate License', 'Renew License'];
  firstName = '';
  lastName = '';
  skuDescription = '';
  skuPrice = '';
  skuCurrency = '';
  skuPurchaseForms = '';
  activateLicenseKey = '';
  renewLicenseKey = '';

  setRenewLicenseKey = renewLicenseKey => {
    this.renewLicenseKey = renewLicenseKey;
  };

  setActivateLicenseKey = activateLicenseKey => {
    this.activateLicenseKey = activateLicenseKey;
  };

  setSkuDescription = skuDescription => {
    this.skuDescription = skuDescription;
  };

  setSkuPrice = skuPrice => {
    this.skuPrice = skuPrice;
  };

  setSkuCurrency = skuCurrency => {
    this.skuCurrency = skuCurrency;
  };

  setSkuPurchaseForms = skuPurchaseForms => {
    this.skuPurchaseForms = skuPurchaseForms;
  };

  setFirstName = firstName => {
    this.firstName = firstName;
  };

  setLastName = lastName => {
    this.lastName = lastName;
  };

  setSelectedAction = selectedAction => {
    this.selectedAction = selectedAction;
  };

  setAvailableAction = availableAction => {
    this.availableAction = availableAction;
  };

  setSelectedLicenseSku = selectedLicenseSku => {
    this.selectedLicenseSku = selectedLicenseSku;
    this.updateSkuDetail();
  };

  updateSkuDetail = () => {
    if (this.selectedLicenseSku !== null) {
      if (this.availableLicenseSku.length > this.selectedLicenseSku) {
        this.skuDescription = this.availableLicenseSku[
          this.selectedLicenseSku
        ].description;
        this.skuPrice = this.availableLicenseSku[this.selectedLicenseSku].price;
        this.skuCurrency = this.availableLicenseSku[
          this.selectedLicenseSku
        ].currency;
        this.skuPurchaseForms = this.availableLicenseSku[
          this.selectedLicenseSku
        ].purchaseForms;
      }
    }
  };

  setAvailableLicenseSku = availableLicenseSku => {
    this.availableLicenseSku = availableLicenseSku;
  };

  setLoading = loading => {
    this.loading = loading;
  };

  getAllLicenseSkus = async () => {
    try {
      this.availableSessions = [];
      let snapshot = await firebaseHelper.getAllLicenseSkus();
      const theValue = snapshot.val();
      let availableLicenseSku = [];
      for (const key in theValue) {
        if (theValue.hasOwnProperty(key)) {
          const element = theValue[key];
          availableLicenseSku.push({
            currency: element.currency,
            description: element.description,
            price: element.price,
            purchaseForms: element.purchaseForms,
            shortDescription: element.shortDescription,
          });
        }
      }
      this.availableLicenseSku = availableLicenseSku;
      this.updateSkuDetail();
    } catch (e) {
      this.availableLicenseSku = [];
    }
  };

  purchase = () => {
    return new Promise(async (resolve, reject) => {
      try {
        if (!this.firstName && !this.lastName) {
          reject('invalid firstName or lastName');
        } else {
          let url = `${this.skuPurchaseForms}?custom=${this.firstName}%20${this.lastName}`;
          let supported = await Linking.canOpenURL(url);
          if (supported) {
            await Linking.openURL(url);
            this.setSelectedAction(1);
          } else {
            reject('Don\'t know how to open URI: ' + url);
          }
        }
      } catch (e) {
        reject('Fail to open payment form');
      }
    });
  };
}

decorate(LicenseViewModel, {
  selectedLicenseSku: observable,
  availableLicenseSku: observable,
  loading: observable,
  selectedAction: observable,
  availableAction: observable,
  firstName: observable,
  lastName: observable,
  skuDescription: observable,
  skuPrice: observable,
  skuCurrency: observable,
  skuPurchaseForms: observable,
  activateLicenseKey: observable,
  renewLicenseKey: observable,
  setSelectedLicenseSku: action,
  setAvailableLicenseSku: action,
  setLoading: action,
  setSelectedAction: action,
  setAvailableAction: action,
  setFirstName: action,
  setLastName: action,
  setSkuDescription: action,
  setSkuPrice: action,
  setSkuCurrency: action,
  setSkuPurchaseForms: action,
  setActivateLicenseKey: action,
  setRenewLicenseKey: action,
  getAllLicenseSkus: action,
  updateSkuDetail: action,
  purchase: action,
});
export default LicenseViewModel;
