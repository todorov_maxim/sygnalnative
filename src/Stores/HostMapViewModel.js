import {decorate, observable, action, computed} from 'mobx';

class HostMapViewModel {
  startLatitude = 0;
  startLongitude = 0;
  latitude = 0;
  longitude = 0;
  sessionName = '';

  setStartLatitude = startLatitude => {
    this.startLatitude = startLatitude;
  };

  setStartLongitude = startLongitude => {
    this.startLongitude = startLongitude;
  };

  setLatitude = latitude => {
    this.latitude = latitude;
  };

  setLongitude = longitude => {
    this.longitude = longitude;
  };

  setSessionName = sessionName => {
    this.sessionName = sessionName;
  };
}
decorate(HostMapViewModel, {
  latitude: observable,
  longitude: observable,
  sessionName: observable,

  setLatitude: action,
  setLongitude: action,
  setSessionName: action,
  setStartLatitude: action,
  setStartLongitude: action,
});

export default HostMapViewModel;
