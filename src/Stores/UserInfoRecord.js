import {decorate, observable, action} from 'mobx';
import UserInfo from './UserInfo';

class UserInfoRecord extends UserInfo {
  recordkey = '';

  constructor(options) {
    super(options);
    this.recordkey = options
      ? options.recordkey || Date.now() + ''
      : Date.now() + '';
  }
}
decorate(UserInfoRecord, {});

export default UserInfoRecord;
