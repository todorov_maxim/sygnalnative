import {decorate, observable, action, toJS} from 'mobx';
import {sha512} from 'react-native-sha512';
import GasItem from './GasItem';
import firebaseHelper from '../Services/FirebaseHelper';
import SecureStorage, {ACCESSIBLE} from 'react-native-secure-storage';

const config = {
  accessible: ACCESSIBLE.ALWAYS,
};

const SaveKey = 'SessionSetup';
class SessionSetup {
  name = '';
  password = '';
  range = 500;
  pwdHash = '';
  gasItems = [];
  newGasSetup = new GasItem();
  loading = false;

  constructor() {
    const gasItem = this.newGasInstance();
    this.gasItems.push(gasItem);
  }
  newGasInstance() {
    const gasItem = new GasItem();
    gasItem.setGasName('Oxygen');
    gasItem.setGasScale('%');
    gasItem.setMinAlarm(19.5);
    gasItem.setMaxAlarm(23);
    return gasItem;
  }
  createNewGas = () => {
    const gasItem = this.newGasInstance();
    this.newGasSetup = gasItem;
  };
  setRange = newRange => {
    this.range = newRange;
  };
  setPassword = password => {
    this.password = password;
  };
  setName = name => {
    this.name = name;
  };
  removeGasItemByKey = key => {
    const item = this.gasItems.find(item => key !== item.key);
    this.gasItems.remove(item);
  };
  removeGasItem = item => {
    this.gasItems.remove(item);
  };
  addGasItem = gasItem => {
    gasItem.timestamp = Date.now();
    this.gasItems.push(gasItem);
  };
  selectGasForEdit = item => {
    this.newGasSetup = item;
  };
  createSession = async (uid, resolve, reject) => {
    if (this.latitude === null || this.longitude === null) {
      reject('no GPS location');
    }
    this.pwdHash = await sha512(this.password);
    firebaseHelper.createSession(uid, this).then(
      () => {
        resolve();
      },
      err => {
        reject(err);
      },
    );
  };

  save = async () => {
    let saveObject = {
      name: this.name,
      range: this.range,
      password: this.password,
      gasItems: toJS(this.gasItems),
      version: 1,
    };

    try {
      await SecureStorage.setItem(SaveKey, JSON.stringify(saveObject), config);
    } catch (error) {
      // Error saving data
    }
  };

  load = async () => {
    try {
      let saveObjectString = await SecureStorage.getItem(SaveKey, config);
      if (saveObjectString) {
        var saveObject = JSON.parse(saveObjectString);
        if (saveObject && saveObject.version == 1) {
          let gasItems = [];
          this.name = saveObject.name;
          this.range = saveObject.range;
          this.password = saveObject.password;
          saveObject.gasItems.forEach(element => {
            gasItems.push(new GasItem(element));
          });
          this.gasItems = gasItems;
        }
      }
    } catch (error) {
      console.log('error', error);
    }
  };

  setLoading = loading => {
    this.loading = loading;
  };
}
decorate(SessionSetup, {
  name: observable,
  password: observable,
  range: observable,
  pwdHash: observable,
  gasItems: observable,
  newGasSetup: observable,
  loading: observable,
  setRange: action,
  setPassword: action,
  setName: action,
  removeGasItemByKey: action,
  removeGasItem: action,
  addGasItem: action,
  createNewGas: action,
  selectGasForEdit: action,
  createSession: action,
  save: action,
  load: action,
  setLoading: action,
});

export default SessionSetup;
