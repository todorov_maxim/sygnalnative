import uuid from 'react-native-uuid';
import { decorate, observable, action, toJS } from 'mobx';
import { sha512 } from 'react-native-sha512';
import firebaseHelper from '../Services/FirebaseHelper';
import locationHelper from '../Services/LocationHelper';
import { getDistance } from 'geolib';

import SecureStorage, {
  ACCESS_CONTROL,
  ACCESSIBLE,
  AUTHENTICATION_TYPE,
} from 'react-native-secure-storage';

const config = {
  accessible: ACCESSIBLE.ALWAYS,
};

const SaveKey = 'IncidentViewModel';

class IncidentViewModel {
  selectedSession = null;
  password = '';
  range = 500;
  availableSessions = [];
  loading = false;
  pwdHash = '';
  // latitude = 0;
  // longitude = 0;
  // watchId = null;

  constructor(options) { }

  setRange = newRange => {
    this.range = newRange;
    this.getAvailableSessions();
  };

  setSelectedSession = selectedSession => {
    this.selectedSession = selectedSession;
  };

  setPassword = password => {
    this.password = password;
  };

  setLoading = loading => {
    this.loading = loading;
  };
  setAvailableSessions = availableSessions => {
    this.availableSessions = availableSessions;
  };

  toDbSession = theValue => {
    return {
      name: theValue.name,
      range: theValue.range,
      latitude: theValue.latitude,
      longitude: theValue.longitude,
      pwdHash: theValue.pwdHash,
      stamp: theValue.stamp,
    };
  };

  getAvailableSessions = async () => {
    try {
      this.availableSessions = [];
      firebaseHelper.getAllSessions(snapshot => {
        const theValue = snapshot.val();
        let availableSessions = [];
        for (const key in theValue) {
          if (theValue.hasOwnProperty(key)) {
            const element = theValue[key];
            const session = this.toDbSession(element);
            const distance = this.computeSessionDistance(session);
            if ((session.range == 0 || distance <= session.range) &&
              (this.range == 0 || distance <= this.range))
              availableSessions.push(session);
          }
        }
        this.availableSessions = availableSessions;
      });
    } catch (e) {
      this.availableSessions = [];
    }
  };

  joinSession = async (resolve, reject) => {
    try {
      this.setLoading(true);
      this.pwdHash = await sha512(this.password);

      if (this.selectedSession === null) {
        throw 'no selectedSession';
      }
      if (this.pwdHash !== this.selectedSession.pwdHash) {
        throw 'invalid password';
      } else {
        this.setLoading(false);
        resolve();
      }
    } catch (error) {
      this.setLoading(false);
      reject(error);
    }
  };

  /*startLocationUpdates = (onSuccess, errorCallback) => {
    var isSuccessCalled = false;
    this.removeLocationUpdates();
    this.watchId = locationHelper.startLocationUpdates(
      position => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        if (!isSuccessCalled) {
          isSuccessCalled = true;
          onSuccess && onSuccess();
        }
      },
      error => {
        errorCallback(error);
      },
    );
  };

  removeLocationUpdates = () => {
    if (this.watchId !== null) {
      locationHelper.removeLocationUpdates(this.watchId);
      this.watchId = null;
    }
  };*/

  computeSessionDistance = (session) => {
    if (!locationHelper.latitude || !locationHelper.longitude) return 0;

    return getDistance(
      { latitude: locationHelper.latitude, longitude: locationHelper.longitude },
      {
        latitude: session.latitude,
        longitude: session.longitude,
      },
    );
  };

  save = async () => {
    let saveObject = {
      password: this.password,
      version: 1,
    };

    try {
      await SecureStorage.setItem(SaveKey, JSON.stringify(saveObject), config);
    } catch (error) {
      // Error saving data
    }
  };

  load = async () => {
    try {
      let saveObjectString = await SecureStorage.getItem(SaveKey, config);
      if (saveObjectString) {
        var saveObject = JSON.parse(saveObjectString);
        if (saveObject && saveObject.version == 1) {
          this.password = saveObject.password;
        }
      }
    } catch (error) { console.log('error', error);}
  };
}
decorate(IncidentViewModel, {
  selectedSession: observable,
  password: observable,
  range: observable,
  availableSessions: observable,
  loading: observable,
  pwdHash: observable,
  // latitude: observable,
  // longitude: observable,
  setSelectedSession: action,
  setPassword: action,
  setRange: action,
  setAvailableSessions: action,
  getAvailableSessions: action,
  // startLocationUpdates: action,
  // removeLocationUpdates: action,
  computeSessionDistance: action,
  joinSession: action,
  setLoading: action,
});

export default IncidentViewModel;
