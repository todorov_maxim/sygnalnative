import Key from 'cryptolens';

const auth =
  'WyIxMzA2OSIsIkUrVmtNTk9Sak1kK2hlb0ZIa2p2WjJhV1NJT2dObk1HcU1oRG9DOGgiXQ==';
const RSAPubKey =
  '<RSAKeyValue><Modulus>whOWx61kZrdl9RgA8etLhAdxuWF15gSvnBk9uN9nQJROVp7QqJ9TIH3212PjrZC0oHhkjoGaElwFziOBEQ6s4Fk2C4hJilniGns3rET8kUbY2VCTf5/gfMLw8+8jW9f61E86oUCC/vR1eGtHtLF6dKbuza2sNTL1bXXMG0TZyTbCEBXO1SDOtbfNaIgeY3SRUCIVF4xL3zVX9V7xo/UO+qAdQQsVGFaZmiKBsQtwbKftSd2x6fC4LmJYnXNwvRP02XljolzG2IKW7Y6SQPFxB+mfNJSdvHNVN91Cfgl5t221JLp6Lvnp2CiivO2VWpdtsJnBcjjYEHKIB4nGOPUFxw==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>';
const productId = 5437;

const floatingTimeIntervalSecond = 10;

class CryptolensHelper {
  activateLinse = key => {
    return new Promise((resolve, reject) => {
      var result = Key.Activate(
        auth,
        RSAPubKey,
        productId,
        key,
        'test',
        0,
        true,
        floatingTimeIntervalSecond,
      );
      result.then(function(license) {
        if (!license) {
          // failure
          reject('failure to Activate key');
          return;
        }
        resolve();
        // Please see https://app.cryptolens.io/docs/api/v3/model/LicenseKey for a complete list of parameters.
        // console.log(license.Created);
      });
    });
  };
}

const cryptolensHelper = new CryptolensHelper();
export default cryptolensHelper;
