// import axios from 'axios';
import {Platform} from 'react-native';
import {getUniqueId} from 'react-native-device-info';
// import { LexFloatClient } from '@cryptlex/lexfloatclient';

const CRYPTLEX_URL = 'https://api.cryptlex.com';
const PRODUCT_ID = '0633be34-e7df-42ab-9a7f-09135336ff49';
const headers = {
  'Content-Type': 'application/json',
};

// export const FIREBASE_URL = "http://10.0.2.2:3000";
export const FIREBASE_URL = 'http://3.85.160.252';

class CryptlexHelper {
  makeToString(str, len) {
    var target = '';
    while (target.length < len) {
      target += str;
    }
    return target;
  }

  async activate(userId, key) {
    await this.removeActivation(userId);

    const activationToken = await this.getActivationToken(userId, key);
    return activationToken;
  }

  async checkActivation(userId) {
    const url = `${FIREBASE_URL}/checkActivation/${userId}`;
    var response = await fetch(url, {
      method: 'POST',
    });
    response = await response.json();
    console.log('checkActivation', response);
    return response;
  }

  async getActivationToken(userId, key) {
    try {
      const url = `${CRYPTLEX_URL}/v3/activations`;
      const deviceId = getUniqueId();
      const fingerprint = this.makeToString(deviceId, 64);
      const hostname = userId;
      const userHash = this.makeToString(userId, 32);

      console.log('getActivationToken');
      var response = await fetch(url, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          key: key,
          os: Platform.OS,
          fingerprint: fingerprint,
          hostname: hostname,
          appVersion: '1.0.0',
          userHash: userHash,
          productId: PRODUCT_ID,
        }),
      });
      response = await response.json();
      return response;
    } catch (e) {
      console.log('EEEEEEE', e);
      throw e;
    }
  }

  async removeActivation(userId) {
    const url = `${FIREBASE_URL}/removeActivation/${userId}`;
    var response = await fetch(url, {
      method: 'POST',
    });
    response = await response.json();
  }
}

const cryptlexHelper = new CryptlexHelper();
export default cryptlexHelper;
