import { PermissionsAndroid, Platform } from 'react-native';
import Geolocation from 'react-native-geolocation-service';

class LocationHelper {
  watchId = null;
  latitude = null;
  longitude = null;
  onGetOnce = null;

  funcMap = {};

  hasLocationPermission = () => {
    return new Promise(async (resolve, reject) => {
      if (
        Platform.OS === 'ios' ||
        (Platform.OS === 'android' && Platform.Version < 23)
      ) {
        resolve();
        return;
      }

      const hasPermission = await PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );

      if (hasPermission) {
        resolve();
        return;
      }

      const status = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );

      if (status === PermissionsAndroid.RESULTS.GRANTED) {
        resolve();
      } else if (status === PermissionsAndroid.RESULTS.DENIED) {
        reject('Location permission denied by user.');
      } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
        reject('Location permission revoked by user.');
      }
    });
  };

  onGetLocation = (position) => {
    this.latitude = position.coords.latitude;
    this.longitude = position.coords.longitude;

    if (this.onGetOnce) {
      this.onGetOnce();
      this.onGetOnce = null;
    }

    for (var key in this.funcMap)
      this.funcMap[key] && this.funcMap[key]({
        latitude: this.latitude,
        longitude: this.longitude
      });
  }

  onErrorCallback = (e) => {
    console.log(e);
  }

  startLocationUpdates = async (onGetOnce, onFailure) => {
    try {
      this.onGetOnce = onGetOnce;
      let status = await this.hasLocationPermission();

      Geolocation.getCurrentPosition(this.onGetLocation.bind(this), this.onErrorCallback.bind(this), {
        enableHighAccuracy: true,
        timeout: 15000,
        maximumAge: 10000,
      });
      this.watchId = Geolocation.watchPosition(this.onGetLocation.bind(this), (e) => {
        console.log(e);
        onFailure && onFailure();
      }, {
        enableHighAccuracy: true,
        distanceFilter: 3,
        interval: 5000,
        fastestInterval: 2000,
        forceRequestLocation: true,
        showLocationDialog: true,
      });
    } catch (e) {
      console.log(e);
      return null;
    }
  };

  removeLocationUpdates = () => {
    if (this.watchId !== null) {
      Geolocation.clearWatch(this.watchId);
      this.watchId = null;
    }
  };

  setLocationUpdate = (key, callback) => {
    this.funcMap[key] = callback;
  }

  removeUpdateCallback = (key) => {
    delete this.funcMap[key];
  }
}

const locationHelper = new LocationHelper();
export default locationHelper;
