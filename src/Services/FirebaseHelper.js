import {decorate, observable, action, computed, toJS} from 'mobx';
import database from '@react-native-firebase/database';
import Timeout from 'await-timeout';
import locationHelper from './LocationHelper';

const waitTime20S = 1000 * 20;
const waitTime5S = 1000 * 50;
const sessionRecords_ref_name = 'SessionRecords';
const gas_ref_name = 'Gas';
const info_ref_name = 'Info';
const users_ref_name = 'Users';
const messages_ref_name = 'Messages';
const sessionList_ref_name = 'SessionList';
const userList_ref_name = 'UserList';
const licenseSkus_ref_name = 'LicenseSkus';
const userInfoRecord_ref_name = 'UserInfoRecords';
const license_ref_name = 'Licenses';
// const cryptlex_ref_name = 'Cryptlex';

class FirebaseHelper {
  goOnline = () => {
    return database().goOnline();
  };

  getAllLicenseSkus = () => {
    const promise = database()
      .ref(licenseSkus_ref_name)
      .once('value');
    return Timeout.wrap(promise, waitTime5S, []);
  };

  registerToAuthData = (userId, onValueChange) => {
    const promise = database()
      .ref(userList_ref_name)
      .child(userId)
      .on('value', onValueChange);
    return promise;
  };

  unRegisterToAuthData = (userId, onValueChange) => {
    database()
      .ref(userList_ref_name)
      .child(userId)
      .off('value', onValueChange);
  };

  setAuthData = userData => {
    return database()
      .ref(userList_ref_name)
      .child(userData.uid)
      .update(userData);
  };

  getAllGasItems = sessionName => {
    const promise = database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(gas_ref_name)
      .orderByValue('timestamp')
      .once('value');
    return Timeout.wrap(promise, waitTime5S, []);
  };

  getSessionsCallback = null;
  getAllSessions = callback => {
    if (this.getSessionsCallback)
      database()
        .ref(sessionList_ref_name)
        .off('value', this.getSessionsCallback);

    this.getSessionsCallback = callback;
    database()
      .ref(sessionList_ref_name)
      .on('value', this.getSessionsCallback);
  };

  getAllSessionsOnce = () => {
    const promise = database()
      .ref(sessionList_ref_name)
      .once('value');
    return Timeout.wrap(promise, waitTime5S, []);
  };

  getSessionsByName = sessionName => {
    const promise = database()
      .ref(sessionList_ref_name)
      .child(sessionName)
      .once('value');
    return Timeout.wrap(promise, waitTime5S, []);
  };

  createSession = (uid, sessionSetup) => {
    const promises = [];

    promises.push(this.deleteSession(sessionSetup.name));

    let sessionListPromise = database()
      .ref(sessionList_ref_name)
      .child(sessionSetup.name)
      .set({
        name: sessionSetup.name,
        range: sessionSetup.range,
        latitude: locationHelper.latitude,
        longitude: locationHelper.longitude,
        pwdHash: sessionSetup.pwdHash,
        uid: uid,
        timestamp: Date.now(),
      });

    promises.push(sessionListPromise);

    promises.push(
      database()
        .ref(sessionRecords_ref_name)
        .child(sessionSetup.name)
        .child(gas_ref_name)
        .remove(),
    );

    toJS(sessionSetup.gasItems).forEach(element => {
      promises.push(
        database()
          .ref(sessionRecords_ref_name)
          .child(sessionSetup.name)
          .child(gas_ref_name)
          .child(element.key)
          .set(element),
      );
    });
    return promises.reduce((p, fn) => p.then(fn), Promise.resolve());
  };

  deleteSession = sessionName => {
    const promises = [];
    promises.push(
      database()
        .ref(sessionRecords_ref_name)
        .child(sessionName)
        .remove(),
    );

    promises.push(
      database()
        .ref(sessionList_ref_name)
        .child(sessionName)
        .remove(),
    );
    return promises.reduce((p, fn) => p.then(fn), Promise.resolve());
  };

  onDisconnectRemoveFromSessionList = sessionName => {
    let onDisconnect = database()
      .ref(sessionList_ref_name)
      .child(sessionName)
      .onDisconnect();
    onDisconnect.remove();
    return onDisconnect;
  };

  onDisconnectRemoveFromSessionRecords = sessionName => {
    let onDisconnect = database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .onDisconnect();
    onDisconnect.remove();
    return onDisconnect;
  };

  joinSession = joinSessionSetup => {
    const promises = [];

    promises.push(
      database()
        .ref(sessionRecords_ref_name)
        .child(joinSessionSetup.selectedSession.name)
        .child(users_ref_name)
        .child(joinSessionSetup.key)
        .remove(),
    );

    let userSessionPromise = database()
      .ref(sessionRecords_ref_name)
      .child(joinSessionSetup.selectedSession.name)
      .child(users_ref_name)
      .child(joinSessionSetup.key)
      .child(info_ref_name)
      .set({
        key: joinSessionSetup.key,
        userName: joinSessionSetup.userName,
        latitude: joinSessionSetup.latitude,
        longitude: joinSessionSetup.longitude,
        isDrone: joinSessionSetup.isDrone,
        timestamp: Date.now(),
      });
    promises.push(userSessionPromise);

    toJS(joinSessionSetup.gasItems).forEach(element => {
      promises.push(
        database()
          .ref(sessionRecords_ref_name)
          .child(joinSessionSetup.selectedSession.name)
          .child(users_ref_name)
          .child(joinSessionSetup.key)
          .child(gas_ref_name)
          .child(element.key)
          .set(element),
      );
    });

    return promises.reduce((p, fn) => p.then(fn), Promise.resolve());
  };

  registerToAllSessionGasUpdate = (sessionName, onValueChange) => {
    database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(gas_ref_name)
      .orderByValue('timestamp')
      .on('value', onValueChange);
  };

  unRegisterToAllSessionGasUpdate = (sessionName, onValueChange) => {
    database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(gas_ref_name)
      .off('value', onValueChange);
  };

  registerToAllUserDataUpdate = (sessionName, onValueChange) => {
    database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(users_ref_name)
      .on('value', onValueChange);
  };

  unRegisterToAllUserDataUpdate = (sessionName, onValueChange) => {
    database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(users_ref_name)
      .off('value', onValueChange);
  };

  deleteUserData = (sessionName, userKey) => {
    const promises = [];
    promises.push(
      database()
        .ref(sessionRecords_ref_name)
        .child(sessionName)
        .child(users_ref_name)
        .child(userKey)
        .remove(),
    );

    promises.push(
      database()
        .ref(sessionRecords_ref_name)
        .child(sessionName)
        .child(userInfoRecord_ref_name)
        .child(userKey)
        .remove(),
    );
    return promises.reduce((p, fn) => p.then(fn), Promise.resolve());
  };

  onDisconnectDeleteUserData = (sessionName, userKey) => {
    let onDisconnect = database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(users_ref_name)
      .child(userKey)
      .onDisconnect();
    onDisconnect.remove();
    return onDisconnect;
  };

  onDisconnectDeleteUserInfoRecord = (sessionName, userKey) => {
    let onDisconnect = database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(userInfoRecord_ref_name)
      .child(userKey)
      .onDisconnect();
    onDisconnect.remove();
    return onDisconnect;
  };

  registerToUserDataUpdate = (sessionName, userKey, onValueChange) => {
    database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(users_ref_name)
      .child(userKey)
      .orderByChild('timestamp')
      .on('value', onValueChange);
  };

  unRegisterToUserDataUpdate = (sessionName, userKey, onValueChange) => {
    database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(users_ref_name)
      .child(userKey)
      .off('value', onValueChange);
  };

  addUserMessage = (sessionName, userKey, message) => {
    return database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(messages_ref_name)
      .child(userKey)
      .push({message: message});
  };

  removeUserMessage = (sessionName, userKey, messageKey) => {
    return database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(messages_ref_name)
      .child(userKey)
      .child(messageKey)
      .remove();
  };

  registerToUserMessageUpdate = (sessionName, userKey, onValueChange) => {
    database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(messages_ref_name)
      .child(userKey)
      .on('value', onValueChange);
  };

  unRegisterToUserMessageUpdate = (sessionName, userKey, onValueChange) => {
    database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(messages_ref_name)
      .child(userKey)
      .off('value', onValueChange);
  };

  updateUserGasItem = (sessionName, userKey, gasItem) => {
    return database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(users_ref_name)
      .child(userKey)
      .child(gas_ref_name)
      .child(gasItem.key)
      .set(gasItem);
  };

  updateUserInfo = (sessionName, userKey, userInfo) => {
    return database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(users_ref_name)
      .child(userKey)
      .child(info_ref_name)
      .set(userInfo);
  };

  updateUserInfoRecord = (sessionName, userKey, userInfoRecord) => {
    return database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(userInfoRecord_ref_name)
      .child(userKey)
      .child(userInfoRecord.recordkey)
      .set(userInfoRecord);
  };

  registerToUserInfoRecordUpdate = (sessionName, userKey, onValueChange) => {
    return database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(userInfoRecord_ref_name)
      .child(userKey)
      .on('child_added', onValueChange);
  };

  unRegisterToUserInfoRecordUpdate = (sessionName, userKey, onValueChange) => {
    return database()
      .ref(sessionRecords_ref_name)
      .child(sessionName)
      .child(userInfoRecord_ref_name)
      .child(userKey)
      .off('child_added', onValueChange);
  };

  addNewLicense = async (uid, licensedata) => {
    const {id, key, expiresAt, createdAt, userId} = licensedata;
    await database()
      .ref(userList_ref_name)
      .child(uid)
      .child(license_ref_name)
      .child(id)
      .set({id, key, expiresAt, createdAt, userId});
  };

  removeLicense = async (uid, licenseId) => {
    await database()
      .ref(userList_ref_name)
      .child(uid)
      .child(license_ref_name)
      .child(licenseId)
      .remove();
  };

  registerToLicenseUpdate = (userId, onValueChange) => {
    database()
      .ref(userList_ref_name)
      .child(userId)
      .child(license_ref_name)
      .orderByChild('createdAt')
      .on('value', onValueChange);
  };

  unRegisterToLicenseUpdate = (userId, onValueChange) => {
    database()
      .ref(userList_ref_name)
      .child(userId)
      .child(license_ref_name)
      .off('value', onValueChange);
  };

  getUserCreatedAt = userId => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        database()
          .ref(userList_ref_name)
          .child(userId)
          .once('value', snapshot => {
            const values = snapshot.val();
            if (!values) resolve(0);
            else resolve(values.createdAt);
          });
      }, 100);
    });
  };

  getUserEmail = userId => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        database()
          .ref(userList_ref_name)
          .child(userId)
          .once('value', snapshot => {
            const values = snapshot.val();
            if (!values) resolve(0);
            else resolve(values.email);
          });
      }, 100);
    });
  };
}

const firebaseHelper = new FirebaseHelper();
export default firebaseHelper;
