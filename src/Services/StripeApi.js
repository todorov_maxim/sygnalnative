import axios from 'axios';
export const doPayment = (email, tokenId) => {
  const body = {
    email: email,
    tokenId: tokenId,
  };
  const headers = {
    'Content-Type': 'application/json',
  };
  return (
    axios
      // .post('http://localhost:5000/api/doPayment', body, {headers})
      .post(
        'https://kcv71k94x7.execute-api.us-east-1.amazonaws.com/prod/payment',
        body,
        {headers},
      )
      .then(({data}) => {
        console.log('data:', data);
        return data;
      })
      .catch((error) => {
        console.log('error:', error);
        return Promise.reject('Error in making payment', error);
      })
  );
};
