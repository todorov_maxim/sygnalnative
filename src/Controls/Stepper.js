import React from 'react';
import {
  Container,
  Text,
  Button,
  Picker,
  Icon,
  Item,
  Content,
  Input,
} from 'native-base';
import {Col, Row, Grid} from 'react-native-easy-grid';
import MainStyle from '../StyleSheets/MainStyle';

class Stepper extends React.Component {
  render() {
    const {label, value, onValueChange, handleInc, handleDec} = this.props;
    return (
      <Grid>
        <Col
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Text>{label}</Text>
        </Col>
        <Col
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'flex-end',
            marginRight: 20,
          }}>
          <Button transparent onPress={handleDec}>
            <Text
              style={[
                MainStyle.stepColor,
                {fontWeight: 'bold', fontSize: 20},
              ]}>
              -
            </Text>
          </Button>
          <Item
            regular
            style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Input
              value={value.toString()}
              onChangeText={onValueChange}
              keyboardType="decimal-pad"
              textAlign={'center'}
            />
          </Item>
          <Button transparent onPress={handleInc}>
            <Text
              style={[
                MainStyle.stepColor,
                {fontWeight: 'bold', fontSize: 20},
              ]}>
              +
            </Text>
          </Button>
        </Col>
      </Grid>
    );
  }
}

export default Stepper;
