import React from 'react';
import {View, Alert, StyleSheet, Image} from 'react-native';
import {
  Container,
  Content,
  Item,
  Label,
  Input,
  Picker,
  Header,
  Title,
  Button,
  Icon,
  Right,
  Body,
  Left,
} from 'native-base';
import Loader from '../Controls/Loader';
import crashlytics from '@react-native-firebase/crashlytics';

import {inject, observer} from 'mobx-react';
import MainStyle, {mainColor} from '../StyleSheets/MainStyle';

import GradientButton from '../elements/GradientButton';
import GradientNavigationBar from '../elements/GradientNavigationBar';

import {shadowOpt} from '../styles/variables';

import {delay} from '../Utils';
import {SessionRanges} from '../Constants';
import Toast from 'react-native-root-toast';

import BackArrow from '../../img/left-arrow.png';

const IncidentMenuScreen = observer(
  class IncidentMenuScreen extends React.Component {
    static navigationOptions = {headerShown: false};
    didFocusSubscription = this.props.navigation.addListener('didFocus', () => {
      const incidentViewModel = this.props.store.incidentViewModel;
      incidentViewModel.getAvailableSessions();
      incidentViewModel.load();
    });

    didBlurSubscription = this.props.navigation.addListener('didBlur', () => {
      const incidentViewModel = this.props.store.incidentViewModel;
      incidentViewModel.save();
    });

    componentWillUnmount() {
      this.didBlurSubscription.remove();
      this.didFocusSubscription.remove();
    }

    joinSession = async () => {
      const {loginScreenViewModel} = this.props.store;
      if (
        !loginScreenViewModel.isOnTrial() &&
        !loginScreenViewModel.isActivated
      ) {
        Toast.show('License Activation is required.');
        return;
      }
      const {navigate} = this.props.navigation;
      const incidentViewModel = this.props.store.incidentViewModel;
      if (!incidentViewModel.selectedSession) {
        Toast.show('Select Session.');
        return;
      }
      incidentViewModel.joinSession(
        () => {
          navigate('HostGame', {
            sessionName: incidentViewModel.selectedSession.name,
            latitude: incidentViewModel.selectedSession.latitude,
            longitude: incidentViewModel.selectedSession.longitude,
            hostMode: false,
          });
        },
        err => {
          delay(100).then(() => Alert.alert('Error', err));
        },
      );
    };

    render() {
      const incidentViewModel = this.props.store.incidentViewModel;
      const availableSessionsItems = incidentViewModel.availableSessions.map(
        (s, i) => {
          return <Picker.Item key={i} value={s} label={s.name} />;
        },
      );

      const sessionRangesItems = SessionRanges.map((s, i) => {
        return <Picker.Item key={i} value={s.value} label={s.label} />;
      });
      crashlytics().log('Spectator mode Screen');
      return (
        <Container>
          <GradientNavigationBar
            navigation={this.props.navigation}
            back
            titleText="Join a Session"
          />
          <Loader loading={incidentViewModel.loading} />
          <Content padder>
            <View style={styles.view}>
              <Item picker style={[MainStyle.marginV10, styles.item]}>
                <Label style={MainStyle.labelColor}>Session Range</Label>
                <Picker
                  iosIcon={
                    <Image
                      source={BackArrow}
                      style={{
                        backgroundColor: 'transparent',
                        width: 10,
                        height: 10,
                        color: 'black',
                        tintColor: 'black',
                        transform: [{rotate: '-90deg'}],
                      }}
                    />
                  }
                  mode="dialog"
                  renderHeader={backAction => (
                    <Header style={{backgroundColor: mainColor}}>
                      <Left>
                        <Button transparent onPress={backAction}>
                          {/* <Icon name="arrow-back" style={{color: '#fff'}} /> */}
                          <Image
                            source={BackArrow}
                            style={{
                              backgroundColor: 'transparent',
                              width: 30,
                              height: 30,
                              color: 'white',
                              tintColor: 'white',
                            }}
                          />
                        </Button>
                      </Left>
                      <Body style={{flex: 3, alignItems: 'center'}}>
                        <Title style={{color: '#fff'}}>Select Range</Title>
                      </Body>
                      <Right />
                    </Header>
                  )}
                  placeholder="SESSION RANGE"
                  placeholderStyle={styles.itemPlaceholder}
                  placeholderIconColor="black"
                  selectedValue={incidentViewModel.range}
                  onValueChange={newRange =>
                    incidentViewModel.setRange(newRange)
                  }>
                  {sessionRangesItems}
                </Picker>
              </Item>

              {incidentViewModel.availableSessions.length > 0 ? (
                <Item picker style={[MainStyle.marginV10, styles.picker]}>
                  <Picker
                    iosIcon={
                      <Image
                        source={BackArrow}
                        style={{
                          backgroundColor: 'transparent',
                          width: 10,
                          height: 10,
                          color: 'black',
                          tintColor: 'black',
                          transform: [{rotate: '-90deg'}],
                        }}
                      />
                    }
                    mode="dialog"
                    renderHeader={backAction => (
                      <Header style={{backgroundColor: mainColor}}>
                        <Left>
                          <Button transparent onPress={backAction}>
                            {/* <Icon name="arrow-back" style={{color: '#fff'}} /> */}
                            <Image
                              source={BackArrow}
                              style={{
                                backgroundColor: 'transparent',
                                width: 30,
                                height: 30,
                                color: 'white',
                                tintColor: 'white',
                              }}
                            />
                          </Button>
                        </Left>
                        <Body style={{flex: 3, alignItems: 'center'}}>
                          <Title style={{color: '#fff'}}>Select Session</Title>
                        </Body>
                        <Right />
                      </Header>
                    )}
                    placeholder="View Available Sessions"
                    placeholderStyle={styles.pickerPlaceholder}
                    placeholderIconColor="white"
                    selectedValue={incidentViewModel.selectedSession}
                    onValueChange={newSelectedSession =>
                      incidentViewModel.setSelectedSession(newSelectedSession)
                    }>
                    <Picker.Item label="View Available Sessions" value={null} />
                    {availableSessionsItems}
                  </Picker>
                </Item>
              ) : (
                <Item style={MainStyle.marginV10}>
                  <Label style={styles.loadingLabel}>Loading Sessions</Label>
                </Item>
              )}
              <Item floatingLabel style={MainStyle.marginV10}>
                <Label style={MainStyle.labelColor}>Session Password</Label>
                <Input
                  autoCapitalize="none"
                  secureTextEntry={true}
                  value={incidentViewModel.password}
                  onChangeText={incidentViewModel.setPassword}
                />
              </Item>
              <View style={MainStyle.marginT10}>
                <GradientButton
                  onPressButton={this.joinSession.bind(this)}
                  setting={shadowOpt}
                  btnText="Join"
                />
              </View>
            </View>
          </Content>
        </Container>
      );
    }
  },
);

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    width: '100%',
  },
  itemPlaceholder: {
    color: 'black',
  },
  picker: {
    width: '100%',
  },
  pickerPlaceholder: {
    color: 'black',
  },
  loadingLabel: {
    flex: 1,
    ...MainStyle.marginV10,
    ...MainStyle.labelColor,
  },
});
export default inject('store')(IncidentMenuScreen);
