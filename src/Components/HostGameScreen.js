import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  Dimensions,
  ScrollView,
  FlatList,
} from 'react-native';
import {
  Container,
  Button,
  List,
  ListItem,
  Input,
  Textarea,
  Grid,
  Col,
} from 'native-base';

import Carousel, {Pagination} from 'react-native-snap-carousel';
import KeepAwake from 'react-native-keep-awake';

import {Dialog} from 'react-native-simple-dialogs';

import {inject, observer} from 'mobx-react';
import MainStyle from '../StyleSheets/MainStyle';
import {sliderWidth, itemWidth} from '../StyleSheets/MainStyle';
import {roundFloat, getElevation} from '../Utils';

import Text from '../elements/Text';
import GradientNavigationBar from '../elements/GradientNavigationBar';

import {colors, blueGradient} from '../styles/variables';
import LinearGradient from 'react-native-linear-gradient';
import DroneParamItem from '../elements/DroneParamItem';
import DroneModelItem from '../elements/DroneModelItem';

const _renderFlatListItem = observer(
  ({hostGameViewModel, userGameViewModel, item, index, hostMode}) => {
    return (
      <SafeAreaView style={{flex: 1, marginTop: 10}}>
        <ListItem style={styles.ListItemView}>
          <Text style={styles.gasNameText}>{item.gasName}</Text>
          <View style={styles.updateUserGasItemView}>
            {hostMode ? (
              <TouchableOpacity
                onPress={() => {
                  item.setValue(roundFloat(item.value - 0.1));
                  hostGameViewModel.updateUserGasItem(userGameViewModel, item);
                }}>
                <Image
                  style={styles.leftIcon}
                  source={require('../Images/left.png')}
                />
              </TouchableOpacity>
            ) : (
              <></>
            )}
            <View style={styles.inputView}>
              <Input
                value={item.value.toString()}
                keyboardType="decimal-pad"
                onChangeText={s => {
                  item.tempValue(s);
                }}
                onSubmitEditing={() => {
                  item.setValue(item.value);
                  hostGameViewModel.updateUserGasItem(userGameViewModel, item);
                }}
                returnKeyType="done"
                style={styles.input}
                textAlign={'center'}
                textAlignVertical={'center'}
              />
            </View>
            {hostMode ? (
              <TouchableOpacity
                onPress={() => {
                  item.setValue(roundFloat(item.value + 0.1));
                  hostGameViewModel.updateUserGasItem(userGameViewModel, item);
                }}>
                <Image
                  style={styles.rightIcons}
                  source={require('../Images/right.png')}
                />
              </TouchableOpacity>
            ) : (
              <></>
            )}
          </View>
        </ListItem>
      </SafeAreaView>
    );
  },
);

const _renderFlatListItemSpectatorMode = observer(({item}) => {
  const gasItem = item;
  const labelColor = gasItem.isAlarmTriggered
    ? MainStyle.labelColorRed
    : MainStyle.labelColor;

  return (
    <SafeAreaView style={{flex: 1, marginTop: 10}}>
      <View style={styles.readingListItemView}>
        <ListItem style={{paddingBottom: 5, paddingTop: 5}}>
          <Grid>
            <Col style={{}}>
              <Text
                style={[
                  labelColor,
                  styles.text,
                  {fontWeight: '600', fontSize: 20},
                ]}>
                {gasItem.gasName}
              </Text>
            </Col>

            <Col style={{alignItems: 'center'}}>
              <Text
                style={[
                  labelColor,
                  styles.text,
                  {paddingTop: 40, fontSize: 30, fontWeight: 'bold'},
                ]}>
                {gasItem.value}
              </Text>
            </Col>

            <Col style={{alignItems: 'flex-end'}}>
              <Text
                style={[
                  labelColor,
                  styles.text,
                  {fontSize: 20, fontWeight: '600'},
                ]}>
                {gasItem.gasScale}
              </Text>
            </Col>
          </Grid>
        </ListItem>
      </View>
    </SafeAreaView>
  );
});

const _renderItem = observer(({hostGameViewModel, item, index, hostMode}) => {
  let userGameViewModel = item;
  let speed = 0;
  if (userGameViewModel.Info.isDrone) {
    speed =
      Math.pow(userGameViewModel.Info.velocityX, 2) +
      Math.pow(userGameViewModel.Info.velocityY, 2) +
      Math.pow(userGameViewModel.Info.velocityZ, 2);
    speed = Math.sqrt(speed);
    speed = Math.round(speed * 100) / 100;
  }

  return (
    <View style={{width: '100%', flex: 1}}>
      <View style={{width: '100%', flex: 1}}>
        <SafeAreaView>
          <LinearGradient
            start={{x: 0.2, y: 0.4}}
            end={{x: 1.2, y: 1.0}}
            colors={blueGradient.colors}
            // eslint-disable-next-line react-native/no-inline-styles
            style={{
              ...getElevation(5),
              width: '95%',
              height: userGameViewModel.Info.isDrone ? 150 : 50,
              marginTop: 30,
              alignSelf: 'center',
              padding: 10,
              justifyContent: !userGameViewModel.Info.isDrone
                ? 'center'
                : 'space-between',
              borderRadius: 15,
            }}>
            <View style={styles.userNameTextView}>
              <Text style={styles.userNameText}>
                {userGameViewModel.Info.userName}
              </Text>
              {/* {userGameViewModel.Info.isDrone && (
                <DroneModelItem model={userGameViewModel.Info.droneModel} />
              )} */}
            </View>

            {userGameViewModel.Info.isDrone && (
              <View style={styles.droneConnectionStatusView}>
                <View style={{flex: 1}}>
                  <DroneParamItem
                    name="STATUS"
                    value={userGameViewModel.Info.droneConnectionStatus}
                  />
                </View>

                <View
                  style={{
                    height: 80,
                    width: 100,
                    justifyContent: 'center',
                    alignItems: 'center',
                    flex: 1,
                  }}>
                  <View
                    style={{
                      borderWidth: 1,
                      borderRadius: 10,
                      borderColor: 'white',
                      padding: 2.5,
                      flexDirection: 'row',
                      paddingHorizontal: 10,
                    }}>
                    <Text
                      style={{
                        textAlign: 'center',
                        fontSize: 16,
                        alignSelf: 'center',
                        color: colors.white,
                      }}>
                      {userGameViewModel.Info.droneModel}
                    </Text>
                    <Image
                      source={require('../../img/camera-drone.png')}
                      style={{width: 16, height: 16, marginLeft: 10}}
                    />
                  </View>
                  <Text
                    style={{
                      fontSize: 12,
                      // justifyContent: 'center',
                      // alignItems: 'flex-end',
                      // alignSelf: 'flex-end',
                      color: colors.white,
                      position: 'absolute',
                      bottom: 0,
                    }}>
                    Model
                  </Text>
                </View>

                {/* <DroneParamItem
                  name="Model"
                  value={userGameViewModel.Info.droneModel}
                /> */}

                {/* <DroneParamItem
                  name="ALTITUDE"
                  extraStyle={styles.droneAltitude}
                  value={userGameViewModel.Info.altitude}
                />
                <DroneParamItem name="SPEED" value={speed} /> */}
              </View>
            )}
          </LinearGradient>
        </SafeAreaView>

        <List
          dataArray={item.Gas}
          renderItem={({item, index}) =>
            hostMode ? (
              <_renderFlatListItem
                hostGameViewModel={hostGameViewModel}
                userGameViewModel={userGameViewModel}
                item={item}
                index={index}
                hostMode={hostMode}
              />
            ) : (
              <_renderFlatListItemSpectatorMode item={item} />
            )
          }
        />
      </View>
    </View>
  );
});

const HostGameScreen = observer(
  class HostGameScreen extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        viewport: {
          width: Dimensions.get('window').width,
          height: Dimensions.get('window').height,
        },
      };
    }

    static navigationOptions = {headerShown: false};
    didFocusSubscription = this.props.navigation.addListener('didFocus', () => {
      const hostGameViewModel = this.props.store.hostGameViewModel;

      hostGameViewModel.registerToAllUserDataUpdate();
    });

    didBlurSubscription = this.props.navigation.addListener('didBlur', () => {
      const hostGameViewModel = this.props.store.hostGameViewModel;
      hostGameViewModel.unRegisterToAllUserDataUpdate();
    });

    sendMessageToUser = () => {
      const hostGameViewModel = this.props.store.hostGameViewModel;
      let userGameViewModel = hostGameViewModel.activeUserGameViewModel;
      if (userGameViewModel) {
        hostGameViewModel.addUserMessage(userGameViewModel.key);
      }
      hostGameViewModel.setShowSendMgsDialog(false);
    };

    showSendMgsDialog = () => {
      const hostGameViewModel = this.props.store.hostGameViewModel;
      hostGameViewModel.setShowSendMgsDialog(true);
    };

    hostMode = false;
    componentDidMount() {
      const sessionName = this.props.navigation.getParam('sessionName');
      const latitude = this.props.navigation.getParam('latitude');
      const longitude = this.props.navigation.getParam('longitude');
      this.hostMode = this.props.navigation.getParam('hostMode');

      const hostGameViewModel = this.props.store.hostGameViewModel;

      this.props.navigation.setParams({
        showSendMgsDialog: this.showSendMgsDialog,
        sessionName: sessionName,
        latitude: latitude,
        longitude: longitude,
        hostMode: this.hostMode,
      });

      hostGameViewModel.setSessionName(sessionName);
      hostGameViewModel.registerToAllSessionGasUpdate(() => {
        if (!this.hostMode) this.props.navigation.popToTop();
      });

      hostGameViewModel.deleteSessionOnDisconnect();
    }

    componentWillUnmount() {
      const hostGameViewModel = this.props.store.hostGameViewModel;
      if (this.hostMode) {
        hostGameViewModel.deleteSession();
      }
      hostGameViewModel.unRegisterToAllSessionGasUpdate();
      this.didBlurSubscription.remove();
      this.didFocusSubscription.remove();
    }

    get pagination() {
      const hostGameViewModel = this.props.store.hostGameViewModel;

      return (
        <Pagination
          dotsLength={hostGameViewModel.usersData.length}
          activeDotIndex={hostGameViewModel.activeUserData}
          containerStyle={styles.paginationContainer}
          dotStyle={styles.paginationDot}
          inactiveDotStyle={
            {
              // Define styles for inactive dots here
            }
          }
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
        />
      );
    }
    render() {
      const {navigation} = this.props;
      const hostGameViewModel = this.props.store.hostGameViewModel;
      const userName =
        hostGameViewModel.activeUserGameViewModel &&
        hostGameViewModel.activeUserGameViewModel.Info
          ? ' ' + hostGameViewModel.activeUserGameViewModel.Info.userName
          : '';
      const hostMode = navigation.getParam('hostMode');
      return (
        <Container>
          <GradientNavigationBar
            navigation={this.props.navigation}
            back
            titleText={hostMode ? 'Host Session' : 'Spectator Mode'}
            rightButtons={[
              {
                key: 1,
                buttonIcon: require('../../img/map.png'),
                buttonAction: () =>
                  navigation.navigate('HostMap', {
                    sessionName: navigation.getParam('sessionName'),
                    latitude: navigation.getParam('latitude'),
                    longitude: navigation.getParam('longitude'),
                    hostMode: navigation.getParam('hostMode'),
                  }),
                buttonWidth: 26,
                buttonHeight: 26,
              },
              {
                key: 2,
                buttonIcon: require('../../img/message.png'),
                buttonAction: navigation.getParam('showSendMgsDialog'),
                buttonWidth: 26,
                buttonHeight: 23,
              },
            ]}
          />
          <Dialog
            title={'Send Message To' + userName}
            animationType="fade"
            contentStyle={styles.dialogContent}
            onTouchOutside={() => {}}
            visible={hostGameViewModel.showSendMgsDialog}>
            <Textarea
              style={styles.textArea}
              multiline={true}
              rowSpan={5}
              bordered
              placeholder={'Message ' + userName}
              onChangeText={text => hostGameViewModel.setMessageToSend(text)}
              value={hostGameViewModel.messageToSend}
            />

            <View style={styles.sendMessageView}>
              <Button
                onPress={() => hostGameViewModel.setShowSendMgsDialog(false)}
                style={styles.button}>
                <Text style={MainStyle.btnText}>CLOSE</Text>
              </Button>
              <Button onPress={this.sendMessageToUser} style={styles.button}>
                <Text style={MainStyle.btnText}>SEND MESSAGE</Text>
              </Button>
            </View>
          </Dialog>
          <View
            onLayout={() => {
              this.setState({
                viewport: {
                  width: Dimensions.get('window').width,
                  height: Dimensions.get('window').height,
                },
              });
            }}
            style={styles.carouselView}>
            <View style={styles.logoView}>
              <Image
                source={require('../Images/logo_.png')}
                style={styles.logo}
              />
            </View>

            {this.pagination}
            <Carousel
              data={hostGameViewModel.usersData}
              renderItem={({item, index}) => (
                <_renderItem
                  hostGameViewModel={hostGameViewModel}
                  item={item}
                  index={index}
                  hostMode={hostMode}
                />
              )}
              onSnapToItem={index => (hostGameViewModel.activeUserData = index)}
              ref={c => {
                this.carousel = c;
              }}
              sliderWidth={this.state.viewport.width}
              itemWidth={this.state.viewport.width}
            />
          </View>
          <KeepAwake />
        </Container>
      );
    }
  },
);

const styles = StyleSheet.create({
  readingListItemView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  ListItemView: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignSelf: 'center',
    width: '95%',
    marginLeft: 0,
  },
  gasNameText: {
    alignSelf: 'center',
    fontSize: 20,
  },
  updateUserGasItemView: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginRight: -10,
  },
  inputView: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    alignSelf: 'center',
    minWidth: 70,
  },
  leftIcon: {
    height: 35,
    width: 35,
  },
  rightIcons: {
    height: 35,
    width: 35,
  },
  userNameTextView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  userNameText: {
    fontSize: 24,
    alignSelf: 'flex-start',
    color: colors.white,
  },
  droneConnectionStatusView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 15,
  },
  paginationContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0)',
  },
  paginationDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 8,
    backgroundColor: 'rgba(16, 115, 172, 0.92)',
  },
  droneAltitude: {
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderColor: colors.white,
  },
  carouselView: {
    flex: 1,
  },
  dialogContent: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  textArea: {
    alignSelf: 'stretch',
  },
  sendMessageView: {
    flexDirection: 'row',
  },
  button: {
    margin: 10,
  },
  logoView: {
    alignItems: 'center',
    marginTop: 30,
  },
  logo: {
    width: 200,
    height: 57,
  },
});

export default inject('store')(HostGameScreen);
