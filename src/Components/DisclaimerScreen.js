import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem
} from 'native-base';
import RNExitApp from 'react-native-exit-app';

import { inject, observer } from 'mobx-react';
import { StackActions, NavigationActions } from 'react-navigation';

// import MainStyle from '../StyleSheets/MainStyle';

import Text from '../elements/Text';
import GradientButton from '../elements/GradientButton';

import CommonStyles from '../styles/CommonStyles';
import {
  shadowOpt,
  colors,
  introSpaceHeight,
} from '../styles/variables';

const DisclaimerScreen = observer(
  class DisclaimerScreen extends React.Component {
    static navigationOptions = { headerShown: false };
    agreeAction() {
      const loginScreenViewModel = this.props.store.loginScreenViewModel;
      if (
        loginScreenViewModel.firebaseUser &&
        loginScreenViewModel.firebaseUser.emailVerified
      ) {
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'Main' })],
        });
        this.props.navigation.dispatch(resetAction);
      } else {
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'Login' })],
        });
        this.props.navigation.dispatch(resetAction);
      }
    }

    exitAction() {
      RNExitApp.exitApp();
    }

    render() {
      return (
        <Container>
          <Content>
            <View style={CommonStyles.introPageImageBox}>
              <Image
                source={require('../Images/logo.png')}
                style={styles.logo}
              />
            </View>
            <View style={styles.disclaimerView}>
              <Text header black semiBold>DISCLAIMER</Text>
            </View>
            <Card style={styles.card}>
              <CardItem>
                <View>
                  <Text style={styles.paragraph}>
                    By downloading, accessing or using SYGNAL Mobile App or
                    any page of this app, you signify your assent to this
                    disclaimer. We reserve the right to amend these terms and
                    conditions at any time. The contents of this app,
                    including without limitation, all data, information, text,
                    graphics, links and other materials are provided as a
                    convenience to our app users and are meant to be used for
                    informational purposes only. The application is designed
                    for simulation only. It is not meant be used in hazardous
                    environments and does not provide any realtime warning,
                    protection, or have any actual detection capabilities. We
                    do not take responsibility for decisions taken by the
                    reader based on the information provided in this app.
                  </Text>
                  <Text style={styles.paragraph}>
                    The app EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND,
                    WHETHER EXPRESS OR IMPLIED. The app makes no warranty that
                    (1) THE APP OR THE CONTENT WILL MEET OR SATISFY YOUR
                    REQUIREMENTS (2) THE APP SHALL HAVE NO RESPONSIBILITY FOR
                    ANY DAMAGE TO YOUR PHONE OR TABLET OR LOSS OF DATA THAT
                    RESULTS FROM YOUR USE OF THE APP OR ITS CONTENT. YOU AGREE
                    TO HOLD HARMLESS HANDHELD HAZMAT INC. SYGNAL MOBILE APP,
                    WWW.SYGNALAPP.COM, AND EACH OF ITS OFFICERS, DIRECTORS,
                    EMPLOYEES, SOFTWARE DEVELOPERS, AND AGENTS FROM AND
                    AGAINST ANY AND ALL CLAIMS, ACTIONS, DEMANDS, LIABILITIES,
                    JUDGMENTS AND SETTLEMENTS, INCLUDING WITHOUT LIMITATION,
                    FROM ANY DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL,
                    SPECIAL, EXEMPLARY, PUNITIVE OR ANY OTHER CLAIM YOU MAY
                    INCUR IN CONNECTION WITH YOUR USE OF THIS APP, INCLUDING,
                    WITHOUT LIMITATION, ANY ECONOMIC HARM, LOST PROFITS,
                    DAMAGES TO BUSINESS, DATA OR PHONE SYSTEMS, OR ANY DAMAGES
                    RESULTING FROM RELIANCE ON ANY CONTENT OR RESULTING FROM
                    ANY INTERRUPTIONS, WORK STOPPAGES, PHONE OR TABLET
                    FAILURES,DELETION OF FILES, ERRORS, OMISSIONS,
                    INACCURACIES, DEFECTS, VIRUSES, DELAYS OR MISTAKES OF ANY
                    KIND.
                  </Text>
                  <Text style={styles.paragraph}>
                    The app may include inaccuracies and typographical errors.
                    Changes and improvements are periodically made to the app
                    and the information therein. We do not warrant or assume
                    any legal liability or responsibility for the
                    completeness, use, or usefulness of any information, or
                    service.
                  </Text>
                </View>
              </CardItem>

              <View style={styles.introPageButtonBox}>
                <GradientButton
                  onPressButton={this.agreeAction.bind(this)}
                  setting={shadowOpt}
                  btnText="GET STARTED"
                />
              </View>
            </Card>
          </Content>
        </Container>
      );
    }
  },
);

const styles = StyleSheet.create({
  logo:{
    width: 300, 
    height: 105,
  },
  introPageButtonBox: {
    alignItems: 'center',
    marginTop: introSpaceHeight * 0.07,
    marginBottom: 30,
  },
  paragraph: {
    color: colors.grey,
    marginBottom: 20
  },
  disclaimerView:{
    alignItems: 'center'
  },
  card: { 
    borderColor: 'transparent' 
  }
});


export default inject('store')(DisclaimerScreen);
