import React from 'react';
import {View, Image, Alert} from 'react-native';
import {
  Container,
  Content,
  Item,
  Label,
  Input
} from 'native-base';

import {inject, observer} from 'mobx-react';
import {StackActions, NavigationActions} from 'react-navigation';
import MainStyle from '../StyleSheets/MainStyle';
import Loader from '../Controls/Loader';

import Text from '../elements/Text';
import GradientButton from '../elements/GradientButton';

import CommonStyles from '../styles/CommonStyles';
import {
  shadowOpt,
  colors
} from '../styles/variables';
import {StyleSheet} from 'react-native';

import {delay} from '../Utils';

const LoginScreen = observer(
  class LoginScreen extends React.Component {
    static navigationOptions = {headerShown: false};

    state={isSignup: false}

    signIn = async () => {
      try {
        const loginScreenViewModel = this.props.store.loginScreenViewModel;
        const userCredential = await loginScreenViewModel.signIn();
        if (loginScreenViewModel.firebaseUser.emailVerified) {
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'Main'})],
          });
          this.props.navigation.dispatch(resetAction);
        } else {
          await delay(100);
          Alert.alert(
            'Sign In',
            'Activate account with the verification email',
            [
              {text: 'Cancel'},
              {
                text: 'Resend Email Verification',
                onPress: () => loginScreenViewModel.sendEmailVerification(),
              },
            ],
            {cancelable: false},
          );
        }
      } catch (e) {
        await delay(100);
        Alert.alert('Error', e);
      }
    };

    register = async () => {
      try {
        const loginScreenViewModel = this.props.store.loginScreenViewModel;
        const userCredential = await loginScreenViewModel.register();
        await delay(100);
        Alert.alert('Sign Up', 'Activate account with the verification email');
      } catch (e) {
        await delay(100);
        Alert.alert('Error', e);
      }
    };

    resetPassword = async () => {
      try {
        const loginScreenViewModel = this.props.store.loginScreenViewModel;
        await loginScreenViewModel.resetPassword();
        await delay(100);
        Alert.alert(
          'Forgot Password',
          'Follow reset password instruction email',
        );
      } catch (e) {
        await delay(100);
        Alert.alert('Error', e);
      }
    };

    render() {
      const loginScreenViewModel = this.props.store.loginScreenViewModel;
      const {isSignup} = this.state;
      return (
        <Container>
          <Loader loading={loginScreenViewModel.loading} />
          <Content padder>
            <View style={CommonStyles.introPageImageBox}>
              <Image
                source={require('../Images/logo.png')}
                style={styles.logo}
              />
            </View>
            <View
              style={styles.view}>
              <Item floatingLabel style={MainStyle.marginV10}>
                <Label style={MainStyle.labelColor}>Email</Label>
                <Input
                  value={loginScreenViewModel.email}
                  onChangeText={loginScreenViewModel.setEmail}
                />
              </Item>
              <Item floatingLabel style={MainStyle.marginV10}>
                <Label style={MainStyle.labelColor}>Password</Label>
                <Input
                  autoCapitalize="none"
                  secureTextEntry={true}
                  value={loginScreenViewModel.password}
                  onChangeText={loginScreenViewModel.setPassword}
                />
              </Item>
              {
                isSignup ? 
                  (
                    <Item floatingLabel style={MainStyle.marginV10}>
                      <Label style={MainStyle.labelColor}>Confirm Password</Label>
                      <Input
                        autoCapitalize="none"
                        secureTextEntry={true}
                        value={loginScreenViewModel.confirmPassword}
                        onChangeText={loginScreenViewModel.setConfirmPassword}
                      />
                    </Item>
                  )
                  :null
              }
              {
                isSignup ? 
                  (
                    <View style={MainStyle.marginT10}>
                      <GradientButton
                        onPressButton={this.register.bind(this)}
                        setting={shadowOpt}
                        btnText="Sign Up"
                      />
                    </View>
                  ): 
                  (
                    <View style = {MainStyle.marginT30}>
                      <GradientButton
                        onPressButton={this.signIn.bind(this)}
                        setting={shadowOpt}
                        btnText="Sign In"
                        style={MainStyle.marginV10H32}
                      />
                    </View>
                  )
              }
              


              <View style={MainStyle.marginT30}>
                {
                  isSignup ? 
                    (
                      <Text
                        style={{ color: colors.softBlue }}
                        onPress={() => this.setState({ isSignup: false })}>
                        Already have an account? Click here to Sign In
                      </Text>
                    ):
                    (
                      <Text
                        style={{ color: colors.softBlue }}
                        onPress={() => this.setState({ isSignup: true })}>
                        {'Don\'t have an account? Click here to Sign Up'}
                      </Text>
                    )
                }    
              </View>

              <View style={MainStyle.marginT30}>
                <Text normal lightGrey regular>
                  Did you forget Password?
                  <Text> </Text>
                  <Text
                    style={{color: colors.softBlue}}
                    onPress={() => this.resetPassword()}>
                    Reset Password
                  </Text>
                </Text>
              </View>
            </View>
          </Content>
        </Container>
      );
    }
  },
);

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: { 
    width: 300,
    height: 105, 
    marginVertical: 30 
  }
});

export default inject('store')(LoginScreen);
