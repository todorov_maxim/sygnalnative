import React from 'react';
import {View, Alert, Switch, StyleSheet, Image} from 'react-native';
import {
  Container,
  Content,
  Item,
  Label,
  Input,
  Picker,
  Header,
  Title,
  Button,
  Icon,
  Right,
  Body,
  Left,
} from 'native-base';
import Loader from '../Controls/Loader';

import {inject, observer} from 'mobx-react';
import MainStyle, {mainColor} from '../StyleSheets/MainStyle';

import GradientButton from '../elements/GradientButton';
import GradientNavigationBar from '../elements/GradientNavigationBar';

import {shadowOpt} from '../styles/variables';

import {delay} from '../Utils';
import {SessionRanges} from '../Constants';
import Toast from 'react-native-root-toast';

import BackArrow from '../../img/left-arrow.png';

const UserMainScreen = observer(
  class UserMainScreen extends React.Component {
    static navigationOptions = {headerShown: false};
    didFocusSubscription = this.props.navigation.addListener('didFocus', () => {
      const joinSessionSetup = this.props.store.joinSessionSetup;
      joinSessionSetup.getAvailableSessions();
      joinSessionSetup.load();
    });

    didBlurSubscription = this.props.navigation.addListener('didBlur', () => {
      const joinSessionSetup = this.props.store.joinSessionSetup;
      // joinSessionSetup.removeLocationUpdates();
      joinSessionSetup.save();
    });

    componentWillUnmount() {
      this.didBlurSubscription.remove();
      this.didFocusSubscription.remove();
    }

    joinSession = async () => {
      const {loginScreenViewModel} = this.props.store;
      if (
        !loginScreenViewModel.isOnTrial() &&
        !loginScreenViewModel.isActivated
      ) {
        Toast.show('License Activation is required.');
        return;
      }
      const {navigate} = this.props.navigation;
      const joinSessionSetup = this.props.store.joinSessionSetup;

      if (
        !joinSessionSetup.userName ||
        joinSessionSetup.userName.trim() == ''
      ) {
        Toast.show('User Name is required.');
        return;
      }
      if (!joinSessionSetup.selectedSession) {
        Toast.show('Select Session.');
        return;
      }
      joinSessionSetup.joinSession(
        () => {
          navigate('UserManager');
        },
        err => {
          delay(100).then(() => Alert.alert('Error', err));
        },
      );
    };

    render() {
      const joinSessionSetup = this.props.store.joinSessionSetup;
      const availableSessionsItems = joinSessionSetup.availableSessions.map(
        (s, i) => {
          return <Picker.Item key={i} value={s} label={s.name} />;
        },
      );

      const availableDroneManufactures = joinSessionSetup.droneManufactures.map(
        (s, i) => {
          return <Picker.Item key={i} value={s} label={s} />;
        },
      );

      const sessionRangesItems = SessionRanges.map((s, i) => {
        return <Picker.Item key={i} value={s.value} label={s.label} />;
      });

      return (
        <Container style={styles.container}>
          <GradientNavigationBar
            navigation={this.props.navigation}
            back
            titleText="Join a Session"
          />
          <Loader loading={joinSessionSetup.loading} />
          <Content padder bounces={false}>
            <View style={styles.inputForm}>
              <Item floatingLabel style={MainStyle.marginV10}>
                <Label style={MainStyle.labelColor}>Enter User Name</Label>
                <Input
                  value={joinSessionSetup.userName}
                  onChangeText={joinSessionSetup.setUserName}
                />
              </Item>

              <Item picker style={[MainStyle.marginV10, styles.picker]}>
                <Label style={MainStyle.labelColor}>Session Range</Label>
                <Picker
                  iosIcon={
                    <Image
                      source={BackArrow}
                      style={{
                        backgroundColor: 'transparent',
                        width: 10,
                        height: 10,
                        color: 'black',
                        tintColor: 'black',
                        transform: [{rotate: '-90deg'}],
                      }}
                    />
                  }
                  mode="dialog"
                  renderHeader={backAction => (
                    <Header style={{backgroundColor: mainColor}}>
                      <Left>
                        <Button transparent onPress={backAction}>
                          {/* <Icon name="arrow-back" style={{color: '#fff'}} /> */}
                          <Image
                            source={BackArrow}
                            style={{
                              backgroundColor: 'transparent',
                              width: 30,
                              height: 30,
                              color: 'white',
                              tintColor: 'white',
                            }}
                          />
                        </Button>
                      </Left>
                      <Body style={{flex: 3, alignItems: 'center'}}>
                        <Title style={{color: '#fff'}}>Select Range</Title>
                      </Body>
                      <Right />
                    </Header>
                  )}
                  placeholder="SESSION RANGE"
                  placeholderStyle={styles.placeHolder}
                  placeholderIconColor="black"
                  selectedValue={joinSessionSetup.range}
                  onValueChange={newRange =>
                    joinSessionSetup.setRange(newRange)
                  }>
                  {sessionRangesItems}
                </Picker>
              </Item>

              {joinSessionSetup.availableSessions.length > 0 ? (
                <Item picker style={[MainStyle.marginV10, styles.picker]}>
                  <Picker
                    iosIcon={
                      <Image
                        source={BackArrow}
                        style={{
                          backgroundColor: 'transparent',
                          width: 10,
                          height: 10,
                          color: 'black',
                          tintColor: 'black',
                          transform: [{rotate: '-90deg'}],
                        }}
                      />
                    }
                    mode="dialog"
                    renderHeader={backAction => (
                      <Header style={{backgroundColor: mainColor}}>
                        <Left>
                          <Button transparent onPress={backAction}>
                            {/* <Icon name="arrow-back" style={{color: '#fff'}} /> */}
                            <Image
                              source={BackArrow}
                              style={{
                                backgroundColor: 'transparent',
                                width: 30,
                                height: 30,
                                color: 'white',
                                tintColor: 'white',
                              }}
                            />
                          </Button>
                        </Left>
                        <Body style={{flex: 3, alignItems: 'center'}}>
                          <Title style={{color: '#fff'}}>Select Session</Title>
                        </Body>
                        <Right />
                      </Header>
                    )}
                    placeholder="View Available Sessions"
                    placeholderStyle={styles.placeHolder}
                    placeholderIconColor="white"
                    selectedValue={joinSessionSetup.selectedSession}
                    onValueChange={newSelectedSession =>
                      joinSessionSetup.setSelectedSession(newSelectedSession)
                    }>
                    <Picker.Item label="View Available Sessions" value={null} />
                    {availableSessionsItems}
                  </Picker>
                </Item>
              ) : (
                <Item style={MainStyle.marginV10}>
                  <Label style={styles.loadingSessions}>Loading Sessions</Label>
                </Item>
              )}

              <Item floatingLabel style={MainStyle.marginV10}>
                <Label style={MainStyle.labelColor}>Session Password</Label>
                <Input
                  autoCapitalize="none"
                  secureTextEntry={true}
                  value={joinSessionSetup.password}
                  onChangeText={joinSessionSetup.setPassword}
                />
              </Item>

              <Item style={MainStyle.marginV20}>
                <View style={styles.droneSessionView}>
                  <Label style={MainStyle.labelColor}>
                    Join Session with Drone
                  </Label>
                  <Switch
                    onValueChange={joinSessionSetup.setIsDrone}
                    value={joinSessionSetup.isDrone}
                  />
                </View>
              </Item>

              {joinSessionSetup.isDrone && (
                <Item picker style={[MainStyle.marginV10, styles.picker]}>
                  <Picker
                    iosIcon={
                      <Image
                        source={BackArrow}
                        style={{
                          backgroundColor: 'transparent',
                          width: 10,
                          height: 10,
                          color: 'black',
                          tintColor: 'black',
                          transform: [{rotate: '-90deg'}],
                        }}
                      />
                    }
                    mode="dialog"
                    renderHeader={backAction => (
                      <Header style={{backgroundColor: mainColor}}>
                        <Left>
                          <Button transparent onPress={backAction}>
                            {/* <Icon name="arrow-back" style={{ color: '#fff' }} /> */}
                            <Image
                              source={BackArrow}
                              style={{
                                backgroundColor: 'transparent',
                                width: 30,
                                height: 30,
                                color: 'white',
                                tintColor: 'white',
                              }}
                            />
                          </Button>
                        </Left>
                        <Body style={{flex: 3, alignItems: 'center'}}>
                          <Title style={{color: '#fff'}}>
                            Select Drone Manufacture
                          </Title>
                        </Body>
                        <Right />
                      </Header>
                    )}
                    placeholder="Select Drone Manufacture"
                    placeholderStyle={styles.placeHolder}
                    placeholderIconColor="black"
                    selectedValue={joinSessionSetup.selectedDroneManufacture}
                    onValueChange={newSelectedDroneManufacture =>
                      joinSessionSetup.setSelectedDroneManufacture(
                        newSelectedDroneManufacture,
                      )
                    }
                    style={styles.droneManufacturerPicker}>
                    <Picker.Item
                      label="Select Drone Manufacture"
                      value={null}
                    />
                    {availableDroneManufactures}
                  </Picker>
                </Item>
              )}
              <View style={MainStyle.marginV10}>
                <GradientButton
                  onPressButton={this.joinSession.bind(this)}
                  setting={shadowOpt}
                  btnText="Join"
                />
              </View>
            </View>
          </Content>
        </Container>
      );
    }
  },
);

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
  inputForm: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  picker: {
    width: '100%',
  },
  placeHolder: {color: 'black'},
  loadingSessions: {
    flex: 1,
    ...MainStyle.marginV10,
    ...MainStyle.labelColor,
  },
  droneSessionView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 5,
  },
  droneManufacturerPicker: {
    flex: 1,
  },
});

export default inject('store')(UserMainScreen);
