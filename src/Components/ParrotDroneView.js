import {inject, observer} from 'mobx-react';
import {Button} from 'native-base';
import React from 'react';
import {
  Image,
  requireNativeComponent,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {colors} from '../styles/variables';
import crashlytics from '@react-native-firebase/crashlytics';
import Orientation from 'react-native-orientation';

const _renderMiniListItem = observer(({item, index}) => {
  const gasItem = item;
  const labelColor = gasItem.isAlarmTriggered
    ? MainStyle.labelColorRed
    : {color: 'white'};

  return (
    <Text style={[labelColor, styles.text]}>
      <Text style={styles.textAlt}>{gasItem.gasName}: </Text> {gasItem.value}
      {gasItem.gasScale !== '%' ? ' ' + gasItem.gasScale : gasItem.gasScale}
    </Text>
  );
});

const ParrotDroneView = observer(
  class ParrotDroneView extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        landingState: 'TAKE OFF',
        manualUpdate: 'xafjkdfad',
        returnHomeState: false,
      };
    }

    componentDidMount = () => {
      Orientation.lockToLandscape();
    };

    componentDidUnMount = () => {
      Orientation.unlockAllOrientations();
    };

    render() {
      crashlytics().log('User in Parrot Drone Screen');
      const landingState = this.state.landingState;
      const userGameViewModel = this.props.store.userManagerViewModel.usersData;

      let speed = 0;
      if (null !== userGameViewModel) {
        speed =
          Math.pow(userGameViewModel.Info.velocityX, 2) +
          Math.pow(userGameViewModel.Info.velocityY, 2) +
          Math.pow(userGameViewModel.Info.velocityZ, 2);
        speed = Math.sqrt(speed);
        speed = Math.round(speed * 100) / 100;
      }

      return (
        <View
          style={{
            width: '100%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#2b2b2b',
          }}>
          {userGameViewModel && (
            <View style={styles.topGasBar}>
              <Text
                style={{
                  paddingLeft: 5,
                  marginTop: -4,
                }}>
                <Image
                  source={require('../Images/logo_.png')}
                  style={styles.logoIcon}
                />
              </Text>

              {userGameViewModel.Gas.map((item, index) => (
                <_renderMiniListItem item={item} index={index} />
              ))}
            </View>
          )}
          <ParrotDrone
            style={styles.parrotDrone}
            onDroneFound={e => {
              this.props.store.userManagerViewModel.updateDroneInfo(
                e.nativeEvent,
              );
            }}
            onDroneConnected={e => {
              this.props.store.userManagerViewModel.updateDroneInfo(
                e.nativeEvent,
              );
            }}
            onLocationChanged={e => {
              this.props.store.userManagerViewModel.updateTelemetryInfo(
                e.nativeEvent,
              );
            }}
            onSpeedChanged={e => {
              this.props.store.userManagerViewModel.updateSpeedInfo(
                e.nativeEvent,
              );
            }}
            manualPiloting={this.state.manualUpdate}
            onDroneLanded={() => {
              this.setState({
                landingState: 'Take Off',
              });
            }}
            onDroneFlying={() => {
              this.setState({
                landingState: 'Land',
              });
            }}
            onThrowTakeoff={() => {
              this.setState({
                landingState: 'Take Off',
              });
            }}
            returnHome={this.state.returnHomeState}
            onReturnHome={() => {
              this.setState({
                returnHomeState: true,
              });
            }}
            onReturnHomeIdle={() => {
              this.setState({
                returnHomeState: false,
              });
            }}
          />
          {userGameViewModel ? (
            <>
              <View style={styles.view}>
                <View style={styles.statusBar}>
                  <Text
                    style={[
                      styles.statusText,
                      {
                        color:
                          userGameViewModel.Info.droneConnectionStatus ===
                          'CONNECTED'
                            ? 'green'
                            : 'red',
                      },
                    ]}>
                    {userGameViewModel.Info.droneConnectionStatus}
                  </Text>

                  <View style={styles.topIndicators}>
                    <Text
                      style={[
                        styles.statusText,
                        {
                          color: colors.white,
                        },
                      ]}>
                      <Text style={[styles.textAlt, {fontSize: 12}]}>
                        Model:
                      </Text>{' '}
                      {userGameViewModel.Info.droneModel &&
                      userGameViewModel.Info.droneModel !== ''
                        ? userGameViewModel.Info.droneModel
                        : 'PARROT'}
                    </Text>

                    {speed > 0 ? (
                      <Text
                        style={[
                          styles.statusText,
                          {
                            color: colors.white,
                          },
                        ]}>
                        <Text style={[styles.textAlt, {fontSize: 12}]}>
                          Speed:
                        </Text>{' '}
                        {speed}
                      </Text>
                    ) : null}
                  </View>
                </View>
              </View>

              <View style={[styles.viewBottom]}>
                <Button
                  onPress={() => {
                    this.setState({
                      manualUpdate: (
                        Math.random() * (10 - 999999) +
                        10
                      ).toString(),
                    });
                  }}
                  style={[styles.buttonBottom]}>
                  <Text
                    style={{
                      color: colors.softBlue,
                    }}>
                    {landingState}
                  </Text>
                </Button>

                <Button
                  onPress={() => {
                    this.setState({
                      returnHomeState: !this.state.returnHomeState,
                    });
                  }}
                  style={[styles.buttonBottom]}>
                  <Text
                    style={{
                      color: colors.softBlue,
                    }}>
                    {this.state.returnHomeState
                      ? 'Cancel Return Home'
                      : 'Return Home'}
                  </Text>
                </Button>
              </View>
            </>
          ) : null}
        </View>
      );
    }
  },
);

const styles = StyleSheet.create({
  parrotDrone: {
    width: '100%',
    height: '100%',
  },
  topIndicators: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  statusBar: {
    width: '100%',
    backgroundColor: 'rgba(0,0,0,0.5)',
    height: 25,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 6,
    justifyContent: 'space-between',
  },
  topGasBar: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    height: 22,
    zIndex: 999,
    alignSelf: 'center',
    top: 60,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 6,
    position: 'absolute',
  },
  text: {
    fontSize: 10,
    paddingVertical: 5,
    paddingHorizontal: 5,
  },
  textAlt: {
    fontSize: 10,
    color: 'gray',
  },
  statusText: {
    fontSize: 12,
    paddingHorizontal: 6,
  },
  imageWrap: {
    paddingLeft: 5,
  },
  logoIcon: {
    width: 46,
    height: 15,
  },
  view: {
    width: '100%',
    height: 40,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    position: 'absolute',
    top: 0,
    zIndex: 999,
  },
  viewBottom: {
    bottom: 0,
    width: '100%',
    height: 40,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    position: 'absolute',
    zIndex: 999,
  },
  buttonBottom: {
    marginBottom: 15,
    marginLeft: 15,
    height: 30,
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
    borderWidth: 1,
    paddingHorizontal: 5,
  },
  button: {
    paddingHorizontal: 5,
    marginTop: 15,
    marginLeft: 15,
    height: 30,
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
    borderWidth: 1,
    borderColor: colors.softBlue,
  },
});

const ParrotDrone = requireNativeComponent('ParrotDroneView');

export default inject('store')(ParrotDroneView);
