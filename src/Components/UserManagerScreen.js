/* eslint-disable indent */
import React from 'react';
import {
  View,
  Image,
  Alert,
  TouchableOpacity,
  StyleSheet,
  Platform,
  SafeAreaView,
} from 'react-native';
import {Container, List, ListItem, Grid, Col, Icon} from 'native-base';
import KeepAwake from 'react-native-keep-awake';

import {inject, observer} from 'mobx-react';
import MainStyle from '../StyleSheets/MainStyle';

import Text from '../elements/Text';
import GradientNavigationBar from '../elements/GradientNavigationBar';

import {blueGradient, colors} from '../styles/variables';
import {delay, getElevation} from '../Utils';
import DroneParamItem from '../elements/DroneParamItem';
import DroneModelItem from '../elements/DroneModelItem';
import LinearGradient from 'react-native-linear-gradient';
import DJIDroneView from './DJIDroneView';
import Orientation from 'react-native-orientation';
import ParrotDroneView from './ParrotDroneView';

const _renderReadingFlatListItem = observer(({item, index}) => {
  const gasItem = item;
  const labelColor = gasItem.isAlarmTriggered
    ? MainStyle.labelColorRed
    : MainStyle.labelColor;
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.readingListItemView}>
        <ListItem style={{paddingBottom: 5, paddingTop: 5}}>
          <Grid>
            <Col style={{}}>
              <Text
                style={[
                  labelColor,
                  styles.text,
                  {fontWeight: '600', fontSize: 20},
                ]}>
                {gasItem.gasName}
              </Text>
            </Col>

            <Col style={{alignItems: 'center'}}>
              <Text
                style={[
                  labelColor,
                  styles.text,
                  {paddingTop: 40, fontSize: 30, fontWeight: 'bold'},
                ]}>
                {gasItem.value}
              </Text>
            </Col>

            <Col style={{alignItems: 'flex-end'}}>
              <Text
                style={[
                  labelColor,
                  styles.text,
                  {fontSize: 20, fontWeight: '600'},
                ]}>
                {gasItem.gasScale}
              </Text>
            </Col>
          </Grid>
        </ListItem>
      </View>
    </SafeAreaView>
  );
});

const _renderPeekFlatListItem = observer(({item, index}) => {
  const gasItem = item;
  const labelColor = gasItem.isAlarmTriggered
    ? MainStyle.labelColorRed
    : MainStyle.labelColor;

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.peekFlatListView}>
        {index === 0 && (
          <ListItem itemDivider style={styles.peekFlatListItem}>
            <Grid>
              <Col>
                <Text style={styles.text}>Name</Text>
              </Col>
              <Col style={{alignItems: 'center'}}>
                <Text style={styles.text}>Low</Text>
              </Col>
              <Col style={{alignItems: 'flex-end'}}>
                <Text style={styles.text}>High</Text>
              </Col>
            </Grid>
          </ListItem>
        )}

        <ListItem style={styles.peekFlatListGasItem}>
          <Grid>
            <Col>
              <Text
                style={[
                  labelColor,
                  styles.text,
                  {fontSize: 20, fontWeight: '600'},
                ]}>
                {gasItem.gasName}
              </Text>
            </Col>
            <Col style={{alignItems: 'center'}}>
              <Text
                style={[
                  labelColor,
                  styles.text,
                  {fontSize: 20, fontWeight: '600'},
                ]}>
                {gasItem.lowValue}
              </Text>
            </Col>
            <Col style={{alignItems: 'flex-end'}}>
              <Text
                style={[
                  labelColor,
                  styles.text,
                  {fontSize: 20, fontWeight: '600'},
                ]}>
                {gasItem.maxValue}
              </Text>
            </Col>
          </Grid>
        </ListItem>
      </View>
    </SafeAreaView>
  );
});

const _renderMiniListItem = observer(({item, index}) => {
  const gasItem = item;
  const labelColor = gasItem.isAlarmTriggered
    ? MainStyle.labelColorRed
    : {color: 'white'};

  return (
    <Text style={[labelColor, styles.minilistText]}>
      <Text style={styles.textAlt}>{gasItem.gasName}: </Text> {gasItem.value}
      {gasItem.gasScale !== '%' ? ' ' + gasItem.gasScale : gasItem.gasScale}
    </Text>
  );
});

const TopGasBar = ({userGameViewModel}) => (
  <View style={styles.topGasBar}>
    <Text
      style={{
        paddingLeft: 5,
        marginTop: 4,
      }}>
      <Image source={require('../Images/logo_.png')} style={styles.logoIcon} />
    </Text>

    {userGameViewModel.Gas.map((item, index) => (
      <_renderMiniListItem item={item} index={index} />
    ))}
  </View>
);

const UserManagerScreen = observer(
  class UserManagerScreen extends React.Component {
    static navigationOptions = {headerShown: false};
    didFocusSubscription = this.props.navigation.addListener('didFocus', () => {
      const joinSessionSetup = this.props.store.joinSessionSetup;
      const userManagerViewModel = this.props.store.userManagerViewModel;

      userManagerViewModel.setSessionName(
        joinSessionSetup.selectedSession.name,
      );
      userManagerViewModel.setUserKey(joinSessionSetup.key);

      userManagerViewModel.onDisconnectDeleteUserData();

      userManagerViewModel.registerToUserDataUpdate(() => {
        this.props.navigation.popToTop();
      });

      userManagerViewModel.registerToUserMessageUpdate((messageKey, msg) => {
        const userManagerViewModel = this.props.store.userManagerViewModel;
        delay(100).then(() =>
          Alert.alert(
            'Message From Host',
            msg, // <- this part is optional, you can pass an empty string
            [
              {
                text: 'OK',
                onPress: () =>
                  userManagerViewModel.removeUserMessage(messageKey),
              },
            ],
            {cancelable: false},
          ),
        );
      });
      if (!joinSessionSetup.isDrone) {
        userManagerViewModel.startLocationUpdates();
      }
    });

    didBlurSubscription = this.props.navigation.addListener(
      'didBlur',
      this.onUnmountScreen,
    );

    onUnmountScreen = () => {
      const joinSessionSetup = this.props.store.joinSessionSetup;
      const userManagerViewModel = this.props.store.userManagerViewModel;
      userManagerViewModel.unRegisterToUserDataUpdate();
      userManagerViewModel.unRegisterToUserMessageUpdate();

      if (!joinSessionSetup.isDrone) {
        userManagerViewModel.removeLocationUpdates();
      } else {
        if (joinSessionSetup.isDJIDrone()) {
          userManagerViewModel.removeDJIDroneData();
        } else {
          userManagerViewModel.removeParrotDroneData();
        }
      }
      userManagerViewModel.stopGasAlarm();
    };

    componentWillUnmount() {
      Orientation.unlockAllOrientations();
      if (Orientation.getInitialOrientation() === 'PORTRAIT') {
        // Orientation.lockToPortrait();
      }
      const userManagerViewModel = this.props.store.userManagerViewModel;
      userManagerViewModel.deleteUserData();
      this.onUnmountScreen();
      this.didBlurSubscription.remove();
      this.didFocusSubscription.remove();
    }

    componentDidMount() {
      this.checkIfDroneStatusChanged();
    }

    componentDidUpdate() {
      this.checkIfDroneStatusChanged();
    }

    checkIfDroneStatusChanged() {
      if (
        this.props.store.joinSessionSetup &&
        this.props.store.joinSessionSetup.isDrone
      ) {
        // Orientation.lockToLandscape();
      } else {
        Orientation.unlockAllOrientations();
        if (Orientation.getInitialOrientation() === 'PORTRAIT') {
          // Orientation.lockToPortrait();
        }
      }
    }

    resetPeek = () => {
      const userManagerViewModel = this.props.store.userManagerViewModel;
      userManagerViewModel.resetPeek();
    };

    render() {
      const joinSessionSetup = this.props.store.joinSessionSetup;
      const userManagerViewModel = this.props.store.userManagerViewModel;
      const userGameViewModel = this.props.store.userManagerViewModel.usersData;
      const showPeek = userManagerViewModel.showPeek;
      let speed = 0;
      if (null !== userGameViewModel && joinSessionSetup.isDrone) {
        speed =
          Math.pow(userGameViewModel.Info.velocityX, 2) +
          Math.pow(userGameViewModel.Info.velocityY, 2) +
          Math.pow(userGameViewModel.Info.velocityZ, 2);
        speed = Math.sqrt(speed);
        speed = Math.round(speed * 100) / 100;
      }
      var rightButtons = [
        {
          key: 1,
          buttonIcon: showPeek
            ? require('../../img/read.png')
            : require('../../img/peak.png'),
          buttonAction: () => userManagerViewModel.toggleShowPeek(),
          buttonWidth: 26,
          buttonHeight: 26,
        },
      ];
      if (showPeek)
        rightButtons.push({
          key: 2,
          buttonIcon: require('../../img/reset.png'),
          buttonAction: () => this.resetPeek(),
          buttonWidth: 26,
          buttonHeight: 23,
        });
      return (
        <Container>
          {joinSessionSetup.isDrone ? null : (
            <GradientNavigationBar
              navigation={this.props.navigation}
              back
              titleText="In Session"
              rightButtons={rightButtons}
            />
          )}
          {null !== userGameViewModel && !joinSessionSetup.isDrone ? (
            <View style={styles.view}>
              <View style={styles.logoView}>
                <Image
                  source={require('../Images/logo_.png')}
                  style={styles.logo}
                />
              </View>
              <SafeAreaView>
                <LinearGradient
                  start={{x: 0.2, y: 0.4}}
                  end={{x: 1.2, y: 1.0}}
                  colors={blueGradient.colors}
                  // eslint-disable-next-line react-native/no-inline-styles
                  style={{
                    ...getElevation(5),
                    width: '95%',
                    minHeight: joinSessionSetup.isDrone.isDrone ? 150 : 50,
                    marginTop: 15,
                    alignSelf: 'center',
                    padding: 10,
                    justifyContent: !joinSessionSetup.isDrone
                      ? 'center'
                      : 'space-between',
                    borderRadius: 15,
                  }}>
                  <View style={styles.userInfo}>
                    <Text style={styles.userName}>
                      {userGameViewModel.Info.userName}
                    </Text>
                    {null !== userGameViewModel && joinSessionSetup.isDrone && (
                      <DroneModelItem
                        model={userGameViewModel.Info.droneModel}
                      />
                    )}
                  </View>
                  {null !== userGameViewModel && joinSessionSetup.isDrone && (
                    <View style={styles.droneParamView}>
                      <DroneParamItem
                        name="STATUS"
                        value={userGameViewModel.Info.droneConnectionStatus}
                      />
                      <DroneParamItem
                        name="ALTITUDE"
                        value={userGameViewModel.Info.altitude}
                      />
                      <DroneParamItem name="SPEED" value={speed} />
                    </View>
                  )}
                </LinearGradient>
              </SafeAreaView>

              <List
                style={{marginTop: 10}}
                dataArray={userGameViewModel.Gas}
                renderItem={({item, index}) =>
                  showPeek ? (
                    <_renderPeekFlatListItem item={item} index={index} />
                  ) : (
                    <_renderReadingFlatListItem item={item} index={index} />
                  )
                }
              />
            </View>
          ) : null}

          {null !== userGameViewModel &&
          joinSessionSetup &&
          joinSessionSetup.isDrone ? (
            <View style={styles.joinSessionView}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.goBack();
                }}
                activeOpacity={0.5}
                style={styles.signOutIconView}>
                <Icon
                  type="FontAwesome"
                  name="sign-out"
                  style={styles.signOutIcon}
                />
              </TouchableOpacity>

              <View style={styles.joinSessionSetupView}>
                {!joinSessionSetup ? (
                  <View style={styles.sessionView} />
                ) : joinSessionSetup.isDJIDrone() ? (
                  <DJIDroneView>
                    {Platform.OS === 'android' ? (
                      <>
                        <Text style={styles.imageWrap}>
                          <Image
                            source={require('../Images/logo_.png')}
                            style={styles.logoIcon}
                          />
                        </Text>

                        {userGameViewModel.Gas.map((item, index) => (
                          <_renderMiniListItem item={item} index={index} />
                        ))}
                      </>
                    ) : (
                      <TopGasBar userGameViewModel={userGameViewModel} />
                    )}
                  </DJIDroneView>
                ) : (
                  <ParrotDroneView />
                )}
              </View>
            </View>
          ) : null}

          <KeepAwake />
        </Container>
      );
    }
  },
);

const styles = StyleSheet.create({
  readingListItemView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  topGasBar: {
    backgroundColor: 'rgba(0,0,0,0.7)',
    height: 22,
    zIndex: 999,
    alignSelf: 'center',
    top: 60,
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageWrap: {
    paddingLeft: 5,
  },
  readingListNameValue: {
    justifyContent: 'space-around',
    backgroundColor: 'transparent',
  },
  readingListValueGasScale: {
    justifyContent: 'space-around',
  },
  text: {
    fontSize: 16,
  },
  textAlt: {
    fontSize: 11.5,
    color: 'white',
  },
  minilistText: {
    fontSize: 11.5,
    paddingVertical: 5,
    paddingHorizontal: 5,
    color: 'white',
  },
  miniListView: {
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  miniListGasItemView: {
    paddingHorizontal: 4,
  },
  peekFlatListView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  peekFlatListItem: {
    justifyContent: 'space-around',
    backgroundColor: 'transparent',
  },
  peekFlatListGasItem: {
    justifyContent: 'space-around',
  },
  signOutView: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    height: 50,
    width: 100,
  },
  signOutIconView: {
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 25,
    right: 0,
    zIndex: 999,
    backgroundColor: 'rgba(0,0,0,0.5)',
    paddingVertical: 10,
    paddingHorizontal: 4,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  signOutIcon: {
    color: 'white',
    fontSize: 20,
  },
  logoIconView: {
    alignItems: 'flex-start',
    width: 80,
    position: 'absolute',
    right: 0,
    zIndex: 999,
    top: 40,
    backgroundColor: 'rgba(0,0,0,0.5)',
    paddingVertical: 4,
    paddingHorizontal: 4,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  logoIcon: {
    width: 46,
    height: 15,
  },
  joinSessionView: {
    width: '100%',
    height: '100%',
  },
  joinSessionSetupView: {
    width: '100%',
  },
  sessionView: {
    backgroundColor: 'black',
    width: '100%',
    height: '100%',
  },
  reconnectText: {
    color: colors.white,
    fontSize: 20,
    letterSpacing: 1,
  },
  reconnectView: {
    height: 50,
    justifyContent: 'center',
    marginTop: 15,
    borderTopColor: colors.white,
    borderTopWidth: 1,
  },
  droneParamView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 15,
    alignItems: 'center',
  },
  userName: {
    fontSize: 24,
    alignSelf: 'flex-start',
    color: colors.white,
  },
  userInfo: {
    flexDirection: 'row',
    // justifyContent: 'space-between',
    justifyContent: 'center',
    alignItems: 'center',
  },
  view: {
    flex: 1,
  },
  logoView: {
    alignItems: 'center',
    marginTop: 30,
  },
  logo: {
    width: 200,
    height: 57,
  },
});

export default inject('store')(UserManagerScreen);
