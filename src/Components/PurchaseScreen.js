import React from 'react';
import {View, ScrollView, TextInput, Alert} from 'react-native';
import {Container, Content, Button, Icon, Col, Row} from 'native-base';
import moment from 'moment';
import jwt_decode from 'jwt-decode';

import {inject, observer} from 'mobx-react';
import MainStyle, {mainColor} from '../StyleSheets/MainStyle';
import Loader from '../Controls/Loader';

import Text from '../elements/Text';
import GradientButton from '../elements/GradientButton';
import GradientNavigationBar from '../elements/GradientNavigationBar';
import {shadowOpt} from '../styles/variables';
import {StyleSheet} from 'react-native';

import firebaseHelper from '../Services/FirebaseHelper';
import cryptlexHelper, {FIREBASE_URL} from '../Services/CryptlexHelper';
import Toast from 'react-native-root-toast';

import {
  CardOne,
  CardTwo,
  CardThree,
  CardFour,
  CardFive,
  CardSix,
  CardSeven,
  CardEight,
  CardNine,
  CardTen,
  CardEleven,
  CardTwelve,
  CardEcomOne,
  CardEcomTwo,
  CardEcomThree,
  CardEcomFour,
} from 'react-native-card-ui';

const PurchaseScreen = observer(
  class PurchaseScreen extends React.Component {
    static navigationOptions = {headerShown: false};
    loadedCount = 0;

    constructor(props) {
      super(props);

      this.state = {
        isPurchasing: false,
        isLoading: false,
        isSuccess: false,
        licenses: [],
        licenseText: '',
        user: '',
        email: '',
      };
    }

    async componentDidMount() {
      const loginScreenViewModel = this.props.store.loginScreenViewModel;
      loginScreenViewModel.goOnline();
      const {uid} = this.props.store.loginScreenViewModel.firebaseUser;
      firebaseHelper.registerToLicenseUpdate(
        uid,
        this.onLicenseChange.bind(this),
      );
      const emailId = await firebaseHelper.getUserEmail(uid);
      this.setState({email: emailId});
    }

    componentWillUnmount() {
      const {uid} = this.props.store.loginScreenViewModel.firebaseUser;
      firebaseHelper.unRegisterToLicenseUpdate(
        uid,
        this.onLicenseChange.bind(this),
      );
    }

    onLicenseChange(value) {
      if (value == null || !value || !value.val()) {
        this.setState({licenses: []});
        return;
      }
      this.setState({
        licenses: Object.values(value.val()),
      });
    }

    async onEnterLicense() {
      const {licenseText} = this.state;
      if (licenseText == '') return;
      this.setState({isLoading: true});
      const {uid} = this.props.store.loginScreenViewModel.firebaseUser;
      var response = await cryptlexHelper.activate(uid, licenseText);
      const {activationToken, message} = response;
      if (!activationToken) {
        this.setState({isLoading: false});
        Toast.show(message);
        return;
      }

      const activateResult = jwt_decode(activationToken);
      const {lid, key, eat} = activateResult;
      const expiresAt = eat * 1000;

      await firebaseHelper.addNewLicense(uid, {
        id: lid,
        key: key,
        expiresAt: expiresAt,
        createdAt: Date.now(),
        userId: uid,
      });
      this.props.store.loginScreenViewModel.setActivation();
      this.setState({isLoading: false, licenseText: ''});
    }

    async onActivate(index) {
      const license = this.state.licenses[index];
      const {key} = license;
      const {uid} = this.props.store.loginScreenViewModel.firebaseUser;
      this.setState({
        isLoading: true,
      });
      try {
        this.props.store.loginScreenViewModel.deActivation(false);
        const response = await cryptlexHelper.activate(uid, key);
        var {activationToken, message} = response;
        console.log('activationToken', activationToken);
        if (activationToken) {
          const activateResult = jwt_decode(activationToken);
          const {lid} = activateResult;
          this.props.store.loginScreenViewModel.licenseId = lid;
          message = 'Activated Successfully.';
          this.props.store.loginScreenViewModel.setActivation();
        }
        Toast.show(message);
      } catch (error) {
        console.log(error);
      }
      this.setState({
        isLoading: false,
      });
    }

    onRemove(index) {
      Alert.alert(
        'Confirm',
        'Do you wanna remove this license?',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'OK',
            onPress: () => {
              const license = this.state.licenses[index];
              const {id} = license;
              const {uid} = this.props.store.loginScreenViewModel.firebaseUser;
              firebaseHelper.removeLicense(uid, id);
            },
          },
        ],
        {cancelable: false},
      );
    }

    render() {
      const {isPurchasing, isLoading, licenses} = this.state;
      const {uid} = this.props.store.loginScreenViewModel.firebaseUser;
      const {isActivated, licenseId} = this.props.store.loginScreenViewModel;
      const {navigate} = this.props.navigation;

      if (!uid) {
        return null;
      }

      return (
        <Container>
          <GradientNavigationBar
            navigation={this.props.navigation}
            back
            titleText="Purchase"
          />
          <Content contentContainerStyle={styles.content} bounces={false}>
            <Loader loading={isLoading} />

            <>
              <View style={styles.activatedView}>
                {isActivated ? (
                  <>
                    <Icon
                      type="FontAwesome"
                      name="check-circle-o"
                      style={styles.activatedCircleIcon}
                    />
                    <Text style={MainStyle.marginV10}>
                      Congratulations! You are activated.
                    </Text>
                  </>
                ) : (
                  <>
                    <Icon
                      type="FontAwesome"
                      name="opencart"
                      style={styles.activatedCartIcon}
                    />
                    <Text style={MainStyle.marginV10}>
                      You are not activated.
                    </Text>
                  </>
                )}
              </View>
              <View
                style={[
                  {...styles.introPageButtonBox},
                  styles.purchaseLicenseView,
                ]}>
                <GradientButton
                  onPressButton={() => {
                    navigate('Payment', {
                      userId: uid,
                      email: this.state.email,
                      navigation: this.props,
                    });
                  }}
                  setting={shadowOpt}
                  btnText="PURCHASE NEW LICENSE"
                />
              </View>
              <View style={styles.licenseTextView}>
                <TextInput
                  style={styles.licenseTextInput}
                  onChangeText={text => this.setState({licenseText: text})}
                  placeholder="XXXXXX-XXXXXX-XXXXXX-XXXXXX-XXXXXX-XXXXXX"
                  value={this.state.licenseText}
                />
                <Button
                  icon
                  light
                  style={styles.licenseIconButton}
                  onPress={this.onEnterLicense.bind(this)}>
                  <Icon
                    type="FontAwesome"
                    name="check"
                    style={{color: mainColor}}
                  />
                </Button>
              </View>
              <ScrollView>
                {licenses.map(({id, createdAt, expiresAt, key}, index) => (
                  // eslint-disable-next-line react-native/no-inline-styles
                  <View
                    style={[
                      styles.licenseItem,
                      {
                        backgroundColor: licenseId === id ? '#97d9f7' : '#fff',
                      },
                    ]}
                    key={index}>
                    <Text style={styles.licenseText}>
                      {index + 1}. {key}
                    </Text>
                    <Row>
                      <Col size={4}>
                        <Text style={styles.licenseDesc}>
                          Created At:{' '}
                          {moment(createdAt).format('M/D/YY, hh:mm A')}
                        </Text>
                        <Text style={styles.licenseDesc}>
                          Expires At:{' '}
                          {moment(expiresAt).format('M/D/YY, hh:mm A')}
                        </Text>
                      </Col>
                      <Col>
                        <Button
                          icon
                          light
                          style={styles.licenseIconButton}
                          onPress={this.onActivate.bind(this, index)}>
                          <Icon
                            type="FontAwesome"
                            name="check"
                            style={{color: mainColor}}
                          />
                        </Button>
                      </Col>
                      <Col>
                        <Button
                          icon
                          light
                          style={styles.licenseIconButton}
                          onPress={this.onRemove.bind(this, index)}>
                          <Icon
                            type="FontAwesome"
                            name="trash"
                            style={styles.trashIcon}
                          />
                        </Button>
                      </Col>
                    </Row>
                  </View>
                ))}
              </ScrollView>
            </>
          </Content>
        </Container>
      );
    }
  },
);

const styles = StyleSheet.create({
  content: {flexGrow: 1, marginTop: 15},
  licenseItem: {
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    padding: 10,
    alignItems: 'flex-start',
  },
  licenseText: {
    flex: 1,
    fontSize: 14,
    paddingBottom: 5,
  },
  licenseDesc: {
    color: '#444',
    paddingBottom: 5,
  },
  licenseIconButton: {
    width: 55,
    backgroundColor: '#fff',
  },
  activatedView: {
    alignItems: 'center',
    marginVertical: 30,
  },
  activatedCircleIcon: {
    color: '#228B22',
    fontSize: 60,
  },
  activatedCartIcon: {
    color: '#FF6347',
    fontSize: 60,
  },
  purchaseLicenseView: {
    alignItems: 'center',
    marginBottom: 20,
  },
  licenseTextView: {
    alignItems: 'center',
    marginBottom: 20,
    display: 'flex',
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  licenseTextInput: {
    borderColor: '#ddd',
    borderWidth: 1,
    flex: 1,
    minHeight: 50,
  },
  trashIcon: {
    color: '#f00',
  },
});

export default inject('store')(PurchaseScreen);
