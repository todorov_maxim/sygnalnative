import React from 'react';
import {
  Button
} from 'react-native';

import {inject, observer} from 'mobx-react';

const HomeScreen = observer(
  class HomeScreen extends React.Component {
    static navigationOptions = {
      title: 'Welcome',
    };
    render() {
      const {navigate} = this.props.navigation;
      return (
        <Button
          title={this.props.store.text}
          onPress={() => {
            return navigate('Profile', {name: 'Jane'});
          }}
        />
      );
    }
  },
);

export default inject('store')(HomeScreen);
