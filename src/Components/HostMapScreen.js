import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Container} from 'native-base';
import MapView, {Marker, Polyline} from 'react-native-maps';

import {inject, observer} from 'mobx-react';
import {toJS} from 'mobx';

import GradientNavigationBar from '../elements/GradientNavigationBar';
import locationHelper from '../Services/LocationHelper';

const HostMapScreen = observer(
  class HostMapScreen extends React.Component {
    static navigationOptions = {headerShown: false};
    map = null;
    constructor(props) {
      super(props);
      this.state = {mapViewStyle: {flex: 1, marginHorizontal: 30}};
    }

    showUserPath = () => {
      const hostGameViewModel = this.props.store.hostGameViewModel;
      hostGameViewModel.setShowUserPath(!hostGameViewModel.showUserPath);

      this.props.navigation.setParams({
        showUserPathValue: hostGameViewModel.showUserPath,
        showUserPath: this.showUserPath,
      });
    };

    hostMode = false;
    componentDidMount() {
      const hostGameViewModel = this.props.store.hostGameViewModel;
      const hostMapViewModel = this.props.store.hostMapViewModel;
      const sessionSetup = this.props.store.sessionSetup;

      this.props.navigation.setParams({
        showUserPathValue: hostGameViewModel.showUserPath,
        showUserPath: this.showUserPath,
      });

      this.hostMode = this.props.navigation.getParam('hostMode');
      setTimeout(
        () => this.setState({mapViewStyle: {flex: 1, marginHorizontal: 0}}),
        500,
      );

      const latitude = this.hostMode
        ? locationHelper.latitude
        : this.props.navigation.getParam('latitude');
      const longitude = this.hostMode
        ? locationHelper.longitude
        : this.props.navigation.getParam('longitude');

      const sessionName = this.hostMode
        ? sessionSetup.name
        : this.props.navigation.getParam('sessionName');

      hostMapViewModel.setStartLatitude(latitude);
      hostMapViewModel.setStartLongitude(longitude);

      hostMapViewModel.setLatitude(latitude);
      hostMapViewModel.setLongitude(longitude);
      hostMapViewModel.setSessionName(sessionName);
    }

    didFocusSubscription = this.props.navigation.addListener('didFocus', () => {
      const hostGameViewModel = this.props.store.hostGameViewModel;
      hostGameViewModel.registerToAllUserDataUpdate();
    });

    didBlurSubscription = this.props.navigation.addListener('didBlur', () => {
      const hostGameViewModel = this.props.store.hostGameViewModel;
      hostGameViewModel.unRegisterToAllUserDataUpdate();
    });

    componentWillUnmount() {
      this.didBlurSubscription.remove();
      this.didFocusSubscription.remove();
    }

    render() {
      const {navigation} = this.props;
      const hostGameViewModel = this.props.store.hostGameViewModel;
      const hostMapViewModel = this.props.store.hostMapViewModel;

      const polylines = [];
      if (hostGameViewModel.showUserPath) {
        for (var userPolylineKey in hostGameViewModel.userPolylineMap) {
          let userPolyline = toJS(
            hostGameViewModel.userPolylineMap[userPolylineKey],
          );
          let coordinatesArray = userPolyline;
          if (coordinatesArray.length >= 2) {
            polylines.push(
              <Polyline
                // key={userInfoRecord.recordkey}
                coordinates={coordinatesArray}
                // strokeColor={hostGameViewModel.getUserInfoRecordColors(
                //   userInfoRecord.key,
                // )}
                strokeWidth={6}
              />,
            );
          }
        }
      }

      const isShowMap =
        hostMapViewModel.startLatitude && hostMapViewModel.startLongitude;

      return (
        <Container>
          <GradientNavigationBar
            navigation={this.props.navigation}
            back
            titleText="Map"
            rightButtons={[
              {
                key: 1,
                buttonIcon: require('../../img/path.png'),
                buttonAction: navigation.getParam('showUserPath'),
                buttonWidth: 26,
                buttonHeight: 26,
              },
            ]}
          />

          <View style={styles.container}>
            {isShowMap ? (
              <MapView
                ref={map => (this.map = map)}
                style={[this.state.mapViewStyle, styles.map]}
                initialRegion={{
                  latitude: hostMapViewModel.startLatitude,
                  longitude: hostMapViewModel.startLongitude,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
                }}
                followsUserLocation={false}
                showsUserLocation={false}
                showsMyLocationButton
                showsCompass
                onUserLocationChange={coordinate => {
                  if (this.hostMode) {
                    hostMapViewModel.setLatitude(coordinate.latitude);
                    hostMapViewModel.setLongitude(coordinate.longitude);
                  }
                }}>
                {hostGameViewModel.usersData.map(marker => (
                  <Marker
                    key={marker.key}
                    coordinate={{
                      latitude: marker.Info.latitude,
                      longitude: marker.Info.longitude,
                    }}
                    image={require('../Images/marker.png')}
                    title={marker.Info.userName}
                  />
                ))}

                <Marker
                  key={hostMapViewModel.sessionName}
                  coordinate={{
                    latitude: hostMapViewModel.latitude,
                    longitude: hostMapViewModel.longitude,
                  }}
                  title={`Session Host: ${hostMapViewModel.sessionName}`}
                  image={require('../Images/marker.png')}
                />

                {polylines.length > 0 && polylines}
              </MapView>
            ) : (
              <></>
            )}
          </View>
        </Container>
      );
    }
  },
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    marginTop: 15,
    ...StyleSheet.absoluteFillObject,
  },
});

export default inject('store')(HostMapScreen);
