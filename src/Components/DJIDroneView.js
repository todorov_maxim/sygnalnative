import {inject, observer} from 'mobx-react';
import React from 'react';
import {requireNativeComponent, StyleSheet} from 'react-native';
import crashlytics from '@react-native-firebase/crashlytics';
import Orientation from 'react-native-orientation';

const DJIDroneView = observer(
  class DJIDroneView extends React.Component {
    componentDidMount = () => {
      Orientation.lockToLandscape();
    };

    componentDidUnMount = () => {
      Orientation.unlockAllOrientations();
    };

    render() {
      crashlytics().log('User in DJI Drone Screen');
      return (
        <DJIDrone
          {...this.props}
          style={styles.view}
          onStateChanged={e => {
            try {
              this.props.store.userManagerViewModel.updateDroneInfo(
                e.nativeEvent,
              );
            } catch (e) {}
          }}
          onDownloadProgress={e => {
            try {
              console.log(e.nativeEvent);
            } catch (e) {}
          }}
          onTelemetryUpdated={e => {
            try {
              this.props.store.userManagerViewModel.updateTelemetryInfo(
                e.nativeEvent,
              );
              this.props.store.userManagerViewModel.updateSpeedInfo(
                e.nativeEvent,
              );
            } catch (e) {}
          }}
        />
      );
    }
  },
);

const styles = StyleSheet.create({
  view: {
    width: '100%',
    height: '100%',
  },
});

const DJIDrone = requireNativeComponent('DJIDroneView');

export default inject('store')(DJIDroneView);
