import React from 'react';
import {View, Image, StyleSheet} from 'react-native';
import {Container} from 'native-base';

import MainStyle from '../StyleSheets/MainStyle';

import {inject, observer} from 'mobx-react';
import {StackActions, NavigationActions} from 'react-navigation';

const LogoScreen = observer(
  class LogoScreen extends React.Component {
    static navigationOptions = {headerShown: false};

    didFocusSubscription = this.props.navigation.addListener(
      'didFocus',
      () => {
        const loginScreenViewModel = this.props.store.loginScreenViewModel;
        loginScreenViewModel.startListeningToStateChanged();
      },
    );

    didBlurSubscription = this.props.navigation.addListener(
      'didBlur',
      () => {
        const loginScreenViewModel = this.props.store.loginScreenViewModel;
      },
    );

    componentWillUnmount() {
      this.didBlurSubscription.remove();
      this.didFocusSubscription.remove();
    }

    componentDidMount() {
      setTimeout(() => {
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'Disclaimer'})],
        });
        this.props.navigation.dispatch(resetAction);
      }, 5000); //5000 milliseconds
    }

    render() {
      return (
        <Container>
          <View style={MainStyle.logoContainer}>
            <Image source={require('../Images/logo_white.png')} 
              style={styles.logo}/>
          </View>
        </Container>
      );
    }
  },
);

const styles=StyleSheet.create({
  logo: { 
    width: 300,
    height: 105 
  }
});

export default inject('store')(LogoScreen);
