import React, { PureComponent } from 'react';
import { StyleSheet, ActivityIndicator } from 'react-native';
import WebView from 'react-native-webview';
import GradientNavigationBar from '../elements/GradientNavigationBar';
import { Container, Content } from 'native-base';


const styles = StyleSheet.create({
  activityIndicator: {
    marginTop: 12
  },
  container:{ flex: 1},
  webView: { marginTop: 15 }
});
export default class WebViewScreen extends PureComponent {
    static navigationOptions = { headerShown: false };
    
    /*
     * WebView documentation is not very Googlable.
     * The best source is buried in their repo:
     * https://github.com/react-native-community/react-native-webview/blob/master/docs/Reference.md
     * https://github.com/react-native-community/react-native-webview/blob/master/docs/Guide.md
     */

    render() {
      const { navigation } = this.props;
      const uri = navigation.getParam('uri');
      return (
        <Container>
          <GradientNavigationBar
            navigation={this.props.navigation}
            back
            titleText="Sygnal"
          />
          <Content contentContainerStyle={styles.container} bounces={false}>
            <WebView
              source={{ uri }}
              scalesPageToFit
              style={[StyleSheet.absoluteFill,  styles.webView]}
              sharedCookiesEnabled
              startInLoadingState
              renderLoading={() => (
                <ActivityIndicator style={styles.activityIndicator} size="large" />
              )}
              cacheEnabled={false}
            />
          </Content>
        </Container >
      );
    }
}