import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import {
  Container,
  Picker,
  Item,
  Content,
  Header,
  Title,
  Button,
  Icon,
  Right,
  Body,
  Left,
} from 'native-base';
import crashlytics from '@react-native-firebase/crashlytics';
import {inject, observer} from 'mobx-react';
import MainStyle, {mainColor} from '../StyleSheets/MainStyle';

import {GasNames, GasScales} from '../Constants';

import {roundFloat} from '../Utils';

import Stepper from '../Controls/Stepper.js';

import GradientButton from '../elements/GradientButton';
import GradientNavigationBar from '../elements/GradientNavigationBar';

import {shadowOpt} from '../styles/variables';

import BackArrow from '../../img/left-arrow.png';

const GasAddScreen = observer(
  class GasAddScreen extends React.Component {
    static navigationOptions = {headerShown: false};

    render() {
      crashlytics().log('Visited Gas Add Screen');
      const sessionSetup = this.props.store.sessionSetup;
      const newGasSetup = sessionSetup.newGasSetup;

      const gasNameItems = GasNames.map((s, i) => {
        return <Picker.Item key={i} value={s.value} label={s.label} />;
      });

      const gasScaleItems = GasScales.map((s, i) => {
        return <Picker.Item key={i} value={s.label} label={s.label} />;
      });

      const editMode = this.props.navigation.getParam('editMode', false);

      return (
        <Container>
          <GradientNavigationBar
            navigation={this.props.navigation}
            back
            titleText="Add Gas"
          />
          <Content padder>
            <Item picker style={MainStyle.marginV10}>
              <Picker
                iosIcon={
                  <Image
                    source={BackArrow}
                    style={{
                      backgroundColor: 'transparent',
                      width: 10,
                      height: 10,
                      color: 'black',
                      tintColor: 'black',
                      transform: [{rotate: '-90deg'}],
                    }}
                  />
                }
                mode="dialog"
                renderHeader={backAction => (
                  <Header style={{backgroundColor: mainColor}}>
                    <Left>
                      <Button transparent onPress={backAction}>
                        {/* <Icon name="arrow-back" style={{ color: '#fff' }} /> */}
                        <Image
                          source={BackArrow}
                          style={{
                            backgroundColor: 'transparent',
                            width: 30,
                            height: 30,
                            color: 'white',
                            tintColor: 'white',
                          }}
                        />
                      </Button>
                    </Left>
                    <Body style={{flex: 3, alignItems: 'center'}}>
                      <Title style={{color: '#fff'}}>Select Range</Title>
                    </Body>
                    <Right />
                  </Header>
                )}
                placeholder="HAZARD NAME"
                placeholderStyle={styles.pickerPlaceholer}
                placeholderIconColor="black"
                selectedValue={newGasSetup.gasName}
                onValueChange={newRange => newGasSetup.setGasName(newRange)}>
                {gasNameItems}
              </Picker>
            </Item>

            <Item picker style={MainStyle.marginV10}>
              <Picker
                iosIcon={
                  <Image
                    source={BackArrow}
                    style={{
                      backgroundColor: 'transparent',
                      width: 10,
                      height: 10,
                      color: 'black',
                      tintColor: 'black',
                      transform: [{rotate: '-90deg'}],
                    }}
                  />
                }
                mode="dialog"
                renderHeader={backAction => (
                  <Header style={{backgroundColor: mainColor}}>
                    <Left>
                      <Button transparent onPress={backAction}>
                        {/* <Icon name="arrow-back" style={{color: '#fff'}} /> */}
                        <Image
                          source={BackArrow}
                          style={{
                            backgroundColor: 'transparent',
                            width: 30,
                            height: 30,
                            color: 'white',
                            tintColor: 'white',
                          }}
                        />
                      </Button>
                    </Left>
                    <Body style={{flex: 3, alignItems: 'center'}}>
                      <Title style={{color: '#fff'}}>Select Range</Title>
                    </Body>
                    <Right />
                  </Header>
                )}
                placeholder="HAZARD NAME"
                placeholderStyle={styles.pickerPlaceholer}
                placeholderIconColor="black"
                selectedValue={newGasSetup.gasScale}
                onValueChange={newScale => newGasSetup.setGasScale(newScale)}>
                {gasScaleItems}
              </Picker>
            </Item>

            {newGasSetup.isOxygen ? (
              <View>
                <View style={MainStyle.marginV10}>
                  <Stepper
                    label="Min Alert"
                    value={newGasSetup.minAlarm}
                    onValueChange={s => newGasSetup.setMinAlarm(s)}
                    handleInc={() => {
                      newGasSetup.setMinAlarm(
                        roundFloat(newGasSetup.minAlarm + 0.1),
                      );
                    }}
                    handleDec={() => {
                      newGasSetup.setMinAlarm(
                        roundFloat(newGasSetup.minAlarm - 0.1),
                      );
                    }}
                  />
                </View>
                <View style={MainStyle.marginV10}>
                  <Stepper
                    label="Alert Value"
                    value={newGasSetup.maxAlarm}
                    onValueChange={s => newGasSetup.setMaxAlarm(s)}
                    handleInc={() => {
                      newGasSetup.setMaxAlarm(
                        roundFloat(newGasSetup.maxAlarm + 0.1),
                      );
                    }}
                    handleDec={() => {
                      newGasSetup.setMaxAlarm(
                        roundFloat(newGasSetup.maxAlarm - 0.1),
                      );
                    }}
                  />
                </View>
              </View>
            ) : (
              <View style={MainStyle.marginV10}>
                <Stepper
                  label="Alert Value"
                  value={newGasSetup.maxAlarm}
                  onValueChange={s => newGasSetup.setMaxAlarm(s)}
                  handleInc={() => {
                    newGasSetup.setMaxAlarm(
                      roundFloat(newGasSetup.maxAlarm + 0.1),
                    );
                  }}
                  handleDec={() => {
                    newGasSetup.setMaxAlarm(
                      roundFloat(newGasSetup.maxAlarm - 0.1),
                    );
                  }}
                  style={MainStyle.marginV10}
                />
              </View>
            )}

            <View style={styles.createGasView}>
              <GradientButton
                onPressButton={() => {
                  if (!editMode) {
                    sessionSetup.addGasItem(newGasSetup);
                  }
                  this.props.navigation.goBack();
                  sessionSetup.createNewGas();
                }}
                setting={shadowOpt}
                // btnText={editMode ? 'EDIT' : 'SAVE'}
                btnText={'SAVE'}
              />
            </View>
          </Content>
        </Container>
      );
    }
  },
);

const styles = StyleSheet.create({
  pickerPlaceholer: {
    color: 'black',
  },
  createGasView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default inject('store')(GasAddScreen);
