import React from 'react';
import {View, Image, Alert} from 'react-native';
import {Container, Content} from 'native-base';

import {inject, observer} from 'mobx-react';
import Loader from '../Controls/Loader';

import GradientNavigationBar from '../elements/GradientNavigationBar';
import CommonStyles from '../styles/CommonStyles';
import {deviceHeight, NAV_HEIGHT, TAB_HEIGHT} from '../styles/variables';
import {StyleSheet} from 'react-native';

import MenuItemBox from '../Components/MenuItemBox';
import {delay} from '../Utils';
import cryptlexHelper from '../Services/CryptlexHelper';
import Toast from 'react-native-root-toast';
import locationHelper from '../Services/LocationHelper';

const MainScreen = observer(
  class MainScreen extends React.Component {
    static navigationOptions = {headerShown: false};

    constructor(props) {
      super(props);
      this.state = {
        isGettingLocation: false,
      };
    }

    onPurchase() {
      this.props.navigation.navigate('Purchase');
    }

    signOut() {
      const loginScreenViewModel = this.props.store.loginScreenViewModel;
      const {navigate} = this.props.navigation;

      loginScreenViewModel.signOut().then(
        () => {
          navigate('Login');
        },
        error => {
          delay(100).then(() => Alert.alert('Error', error));
        },
      );
    }

    componentDidMount() {
      const loginScreenViewModel = this.props.store.loginScreenViewModel;
      loginScreenViewModel.goOnline();

      this.setState({isGettingLocation: true});
      locationHelper
        .startLocationUpdates(
          () => {
            this.setState({isGettingLocation: false});
          },
          () => {
            this.setState({isGettingLocation: false});
          },
        )
        .then(() => {
          this.setState({isGettingLocation: false});
        });
      //Disable Activation
      loginScreenViewModel.checkActivation(isActivated => {
        if (isActivated) {
          loginScreenViewModel.startCheckActivation();
          Toast.show('License is activated.');
        }
      });
    }

    componentWillUnmount() {
      const {uid} = this.props.store.loginScreenViewModel.firebaseUser;
      cryptlexHelper.removeActivation(uid);

      locationHelper.removeLocationUpdates();
    }

    render() {
      const {navigate} = this.props.navigation;
      const loginScreenViewModel = this.props.store.loginScreenViewModel;
      const {isGettingLocation} = this.state;
      return (
        <Container>
          <GradientNavigationBar
            navigation={this.props.navigation}
            // menu
            titleImg={require('../Images/logo_white_nosubtitle.png')}
            titleImgStyle={styles.titleImage}
            rightButtons={[
              {
                key: 1,
                buttonIcon: require('../../img/purchase.png'),
                buttonAction: this.onPurchase.bind(this),
                buttonWidth: 26,
                buttonHeight: 26,
              },
              {
                key: 2,
                buttonIcon: require('../../img/healer/logout.png'),
                buttonAction: () => this.signOut(),
                buttonWidth: 22,
                buttonHeight: 24,
              },
            ]}
          />
          <Content bounces={false}>
            <Loader
              loading={loginScreenViewModel.loading || isGettingLocation}
            />
            <View style={CommonStyles.introPageImageBox}>
              <Image
                source={require('../Images/logo.png')}
                style={styles.logoImage}
              />
            </View>

            <View style={styles.fullField}>
              <View style={styles.colMainLeft}>
                <MenuItemBox
                  header="Create session"
                  subHeader="Setup a public or private session"
                  icon={require('../../img/create.png')}
                  iconWidth={26}
                  iconHeight={26}
                  onPressCard={() => navigate('HostOption')}
                />
                <MenuItemBox
                  header="Spectator Mode"
                  subHeader="Oversee a session"
                  icon={require('../../img/viewer.png')}
                  iconWidth={37}
                  iconHeight={26}
                  onPressCard={() => navigate('IncidentMenu')}
                />
              </View>

              <View style={styles.colMainRight}>
                <MenuItemBox
                  header="Join session"
                  subHeader="View and join available sessions"
                  icon={require('../../img/join.png')}
                  iconWidth={26}
                  iconHeight={26}
                  onPressCard={() => navigate('UserMain')}
                />
                <MenuItemBox
                  header="Website"
                  subHeader="Click to be taken to the website"
                  icon={require('../../img/website.png')}
                  iconWidth={26}
                  iconHeight={25}
                  onPressCard={() =>
                    navigate('WebView', {
                      uri: 'https://www.sygnalapp.com',
                      title: 'Sygnal',
                    })
                  }
                />
              </View>
            </View>
          </Content>
        </Container>
      );
    }
  },
);

const ELEMENT_HEIGHT = 430;
const spaceHeight = deviceHeight - (NAV_HEIGHT + TAB_HEIGHT + ELEMENT_HEIGHT);

const styles = StyleSheet.create({
  titleImage: {
    width: 73,
    height: 23,
  },
  logoImage: {
    width: 300,
    height: 105,
    marginVertical: 30,
  },
  fullField: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 15,
    marginTop: spaceHeight * 0.25,
  },
  colMainLeft: {
    flex: 1,
    marginRight: 8,
  },
  colMainRight: {
    flex: 1,
    marginLeft: 8,
  },
});

export default inject('store')(MainScreen);
