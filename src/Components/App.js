import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import HomeScreen from './HomeScreen';
import ProfileScreen from './ProfileScreen';
import LogoScreen from './LogoScreen';
import DisclaimerScreen from './DisclaimerScreen';
import MainScreen from './MainScreen';
import HostMainScreen from './HostMainScreen';
import HostOptionScreen from './HostOptionScreen';
import UserMainScreen from './UserMainScreen';
import IncidentMenuScreen from './IncidentMenuScreen';
import GasAddScreen from './GasAddScreen';
import HostGameScreen from './HostGameScreen';
import UserManagerScreen from './UserManagerScreen';
import HostMapScreen from './HostMapScreen';
import LoginScreen from './LoginScreen';
import LicenseScreen from './LicenseScreen';
import PurchaseScreen from './PurchaseScreen';
import WebViewScreen from './WebViewScreen';
import PaymentScreen from './PaymentScreen';
import {PermissionsAndroid, ToastAndroid, Platform} from 'react-native';
import Orientation from 'react-native-orientation';

const MainNavigator = createStackNavigator(
  {
    Home: {screen: HomeScreen},
    Profile: {screen: ProfileScreen},
    Logo: {screen: LogoScreen},
    Disclaimer: {screen: DisclaimerScreen},
    Main: {screen: MainScreen},
    HostMain: {screen: HostMainScreen},
    HostOption: {screen: HostOptionScreen},
    UserMain: {screen: UserMainScreen},
    IncidentMenu: {screen: IncidentMenuScreen},
    GasAdd: {screen: GasAddScreen},
    HostGame: {screen: HostGameScreen},
    UserManager: {screen: UserManagerScreen},
    HostMap: {screen: HostMapScreen},
    Login: {screen: LoginScreen},
    License: {screen: LicenseScreen},
    Purchase: {screen: PurchaseScreen},
    WebView: {screen: WebViewScreen},
    Payment: {screen: PaymentScreen},
  },
  {
    initialRouteName: 'Logo',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#3F51B5',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);

const AppContainer = createAppContainer(MainNavigator);

export default class App extends React.Component {
  state = {
    orientation: 'PORTRAIT',
  };

  componentDidMount() {
    Orientation.addOrientationListener(this._onOrientationChange);
    this._requestPermissions();
  }

  componentWillUnmount() {
    Orientation.removeOrientationListener(this._onOrientationChange);
  }

  _requestPermissions() {
    //Request Permissions on App Load.
    if (Platform.OS === 'android') {
      PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      ]).then(result => {
        if (
          result['android.permission.READ_PHONE_STATE'] &&
          result['android.permission.READ_EXTERNAL_STORAGE'] &&
          result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted'
        ) {
          // ToastAndroid.show('Permissions Granted!', 300); // Keep this for DEV test
        } else if (
          result['android.permission.READ_PHONE_STATE'] ||
          result['android.permission.READ_EXTERNAL_STORAGE'] ||
          result['android.permission.WRITE_EXTERNAL_STORAGE'] ===
            'never_ask_again'
        ) {
          ToastAndroid.show(
            'Please Go into Settings -> Applications -> Sygnal Native -> Permissions and Allow permissions',
            300,
          );
        }
      });
    }
  }

  _onOrientationChange = (error,result) => {
    // Update state on Orientation Change to reset UI
    console.log(result, error);
    this.setState({
      orientation: result,
    });
  };

  render() {
    return <AppContainer />;
  }
}
