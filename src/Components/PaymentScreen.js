import React, {Component} from 'react';
import {View, Alert} from 'react-native';
import stripe from 'tipsi-stripe';
import {doPayment} from '../Services/StripeApi';
import {Container, Content} from 'native-base';
import GradientNavigationBar from '../elements/GradientNavigationBar';
import Loader from '../Controls/Loader';

import {ProductCard} from '../elements/ProductCard';

import firebaseHelper from '../Services/FirebaseHelper';

const CryptlexLambdaUrl =
  'https://3j2t766xtf.execute-api.us-east-1.amazonaws.com/prod/license';

stripe.setOptions({
  publishableKey:
    'pk_live_51HFe74GCrSYyZMBH5Uf5cV4nY2NgSh3RIs55clZc7O8UejsbTXNW8Z8Z77fEweI7BnvILeZpyH1SB1rp2m3Qk1bE00m3FUGcyX',
});

export default class Payment extends Component {
  static navigationOptions = {headerShown: false};
  state = {isPaymentPending: false, isLoading: false};

  // This chunk gets called after payment is successful on stripe
  async onSuccessHandler(uid, email, stripeSubscriptionId) {
    const createdAt = Date.now();
    const expiresAt = createdAt + 31556952000; // 31556952000  is one year in milliseconds.
    const licenseId = `${createdAt}${uid}`;

    // Creates License in cryptlex
    var licenseKey = await fetch(CryptlexLambdaUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        // Such a sad thing we dont have the user's name stored. Perhaps , we should collect it at sign-in.
        first_name: uid,
        last_name: uid,
        order_id: stripeSubscriptionId,
      }),
    });

    licenseKey = await licenseKey.json();

    // Stores the license in firebase
    firebaseHelper.addNewLicense(uid, {
      id: licenseId,
      key: licenseKey,
      expiresAt: expiresAt,
      createdAt: Date.now(),
      userId: uid,
    });
  }

  requestPayment = async () => {
    const {email, userId} = this.props.navigation.state.params;
    const {navigation} = this.props;

    this.setState({isPaymentPending: true});

    return (
      stripe
        .paymentRequestWithCardForm()
        .then(stripeTokenInfo => {
          this.setState({isLoading: true});
          return doPayment(email, stripeTokenInfo.tokenId);
        })
        // success
        .then(data => {
          this.onSuccessHandler(userId, email, data);
          Alert.alert(
            'Payment Successful',
            null,
            [{text: 'OK', onPress: () => navigation.goBack(null)}],
            {cancelable: false},
          );
          console.log('Subscription succeeded!', data);
        })
        //failed / cancelled by user
        .catch(error => {
          if (error.code !== 'cancelled') {
            Alert.alert(
              'Oops! Something Went Wrong! Please check you connection and try again',
              null,
              [{text: 'OK', onPress: () => navigation.goBack(null)}],
              {cancelable: false},
            );
          }
          console.log('Payment failed', {error});
        })
        .finally(() => {
          this.setState({isPaymentPending: false});
        })
    );
  };

  render() {
    const {email, userId} = this.props.navigation.state.params;
    const {isLoading} = this.state;

    if (!email || !userId) {
      return null;
    }

    return (
      <Container>
        <GradientNavigationBar
          navigation={this.props.navigation}
          back
          titleText="Payment"
        />
        <Content contentContainerStyle={styles.content} bounces={false}>
          <Loader loading={isLoading} />
          <View style={styles.container}>
            <ProductCard
              title={'Sygnal License'}
              subTitle={
                'Hosted floating license for up to 5 simultaneous users'
              }
              price={'US $999.99'}
              image={require('../Images/logo.png')}
              buttonText={'Buy now'}
              buttonColor={'#ff2788'}
              onClickButton={this.requestPayment}
            />
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop: 100,
  },
  content: {flexGrow: 1, marginTop: 15},
};
