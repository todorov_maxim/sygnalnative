import React from 'react';
import {
  View, 
  ImageBackground, 
  Alert,
  StyleSheet,
  Platform
} from 'react-native';
import {
  Container,
  Content,
  Text,
  Button,
  Item,
  Label,
  Input,
  Picker,
  Icon,
} from 'native-base';

import {inject, observer} from 'mobx-react';
import MainStyle from '../StyleSheets/MainStyle';
import Loader from '../Controls/Loader';

import {delay} from '../Utils';

const LicenseScreen = observer(
  class LicenseScreen extends React.Component {
    static navigationOptions = {
      title: 'MANAGE LICENSE',
    };
    didFocusSubscription = this.props.navigation.addListener(
      'didFocus',
      () => {
        const licenseViewModel = this.props.store.licenseViewModel;
        licenseViewModel.getAllLicenseSkus();
      },
    );

    didBlurSubscription = this.props.navigation.addListener(
      'didBlur',
      () => {
        const licenseViewModel = this.props.store.licenseViewModel;
      },
    );

    componentWillUnmount() {
      this.didBlurSubscription.remove();
      this.didFocusSubscription.remove();
    }

    purchase = () => {
      const licenseViewModel = this.props.store.licenseViewModel;
      licenseViewModel.purchase().then(
        () => {},
        error => {
          delay(100).then(() => Alert.alert('Error', error));
        },
      );
    };

    activateLicense = () => {};

    renewLicense = () => {};

    render() {
      const licenseViewModel = this.props.store.licenseViewModel;
      const availabLicenseSkuItems = licenseViewModel.availableLicenseSku.map(
        (s, i) => {
          return <Picker.Item key={i} value={i} label={s.shortDescription} />;
        },
      );

      const availableActionItems = licenseViewModel.availableAction.map(
        (s, i) => {
          return <Picker.Item key={i} value={i} label={s} />;
        },
      );

      return (
        <Container>
          <ImageBackground
            source={require('../Images/background.png')}
            style={styles.imageBackgound}>
            <Loader loading={licenseViewModel.loading} />
            <Content padder>
              <Item picker style={MainStyle.marginV10}>
                <Picker
                  mode="dialog"
                  iosIcon={<Icon name="arrow-down" />}
                  placeholder="Select Action"
                  placeholderStyle={styles.placeholder}
                  placeholderIconColor="black"
                  selectedValue={licenseViewModel.selectedAction}
                  onValueChange={newSelectedSession =>
                    licenseViewModel.setSelectedAction(newSelectedSession)
                  }>
                  {Platform.OS === 'android' && (
                    <Picker.Item label="Select Action" value={null} />
                  )}
                  {availableActionItems}
                </Picker>
              </Item>

              {licenseViewModel.selectedAction === 0 && (
                <View>
                  {licenseViewModel.availableLicenseSku.length > 0 ? (
                    <Item picker style={MainStyle.marginV10}>
                      <Picker
                        mode="dialog"
                        iosIcon={<Icon name="arrow-down" />}
                        placeholder="Available License Sku"
                        placeholderStyle={styles.placeholder}
                        placeholderIconColor="black"
                        selectedValue={licenseViewModel.selectedLicenseSku}
                        onValueChange={newSelectedSession =>
                          licenseViewModel.setSelectedLicenseSku(
                            newSelectedSession,
                          )
                        }>
                        {Platform.OS === 'android' && (
                          <Picker.Item
                            label="Available License Sku"
                            value={null}
                          />
                        )}
                        {availabLicenseSkuItems}
                      </Picker>
                    </Item>
                  ) : (
                    <Item style={MainStyle.marginV10}>
                      <Label
                        style={styles.loadingLabel}>
                        Loading Available License Sku
                      </Label>
                    </Item>
                  )}

                  <Item floatingLabel style={MainStyle.marginV10}>
                    <Label style={MainStyle.labelColor}>First Name</Label>
                    <Input
                      value={licenseViewModel.firstName}
                      onChangeText={licenseViewModel.setFirstName}
                    />
                  </Item>
                  <Item floatingLabel style={MainStyle.marginV10}>
                    <Label style={MainStyle.labelColor}>Last Name</Label>
                    <Input
                      value={licenseViewModel.lastName}
                      onChangeText={licenseViewModel.setLastName}
                    />
                  </Item>

                  <Label style={[MainStyle.labelColor, MainStyle.marginV10]}>
                    {licenseViewModel.skuDescription}
                  </Label>

                  <Label style={[MainStyle.labelColor, MainStyle.marginV10]}>
                    {`${licenseViewModel.skuPrice} ${licenseViewModel.skuCurrency}`}
                  </Label>
                  <Button
                    full
                    rounded
                    success
                    onPress={() => this.purchase()}
                    style={MainStyle.marginV10H32}>
                    <Text>PURCHASE LICENSE</Text>
                  </Button>
                </View>
              )}

              {licenseViewModel.selectedAction === 1 && (
                <View>
                  <Item floatingLabel style={MainStyle.marginV10}>
                    <Label style={MainStyle.labelColor}>License Key</Label>
                    <Input
                      value={licenseViewModel.activateLicenseKey}
                      onChangeText={licenseViewModel.setActivateLicenseKey}
                    />
                  </Item>

                  <Button
                    full
                    rounded
                    success
                    onPress={() => this.activateLicense()}
                    style={MainStyle.marginV10H32}>
                    <Text>ACTIVATE LICENSE</Text>
                  </Button>
                </View>
              )}

              {licenseViewModel.selectedAction === 2 && (
                <View>
                  <Item floatingLabel style={MainStyle.marginV10}>
                    <Label style={MainStyle.labelColor}>
                      Renew License Key
                    </Label>
                    <Input
                      value={licenseViewModel.renewLicenseKey}
                      onChangeText={licenseViewModel.setRenewLicenseKey}
                    />
                  </Item>

                  <Button
                    full
                    rounded
                    success
                    onPress={() => this.renewLicense()}
                    style={MainStyle.marginV10H32}>
                    <Text>RENEW LICENSE</Text>
                  </Button>
                </View>
              )}
            </Content>
          </ImageBackground>
        </Container>
      );
    }
  },
);

const styles = StyleSheet.create({
  imageBackgound: { 
    width: '100%', 
    height: '100%' 
  },
  placeholder: {
    color: 'black' 
  },
  loadingLabel: {
    flex: 1,
    ...MainStyle.marginV10,
    ...MainStyle.labelColor,
  }
});

export default inject('store')(LicenseScreen);