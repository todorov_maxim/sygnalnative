import React from 'react';
import {ImageBackground, Image, StyleSheet} from 'react-native';
import {Container, Text, Button} from 'native-base';

import {inject, observer} from 'mobx-react';
import MainStyle from '../StyleSheets/MainStyle';

const HostMainScreen = observer(
  class HostMainScreen extends React.Component {
    static navigationOptions = {
      title: 'CREATE SESSION',
    };

    render() {
      const {navigate} = this.props.navigation;
      return (
        <Container>
          <ImageBackground
            source={require('../Images/background.png')}
            style={styles.imageBackground}>
            <Image
              source={require('../Images/logo.png')}
              style={styles.image}
            />
            <Button
              full
              rounded
              onPress={() => navigate('UserMain')}
              style={MainStyle.buttonItems}>
              <Text>START SESSION</Text>
            </Button>
            <Button
              full
              rounded
              onPress={() => navigate('HostOption')}
              style={MainStyle.buttonItems}>
              <Text>SETUP SESSION</Text>
            </Button>
            <Button
              full
              danger
              rounded
              onPress={() => this.props.store.openWebsite()}
              style={MainStyle.buttonItems}>
              <Text>WEBSITE</Text>
            </Button>
          </ImageBackground>
        </Container>
      );
    }
  },
);

const styles=StyleSheet.create({
  imageBackground: { 
    width: '100%', 
    height: '100%' 
  },
  image: {
    alignSelf: 'center', 
    marginVertical: 40 
  }
});

export default inject('store')(HostMainScreen);
