package com.koktech.sygnal;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.ReactActivity;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.Map;


public class ParrotDroneManager extends ViewGroupManager<ParrotDroneView> {

    public static final String REACT_CLASS = "ParrotDroneView";



    ReactApplicationContext mCallerContext;
    ReactActivity mActivity;

    public ParrotDroneManager(ReactApplicationContext reactContext) {
        mCallerContext = reactContext;
        mActivity = getActivity();

    }

    public ReactActivity getActivity() {

        return (ReactActivity) mCallerContext.getCurrentActivity();
    }

    @ReactProp(name = "manualPiloting")
    public void pilotDrone(ParrotDroneView view, String takeOffLand) {

        view.smartTakeOffLand(takeOffLand);
    }

    @ReactProp(name = "returnHome")
    public void pilotDrone(ParrotDroneView view, boolean value) {

        view.returnHome(value);
    }


    @Override
    @Nullable
    public Map getExportedCustomDirectEventTypeConstants() {
        MapBuilder.Builder builder = MapBuilder.builder();
        for (ParrotDroneView.Events event : ParrotDroneView.Events.values()) {
            builder.put(event.toString(), MapBuilder.of("registrationName", event.toString()));
        }
        return builder.build();
    }

    @NonNull
    @Override
    protected ParrotDroneView createViewInstance(@NonNull ThemedReactContext reactContext) {

        return new ParrotDroneView(reactContext,mActivity);
    }


    @Override
    public String getName() {
        return REACT_CLASS;
    }



}

