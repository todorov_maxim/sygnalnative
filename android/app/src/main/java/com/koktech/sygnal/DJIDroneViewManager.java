package com.koktech.sygnal;

import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.ReactActivity;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;

import java.util.Map;


public class DJIDroneViewManager extends ViewGroupManager<DJIDroneView> {

    public static final String REACT_CLASS = "DJIDroneView";



    ReactApplicationContext mCallerContext;
    ReactActivity mActivity;

      @NonNull
    @Override
    protected DJIDroneView createViewInstance(@NonNull ThemedReactContext reactContext) {

        return new DJIDroneView(reactContext,mActivity);
    }

    public DJIDroneViewManager(ReactApplicationContext reactContext) {
        mCallerContext = reactContext;
        mActivity = getActivity();

    }

    @Override
    public void addView(DJIDroneView parent, View child, int index) {
        //super.addView(parent, child, index);
       LinearLayout cameraView = parent.findViewById(R.id.camera);
       cameraView.addView(child,index);


    }

    public ReactActivity getActivity() {

        return (ReactActivity) mCallerContext.getCurrentActivity();
    }

    @Override
    public void onDropViewInstance(@NonNull DJIDroneView view) {
        super.onDropViewInstance(view);
        if (view != null) {
            view.destroy();
        }

    }

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    @Nullable
    public Map getExportedCustomDirectEventTypeConstants() {
        MapBuilder.Builder builder = MapBuilder.builder();
        for (DJIDroneView.Events event : DJIDroneView.Events.values()) {
            builder.put(event.toString(), MapBuilder.of("registrationName", event.toString()));
        }
        return builder.build();
    }

}
