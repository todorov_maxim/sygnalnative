package com.koktech.sygnal;



import androidx.annotation.Nullable;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.parrot.drone.groundsdk.GroundSdk;

import com.parrot.drone.groundsdk.device.Drone;
import com.parrot.drone.groundsdk.device.instrument.Altimeter;
import com.parrot.drone.groundsdk.device.instrument.BatteryInfo;
import com.parrot.drone.groundsdk.device.instrument.FlightMeter;
import com.parrot.drone.groundsdk.device.instrument.FlyingIndicators;
import com.parrot.drone.groundsdk.device.instrument.Gps;
import com.parrot.drone.groundsdk.device.instrument.Radio;
import com.parrot.drone.groundsdk.device.instrument.Speedometer;
import com.parrot.drone.groundsdk.facility.AutoConnection;


public class ParrotModule extends ReactContextBaseJavaModule {
    private static final String TAG = ParrotModule.class.getSimpleName();
    private ReactContext rc;

    public ParrotModule(ReactApplicationContext reactContext) {
        super(reactContext);

        this.rc = reactContext;

    }

    private GroundSdk groundSdk;
    private Drone drone = null;

    @Override
    public String getName() {
        return "Parrot";
    }


    @ReactMethod
    public void getDrone(Promise promise) {
        drone = null;
        groundSdk = MainActivity.getGroundSdk();
        groundSdk.getFacility(AutoConnection.class).start();

        drone = groundSdk.getFacility(AutoConnection.class).getDrone();

        if (drone == null) {
            promise.reject("Error","Parrot drone not found");
            return;
        }
        WritableMap event = Arguments.createMap();
        event.putString("uid", drone.getUid());
        event.putString("name", drone.getName());
        event.putString("model", drone.getModel().name());
        event.putString("state", drone.getState().getConnectionState().name());

        promise.resolve(event);

    }

    private void sendEvent(String eventName, @Nullable WritableMap params) {
        if (rc != null) {
            if (rc.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class) != null) {
                rc.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, params);
            }
        }

    }


    @ReactMethod
    public void getRadioInfo(Promise promise) {

        WritableMap event = Arguments.createMap();
        if (groundSdk != null && drone != null) {

            // RADIO INFO
            try {

                promise.resolve(getRadio());
            } catch (Exception e) {

                promise.reject("Error", e.getMessage());
            }
        } else {

            promise.reject("Error", "no Data");
        }

    }


    @ReactMethod
    public void getFlightInfo(Promise promise) {

        WritableMap event = Arguments.createMap();
        if (groundSdk != null && drone != null) {

            // SPEED & ALTITUDE
            try {
                promise.resolve(getFlight());
            } catch (Exception e) {

                promise.reject("Error", e.getMessage());
            }
        } else {

            promise.reject("Error", "no Data");
        }

    }


    @ReactMethod
    public void getBatteryInfo(Promise promise) {

        if (groundSdk != null && drone != null) {

            // SPEED & ALTITUDE
            try {

                promise.resolve(getBattery());

            } catch (Exception e) {

                promise.reject("Error", e.getMessage());
            }
        } else {

            promise.reject("Error", "no Data");
        }

    }

    @ReactMethod
    public void getAltitudeInfo(Promise promise) {


        if (groundSdk != null && drone != null) {

            // SPEED & ALTITUDE
            try {

                promise.resolve(getAltitude());
            } catch (Exception e) {

                promise.reject("Error", e.getMessage());
            }
        } else {

            promise.reject("Error", "no Data");
        }


    }


    @ReactMethod
    public void getSpeedInfo(Promise promise) {

        if (groundSdk != null && drone != null) {

            // SPEED & ALTITUDE
            try {

                promise.resolve(getSpeed());
            } catch (Exception e) {

                promise.reject("Error", e.getMessage());
            }
        } else {

            promise.reject("Error", "no Data");
        }
    }

    @ReactMethod
    public void getLocationInfo(Promise promise) {
        if (groundSdk != null && drone != null) {

            // SPEED & ALTITUDE
            try {

                promise.resolve(getLocation());
            } catch (Exception e) {

                promise.reject("Error", e.getMessage());
            }
        } else {

            promise.reject("Error", "no Data");
        }

    }

    public WritableMap getRadio() {
        if (groundSdk == null && drone == null) return null;

        WritableMap radioInfo = Arguments.createMap();
        radioInfo.putInt("signalQuality", drone.getInstrument(Radio.class).getLinkSignalQuality());
        radioInfo.putInt("rssi", drone.getInstrument(Radio.class).getRssi());

        return radioInfo;

    }

    public WritableMap getFlight() {
        if (groundSdk == null && drone == null) return null;

        WritableMap flightInfo = Arguments.createMap();
        flightInfo.putInt("lastFlightDuration", drone.getInstrument(FlightMeter.class).getLastFlightDuration());
        flightInfo.putInt("totalFlightCount", drone.getInstrument(FlightMeter.class).getTotalFlightCount());
        flightInfo.putString("STATE", drone.getInstrument(FlyingIndicators.class).getState().name());

        return flightInfo;

    }

    public WritableMap getAltitude() {
        if (groundSdk == null && drone == null) return null;
        try {


            WritableMap altitude = Arguments.createMap();
            altitude.putDouble("verticalSpeed", drone.getInstrument(Altimeter.class).getVerticalSpeed());
            altitude.putDouble("absoluteAltitude", drone.getInstrument(Altimeter.class).getAbsoluteAltitude().getValue());
            altitude.putDouble("relativeAltitude", drone.getInstrument(Altimeter.class).getGroundRelativeAltitude().getValue());
            altitude.putDouble("relativeAltitude", drone.getInstrument(Altimeter.class).getGroundRelativeAltitude().getValue());
            altitude.putDouble("relativeTakeOffAltitude", drone.getInstrument(Altimeter.class).getTakeOffRelativeAltitude());

            return altitude;
        } catch (Exception e) {
            return null;
        }

    }

    public WritableMap getBattery() {
        if (groundSdk == null && drone == null) return null;
        WritableMap battery = Arguments.createMap();
        battery.putDouble("batteryLevel", drone.getInstrument(BatteryInfo.class).getBatteryLevel());
        battery.putInt("batteryHealth", drone.getInstrument(BatteryInfo.class).getBatteryHealth().getValue());
        return battery;

    }


    public WritableMap getSpeed() {
        if (groundSdk == null && drone == null) return null;
        WritableMap speed = Arguments.createMap();
        speed.putDouble("airSpeed", drone.getInstrument(Speedometer.class).getAirSpeed().getValue());
        speed.putDouble("downSpeed", drone.getInstrument(Speedometer.class).getDownSpeed());
        speed.putDouble("eastSpeed", drone.getInstrument(Speedometer.class).getEastSpeed());
        speed.putDouble("forwardSpeed", drone.getInstrument(Speedometer.class).getForwardSpeed());
        speed.putDouble("groundSpeed", drone.getInstrument(Speedometer.class).getGroundSpeed());
        speed.putDouble("northSpeed", drone.getInstrument(Speedometer.class).getNorthSpeed());
        speed.putDouble("rightSpeed", drone.getInstrument(Speedometer.class).getRightSpeed());
        speed.putDouble("speed", drone.getInstrument(Gps.class).lastKnownLocation().getSpeed());
        return speed;

    }


    public WritableMap getLocation() {
        if (groundSdk == null && drone == null) return null;
        WritableMap location = Arguments.createMap();
        location.putDouble("latitude", drone.getInstrument(Gps.class).lastKnownLocation().getLatitude());
        location.putDouble("longitude", drone.getInstrument(Gps.class).lastKnownLocation().getLongitude());

        return location;

    }

}
