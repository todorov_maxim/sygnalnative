package com.koktech.sygnal;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.facebook.react.ReactActivity;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.PermissionListener;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.events.RCTEventEmitter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import dji.common.error.DJIError;
import dji.common.error.DJISDKError;
import dji.common.flightcontroller.FlightControllerState;
import dji.common.realname.AircraftBindingState;
import dji.common.realname.AppActivationState;
import dji.common.useraccount.UserAccountState;
import dji.common.util.CommonCallbacks;
import dji.sdk.base.BaseComponent;
import dji.sdk.base.BaseProduct;
import dji.sdk.flightcontroller.FlightController;
import dji.sdk.products.Aircraft;
import dji.sdk.sdkmanager.DJISDKInitEvent;
import dji.sdk.sdkmanager.DJISDKManager;
import dji.sdk.useraccount.UserAccountManager;
import dji.ux.widget.FPVWidget;

public class DJIDroneView extends LinearLayout implements LifecycleEventListener {

    public static final String FLAG_CONNECTION_CHANGE = "uxsdk_demo_connection_change";
    private static final int REQUEST_PERMISSION_CODE = 12345;
    private static final String[] REQUIRED_PERMISSION_LIST = new String[]{
            Manifest.permission.VIBRATE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.WAKE_LOCK,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CHANGE_WIFI_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
    };
    private static BaseProduct mProduct;
    private final Runnable measureAndLayout = new Runnable() {
        @Override
        public void run() {
            measure(
                    MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.EXACTLY));
            layout(getLeft(), getTop(), getRight(), getBottom());
        }
    };
    public RCTEventEmitter mEventEmitter;
    FlightControllerState.Callback mFlightControllerState = new FlightControllerState.Callback() {
        @Override
        public void onUpdate(FlightControllerState
                                     djiFlightControllerCurrentState) {
            double droneLocationLat = djiFlightControllerCurrentState.getAircraftLocation().getLatitude();
            double droneLocationLng = djiFlightControllerCurrentState.getAircraftLocation().getLongitude();
            double droneLocationAltitude = djiFlightControllerCurrentState.getAircraftLocation().getAltitude();
            try {

                WritableMap params = Arguments.createMap();
                params.putBoolean("success", true);
                params.putDouble("latitude", droneLocationLat);
                params.putDouble("longitude", droneLocationLng);
                params.putDouble("absoluteAltitude", droneLocationAltitude);
                params.putDouble("forwardSpeed", djiFlightControllerCurrentState.getVelocityZ());
                params.putDouble("rightSpeed", djiFlightControllerCurrentState.getVelocityX());
                params.putDouble("downSpeed", djiFlightControllerCurrentState.getVelocityY());
                params.putString("model", mProduct.getModel().name());
                sendEvent(Events.EVENT_TELEMETRY_UPDATED.toString(), params);

            } catch (Exception e) {

            }

        }
    };
    private ThemedReactContext mContext;
    private RelativeLayout droneView;
    private View rootView;
    private Handler mHandler;
    private FlightController mFlightController = null;
    private String mAircraftBindingState = "Unknown";
    private String mAppActivationState = "Unknown";
    AircraftBindingState.AircraftBindingStateListener mAircraftBindingStateListener = new AircraftBindingState.AircraftBindingStateListener() {
        @Override
        public void onUpdate(AircraftBindingState aircraftBindingState) {

            try {

                mAircraftBindingState = aircraftBindingStateToString(aircraftBindingState);
                WritableMap params = Arguments.createMap();
                params.putBoolean("success", true);
                params.putString("msg", "");
                params.putString("activationState", mAppActivationState);
                params.putString("state", mAircraftBindingState);
                if (mProduct != null && mProduct.getModel() != null && mProduct.getModel().getDisplayName() != null) {
                    params.putString("model", mProduct.getModel().getDisplayName());
                } else {
                    params.putString("model", "");
                }
                sendEvent(Events.EVENT_STATUS_CHANGED.toString(), params);
            } catch (Exception e) {

                WritableMap params = Arguments.createMap();
                params.putBoolean("success", true);
                params.putString("msg", e.getMessage());

                sendEvent(Events.EVENT_STATUS_CHANGED.toString(), params);

            }
        }
    };
    AppActivationState.AppActivationStateListener mAppActivationStateListener = new AppActivationState.AppActivationStateListener() {
        @Override
        public void onUpdate(AppActivationState appActivationState) {
            try {

                mAppActivationState = appActivationStateToString(appActivationState);

                WritableMap params = Arguments.createMap();
                params.putBoolean("success", true);
                params.putString("msg", "");
                params.putString("activationState", mAppActivationState);
                params.putString("state", mAircraftBindingState);
                if (mProduct != null && mProduct.getModel() != null && mProduct.getModel().getDisplayName() != null) {
                    params.putString("model", mProduct.getModel().getDisplayName());
                } else {
                    params.putString("model", "");
                }
                sendEvent(Events.EVENT_STATUS_CHANGED.toString(), params);

            } catch (Exception e) {

                WritableMap params = Arguments.createMap();
                params.putBoolean("success", true);
                params.putString("msg", e.getMessage());

                sendEvent(Events.EVENT_STATUS_CHANGED.toString(), params);

            }
        }
    };
    DJISDKManager.SDKManagerCallback mDJISDKManagerCallback = new DJISDKManager.SDKManagerCallback() {
        @Override
        public void onRegister(DJIError error) {
            if (error == DJISDKError.REGISTRATION_SUCCESS) {

                try {
                    DJISDKManager.getInstance().startConnectionToProduct();
                    loginAccount();

                    if (DJISDKManager.getInstance().getAppActivationManager() != null)
                        mAppActivationState = appActivationStateToString(DJISDKManager.getInstance().getAppActivationManager().getAppActivationState());

                    Toast.makeText(mContext.getApplicationContext(), "Register Success", Toast.LENGTH_LONG).show();

                    WritableMap params = Arguments.createMap();
                    params.putBoolean("success", true);
                    params.putString("msg", "Register Success");
                    params.putString("activationState", mAppActivationState);
                    params.putString("state", mAircraftBindingState);
                    if (mProduct != null && mProduct.getModel() != null && mProduct.getModel().getDisplayName() != null) {
                        params.putString("model", mProduct.getModel().getDisplayName());
                    } else {
                        params.putString("model", "");
                    }

                    sendEvent(Events.EVENT_STATUS_CHANGED.toString(), params);

                    if (rootView != null) {
                        FPVWidget contentView = (FPVWidget) rootView.findViewById(R.id.fpv_widget);
                        if (contentView != null) {
                            contentView.setSourceCameraNameVisibility(false);
                        }

                    }


                    if (DJISDKManager.getInstance().getAppActivationManager() != null) {
                        DJISDKManager.getInstance().getAppActivationManager().addAircraftBindingStateListener(mAircraftBindingStateListener);
                        DJISDKManager.getInstance().getAppActivationManager().addAppActivationStateListener(mAppActivationStateListener);
                    }
                } catch (Exception e) {

                    WritableMap params = Arguments.createMap();
                    params.putBoolean("success", true);
                    params.putString("msg", e.getMessage());

                    sendEvent(Events.EVENT_STATUS_CHANGED.toString(), params);

                }

            } else {
                try {


                    Toast.makeText(mContext.getApplicationContext(), "Register Failed, check network is available", Toast.LENGTH_LONG).show();
                    WritableMap params = Arguments.createMap();
                    params.putBoolean("success", false);
                    params.putString("msg", "Register sdk fails, please check the bundle id and network connection!");
                    params.putString("activationState", mAppActivationState);
                    params.putString("state", mAircraftBindingState);
                    if (mProduct != null && mProduct.getModel() != null && mProduct.getModel().getDisplayName() != null) {
                        params.putString("model", mProduct.getModel().getDisplayName());
                    } else {
                        params.putString("model", "");
                    }
                    sendEvent(Events.EVENT_STATUS_CHANGED.toString(), params);
                } catch (Exception e) {

                    WritableMap params = Arguments.createMap();
                    params.putBoolean("success", true);
                    params.putString("msg", e.getMessage());

                    sendEvent(Events.EVENT_STATUS_CHANGED.toString(), params);

                }
            }
            Log.e("TAG", error.toString());
        }

        @Override
        public void onProductDisconnect() {

            Log.d("TAG", "onProductDisconnect");
            //notifyStatusChange();
            mProduct = null;
            if (mFlightController != null) {
                mFlightController.setStateCallback(null);
                mFlightController = null;
            }

        }

        @Override
        public void onProductConnect(BaseProduct baseProduct) {
            // Log.d("TAG", String.format("onProductConnect newProduct:%s", baseProduct));
            try {


                mProduct = baseProduct;
                initFlightController();
                if (rootView != null) {
                    FPVWidget contentView = (FPVWidget) rootView.findViewById(R.id.fpv_widget);
                    if (contentView != null) {
                        contentView.setSourceCameraNameVisibility(false);
                    }

                }

                //notifyStatusChange();
            } catch (Exception e) {
                WritableMap params = Arguments.createMap();
                params.putBoolean("success", true);

                params.putString("msg", e.getMessage());

                sendEvent(Events.EVENT_STATUS_CHANGED.toString(), params);
            }
        }

        @Override
        public void onProductChanged(BaseProduct baseProduct) {

            Log.d("TAG", String.format("onProductConnect newProduct:%s", baseProduct));
            try {
                mProduct = baseProduct;
                initFlightController();
                //notifyStatusChange();
            } catch (Exception e) {

            }


        }

        @Override
        public void onComponentChange(BaseProduct.ComponentKey componentKey, BaseComponent oldComponent, BaseComponent newComponent) {
            if (newComponent != null) {
                newComponent.setComponentListener(isConnected -> {

                });
            }


        }

        @Override
        public void onInitProcess(DJISDKInitEvent djisdkInitEvent, int i) {

        }

        @Override
        public void onDatabaseDownloadProgress(long l, long l1) {
            WritableMap event = Arguments.createMap();
            event.putBoolean("success", true);
            event.putDouble("completedUnitCount", l);
            event.putDouble("totalUnitCount", l1);
            sendEvent(Events.EVENT_DOWNLOAD_PROGRESS.toString(), event);
        }
    };

    private ReactActivity mActivity;
    private AtomicBoolean isRegistrationInProgress = new AtomicBoolean(false);
    private List<String> missingPermission = new ArrayList<>();
    PermissionListener mPermissionListener = new PermissionListener() {

        @Override
        public boolean onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

            if (requestCode == REQUEST_PERMISSION_CODE) {
                for (int i = grantResults.length - 1; i >= 0; i--) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        missingPermission.remove(permissions[i]);
                    }
                }
                // If there is enough permission, we will start the registration
                if (missingPermission.isEmpty()) {
                    loadSdk();
                } else {

                    WritableMap params = Arguments.createMap();
                    params.putBoolean("success", false);
                    params.putString("msg", "Missing permissions!!!");
                    params.putString("activationState", mAppActivationState);
                    params.putString("state", mAircraftBindingState);
                    if (mProduct != null && mProduct.getModel() != null && mProduct.getModel().getDisplayName() != null) {
                        params.putString("model", mProduct.getModel().getDisplayName());
                    } else {
                        params.putString("model", "");
                    }
                    sendEvent(Events.EVENT_STATUS_CHANGED.toString(), params);

                }
            }
            return false;
        }
    };


    public DJIDroneView(ThemedReactContext context, ReactActivity activity) {
        super(context);
        mContext = context;
        mEventEmitter = mContext.getJSModule(RCTEventEmitter.class);
        mContext.addLifecycleEventListener(this);
        mActivity = activity;
        createView(mContext);
        //checkAndRequestPermissions();
        loadSdk();
    }

    public void loadSdk() {
        mHandler = new Handler(Looper.getMainLooper());

        //Check the permissions before registering the application for android system 6.0 above.
        int permissionCheck = ContextCompat.checkSelfPermission(mContext.getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionCheck2 = ContextCompat.checkSelfPermission(mContext.getApplicationContext(), android.Manifest.permission.READ_PHONE_STATE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || (permissionCheck == 0 && permissionCheck2 == 0)) {

            //This is used to start SDK services and initiate SDK.
            DJISDKManager.getInstance().registerApp(mContext.getApplicationContext(), mDJISDKManagerCallback);

        } else {
            Toast.makeText(mContext.getApplicationContext(), "Please check if the permission is granted.", Toast.LENGTH_LONG).show();
        }

    }

    private void checkAndRequestPermissions() {
        // Check for permissions
        for (String eachPermission : REQUIRED_PERMISSION_LIST) {
            if (ContextCompat.checkSelfPermission(mContext, eachPermission) != PackageManager.PERMISSION_GRANTED) {
                missingPermission.add(eachPermission);
            }
        }
        // Request for missing permissions
        if (missingPermission.isEmpty()) {
            loadSdk();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            mActivity.requestPermissions(missingPermission.toArray(new String[missingPermission.size()]),
                    REQUEST_PERMISSION_CODE, mPermissionListener);
        }

    }

    private void initFlightController() {
        try {


            if (isFlightControllerSupported()) {
                if (DJISDKManager.getInstance().getProduct() == null) return;

                mFlightController = ((Aircraft) DJISDKManager.getInstance().getProduct()).getFlightController();
                mFlightController.setStateCallback(mFlightControllerState);

            }
        } catch (Exception e) {

            WritableMap params = Arguments.createMap();
            params.putBoolean("success", true);
            params.putString("msg", e.getMessage());

            sendEvent(Events.EVENT_STATUS_CHANGED.toString(), params);

        }
    }

    public void destroy() {
        try {
            if (mFlightController != null) {
                mFlightController.setStateCallback(null);
                mFlightController = null;
            }

            if (DJISDKManager.getInstance().getAppActivationManager() != null) {
                DJISDKManager.getInstance().getAppActivationManager().removeAircraftBindingStateListener(mAircraftBindingStateListener);
                DJISDKManager.getInstance().getAppActivationManager().removeAppActivationStateListener(mAppActivationStateListener);
            }
        } catch (Exception e) {
            WritableMap params = Arguments.createMap();
            params.putBoolean("success", true);
            params.putString("msg", e.getMessage());

            sendEvent(Events.EVENT_STATUS_CHANGED.toString(), params);
        }


    }


    private boolean isFlightControllerSupported() {

        return DJISDKManager.getInstance().getProduct() != null &&
                DJISDKManager.getInstance().getProduct() instanceof Aircraft &&
                ((Aircraft) DJISDKManager.getInstance().getProduct()).getFlightController() != null;

    }

    private void loginAccount() {

        UserAccountManager.getInstance().logIntoDJIUserAccount(mContext.getApplicationContext(),
                new CommonCallbacks.CompletionCallbackWith<UserAccountState>() {
                    @Override
                    public void onSuccess(final UserAccountState userAccountState) {
                        Log.e("TAG", "Login Success");
                    }

                    @Override
                    public void onFailure(DJIError error) {
                        Log.e("TAG", "Login Error:" + error.getDescription());
                    }

                });
    }

    private String appActivationStateToString(AppActivationState appActivationState) {
        switch (appActivationState) {
            case NOT_SUPPORTED:
                return "NotSupported";
            case LOGIN_REQUIRED:
                return "LoginRequired";
            case ACTIVATED:
                return "Activated";
            case UNKNOWN:
                return "Unknown";
            default:
                return "Unknown";
        }
    }

    private String aircraftBindingStateToString(AircraftBindingState aircraftBindingState) {
        switch (aircraftBindingState) {
            case NOT_SUPPORTED:
                return "NotSupported";
            case INITIAL:
                return "Initial";
            case BOUND:
                return "Bound";
            case NOT_REQUIRED:
                return "NotRequired";
            case UNBOUND:
                return "Unbound";
            case UNBOUND_BUT_CANNOT_SYNC:
                return "UnboundButCannotSync";
            case UNKNOWN:
                return "Unknown";
            default:
                return "Unknown";
        }
    }

    private void sendEvent(String name, @Nullable WritableMap event) {

        mEventEmitter.receiveEvent(getId(), name, event);

    }

    private void createView(Context context) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        rootView = layoutInflater.inflate(R.layout.dji_drone_view, this, true);
        FPVWidget contentView = (FPVWidget) rootView.findViewById(R.id.fpv_widget);
        if (contentView != null) {
            contentView.setSourceCameraNameVisibility(false);
        }

    }

    @Override
    public void addView(View child) {
        super.addView(child);
        requestLayout();
    }

    @Override
    public void requestLayout() {
        super.requestLayout();
        post(measureAndLayout);
    }

    public enum Events {
        EVENT_DOWNLOAD_PROGRESS("onDownloadProgress"),
        EVENT_STATUS_CHANGED("onStateChanged"),
        EVENT_TELEMETRY_UPDATED("onTelemetryUpdated");
        private final String mName;

        Events(final String name) {
            mName = name;
        }

        @Override
        public String toString() {
            return mName;
        }
    }

    @Override
    public void onHostDestroy() {
        destroy();
    }

    @Override
    public void onHostResume() {}

    @Override
    public void onHostPause() {}

}


