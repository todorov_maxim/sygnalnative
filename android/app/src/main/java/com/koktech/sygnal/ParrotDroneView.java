package com.koktech.sygnal;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import com.facebook.react.ReactActivity;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.parrot.drone.groundsdk.GroundSdk;
import com.parrot.drone.groundsdk.Ref;
import com.parrot.drone.groundsdk.device.DeviceState;
import com.parrot.drone.groundsdk.device.Drone;
import com.parrot.drone.groundsdk.device.instrument.Gps;
import com.parrot.drone.groundsdk.device.instrument.Speedometer;
import com.parrot.drone.groundsdk.device.peripheral.StreamServer;
import com.parrot.drone.groundsdk.device.peripheral.stream.CameraLive;
import com.parrot.drone.groundsdk.device.pilotingitf.Activable;
import com.parrot.drone.groundsdk.device.pilotingitf.ManualCopterPilotingItf;
import com.parrot.drone.groundsdk.device.pilotingitf.ReturnHomePilotingItf;
import com.parrot.drone.groundsdk.facility.AutoConnection;
import com.parrot.drone.groundsdk.stream.GsdkStreamView;


public class ParrotDroneView extends GsdkStreamView implements LifecycleEventListener {

    private final Runnable measureAndLayout = () -> {
        measure(
                MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.EXACTLY));
        layout(getLeft(), getTop(), getRight(), getBottom());
    };
    public RCTEventEmitter mEventEmitter;
    private ThemedReactContext mContext;
    private ReactActivity mActivity;
    private GroundSdk groundSdk;

    private Ref<AutoConnection> autoConnectionRef;
    private Ref<Gps> gpsRef;
    private Ref<Speedometer> speedometerRef;
    private Ref<StreamServer> streamServerRef;
    private Ref<CameraLive> liveStreamRef;
    private Ref<DeviceState> droneStateRef;
    private Ref<ManualCopterPilotingItf> pilotingItfRef;
    private Ref<ReturnHomePilotingItf> mReturnHomeItf;

    private CameraLive liveStream;
    private Drone drone;

    private DeviceState.ConnectionState  connectionState;


    public ParrotDroneView(ThemedReactContext context, ReactActivity activity) {
        super(context);
        mContext = context;
        groundSdk = MainActivity.getGroundSdk();
        mContext.addLifecycleEventListener(this);
        mActivity = activity;
        startConnection();
    }

    public void startConnection() {


        autoConnectionRef = groundSdk.getFacility(AutoConnection.class, autoConnection -> {

            if (autoConnection.getStatus() != AutoConnection.Status.STARTED) {
                autoConnection.start();
            }


            // If the drone has changed.
            if (drone == null || (autoConnection.getDrone() != null && autoConnection.getDrone().getUid().equals(drone.getUid()) )    ) {
                if (drone != null) {
                    // Stop monitoring the old drone.
                    stopDroneMonitors();
                }

                // Monitor the new drone.
                drone = autoConnection.getDrone();
                if (drone != null) {

                    if (drone.getModel() != null) {
                        WritableMap params = Arguments.createMap();
                        params.putBoolean("success", true);
                        params.putString("model", drone.getModel().toString());
                        params.putString("state", connectionStateToString(drone.getState().getConnectionState()));
                        sendEvent(Events.EVENT_DRONE_FOUND.toString(), params);
                    }

                    startDroneMonitors();
                } else {
                    WritableMap params = Arguments.createMap();
                    params.putBoolean("success", false);
                    params.putString("model", "");
                    params.putString("state", "DISCONNECTED");
                    sendEvent(Events.EVENT_DRONE_FOUND.toString(), params);
                }
            }
        });
    }

    public void stopDroneMonitors() {
        try {


            if (droneStateRef != null) {
                droneStateRef.close();
                droneStateRef = null;
            }


            if (gpsRef != null) {
                gpsRef.close();
                gpsRef = null;

            }

            if (speedometerRef != null) {
                speedometerRef.close();
                speedometerRef = null;
            }


            if (streamServerRef != null) {
                streamServerRef.close();
                streamServerRef = null;
            }


            if (liveStreamRef != null) {
                liveStreamRef.close();
                liveStreamRef = null;
                liveStream = null;
            }


            if (pilotingItfRef != null) {
                pilotingItfRef.close();
                pilotingItfRef = null;
            }

            if (mReturnHomeItf != null) {
                mReturnHomeItf.close();
                mReturnHomeItf = null;
            }

        } catch(Exception e) {
            Log.d("PARROT_DRONE_VIEW",e.getMessage());
        }

    }

    public void startDroneMonitors() {


        mReturnHomeItf = drone.getPilotingItf(ReturnHomePilotingItf.class, it -> {
            if (it == null) {
                return;
            }

              Activable.State state =  it.getState();

              if (state == state.ACTIVE) {

                  sendEvent(Events.EVENT_ON_RETURN_HOME.toString(), null);

            } else if (state == state.IDLE) {
                  sendEvent(Events.EVENT_RETURN_HOME_IDLE.toString(), null);
              }

        });


        pilotingItfRef = drone.getPilotingItf(ManualCopterPilotingItf.class, it -> {

            if (it == null) {

                return;

            }

            ManualCopterPilotingItf.SmartTakeOffLandAction btnAction = it.getSmartTakeOffLandAction();

            if (btnAction == null) return;

            switch (btnAction) {
                case TAKE_OFF:
                    sendEvent(Events.EVENT_SHOW_TAKEOFF_BUTTON.toString(), null);
                    break;
                case THROWN_TAKE_OFF:
                    sendEvent(Events.EVENT_SHOW_THROW_TAKEOFF.toString(), null);
                    break;
                case LAND:
                    sendEvent(Events.EVENT_SHOW_LAND_BUTTON.toString(), null);
                case NONE:
                default:

                    break;
            }


        });



        droneStateRef = drone.getState(obj -> {
            if (obj == null) return;
            WritableMap params = Arguments.createMap();

            if (obj.getConnectionState() == connectionState) return;

            connectionState = obj.getConnectionState();
            params.putBoolean("success", true);
            params.putString("model", drone.getModel().toString());
            params.putString("state", connectionStateToString(obj.getConnectionState()));
            sendEvent(Events.EVENT_DRONE_CONNECTED.toString(), params);
        });

        gpsRef = drone.getInstrument(Gps.class, obj -> {
            if (obj == null) return;
            if (obj.lastKnownLocation() != null) {

                WritableMap params = Arguments.createMap();
                params.putBoolean("success", true);
                params.putDouble("latitude", obj.lastKnownLocation().getLatitude());
                params.putDouble("longitude", obj.lastKnownLocation().getLongitude());
                params.putDouble("absoluteAltitude", obj.lastKnownLocation().getAltitude());
                sendEvent(Events.EVENT_LOCATION_UPDATE.toString(), params);

            }

        });

        speedometerRef = drone.getInstrument(Speedometer.class, obj -> {
            if (obj == null) return;
            WritableMap params = Arguments.createMap();
            params.putBoolean("success", true);
            params.putDouble("rightSpeed", obj.getRightSpeed());
            params.putDouble("downSpeed", obj.getDownSpeed());
            params.putDouble("forwardSpeed", obj.getForwardSpeed());
            sendEvent(Events.EVENT_SPEED_UPDATE.toString(), params);
        });

        startVideoStream();

    }


    private String connectionStateToString(DeviceState.ConnectionState connectionState) {
        switch (connectionState) {
            case DISCONNECTED:
                return "DISCONNECTED";
            case CONNECTING:
                return "CONNECTING";
            case CONNECTED:
                return "CONNECTED";
            case DISCONNECTING:
                return "DISCONNECTING";
            default:
                return "Unknown";
        }
    }

    public void smartTakeOffLand(String action) {
        if (drone == null || groundSdk == null) return;
        if (pilotingItfRef == null && pilotingItfRef.get() == null) return;

        pilotingItfRef.get().smartTakeOffLand();

    }

    public void returnHome(boolean value) {
        if (drone == null || groundSdk == null) return;
        if (mReturnHomeItf == null && mReturnHomeItf.get() == null) return;
            if (value) {
                mReturnHomeItf.get().activate();
                sendEvent(Events.EVENT_ON_RETURN_HOME.toString(), null);
            } else {
                mReturnHomeItf.get().deactivate();
                sendEvent(Events.EVENT_RETURN_HOME_IDLE.toString(), null);
            }

    }


    private void sendEvent(
            String eventName,
            WritableMap params) {
        mContext.getJSModule(RCTEventEmitter.class).receiveEvent(getId(), eventName, params);
    }

    private void startVideoStream() {
        // Monitor the stream server.
        streamServerRef = drone.getPeripheral(StreamServer.class, streamServer -> {
            // Called when the stream server is available and when it changes.

            if (streamServer != null) {
                // Enable Streaming
                if (!streamServer.streamingEnabled()) {
                    streamServer.enableStreaming(true);
                }

                // Monitor the live stream.
                if (liveStreamRef == null) {
                    liveStreamRef = streamServer.live(liveStream -> {
                        // Called when the live stream is available and when it changes.

                        if (liveStream != null) {
                            if (this.liveStream == null) {
                                // It is a new live stream.

                                // Set the live stream as the stream
                                // to be render by the stream view.
                                this.setStream(liveStream);
                            }

                            // Play the live stream.
                            if (liveStream.playState() != CameraLive.PlayState.PLAYING) {
                                liveStream.play();
                            }
                        } else {
                            // Stop rendering the stream
                            this.setStream(null);
                        }
                        // Keep the live stream to know if it is a new one or not.
                        this.liveStream = liveStream;
                    });
                }
            } else {
                // Stop monitoring the live stream
                liveStreamRef.close();
                liveStreamRef = null;
                // Stop rendering the stream
                this.setStream(null);
            }
        });
    }


    @Override
    public void addView(View child) {
        super.addView(child);
        requestLayout();
    }

    @Override
    public void requestLayout() {
        super.requestLayout();
        post(measureAndLayout);
    }

    @Override
    public void onHostDestroy() {

    }

    @Override
    public void onHostResume() {
        if (drone == null) {
            startConnection();
        }
    }

    @Override
    public void onHostPause() {

    }

    public enum Events {
        EVENT_DRONE_FOUND("onDroneFound"),
        EVENT_DRONE_CONNECTED("onDroneConnected"),
        EVENT_SPEED_UPDATE("onSpeedChanged"),
        EVENT_LOCATION_UPDATE("onLocationChanged"),
        EVENT_SHOW_TAKEOFF_BUTTON("onDroneLanded"),
        EVENT_SHOW_LAND_BUTTON("onDroneFlying"),
        EVENT_SHOW_THROW_TAKEOFF("onThrowTakeoff"),
        EVENT_ON_RETURN_HOME("onReturnHome"),
        EVENT_RETURN_HOME_IDLE("onReturnHomeIdle");
        private final String mName;

        Events(final String name) {
            mName = name;
        }

        @Override
        public String toString() {
            return mName;
        }
    }


}
