package com.koktech.sygnal;

import android.app.Application;
import android.content.Context;

public class MainApplication extends Application {



    private Application instance;
    public void setContext(Application application) {
        instance = application;
    }

    @Override
    public Context getApplicationContext() {
        return instance;
    }

    public MainApplication() {

    }



    @Override
    public void onCreate() {
        super.onCreate();

    }

}
