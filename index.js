/**
 * @format
 */

import React from 'react';
import { AppRegistry, StatusBar, Platform, StyleSheet } from 'react-native';
import App from './src/Components/App';
import { name as appName } from './app.json';

import { View } from 'react-native';

// imports Provider and store
import { Provider } from 'mobx-react';
import store from './src/Stores/Store';
import { StyleProvider } from 'native-base';
import getTheme from './native-base-theme/components';
import material from './native-base-theme/variables/material';

const Main = () => {
  return (
    <View
      style={styles.view}>
      <View
        style={styles.statusBarView}>
        <StatusBar
          translucent
          backgroundColor="#0c5b7d"
          barStyle="light-content"
        />
      </View>
      <View
        style={styles.appView}>
        <Provider store={store}>
          <StyleProvider style={getTheme(material)}>
            <App />
          </StyleProvider>
        </Provider>
      </View>
    </View>
  );
};

const styles= StyleSheet.create({
  view: {
    flex: 1,
    flexDirection: 'row'
  },
  statusBarView: {
    backgroundColor: '#0c5b7d',
    height: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,
  },
  appView: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
  }
});

AppRegistry.registerComponent(appName, () => Main);
