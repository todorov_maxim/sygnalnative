# SygnalNative

SygnalApp is built using React Native.

The product features are prioritised by the Product Management team.

Note: Make use of apks ( arm64-v8a ) for releasing to the Google Store. DJI sdk seems to not like .aab

## A quote to remember

“Any fool can write code that a computer can understand. Good programmers write code that humans can understand.” – Martin Fowler

## Getting Started

Setting up Android, iOS and React Native can be done in parallel.

### Android
Install Java.
Install Android SDK.
Open Android SDK, install Gradle, Developer Tools, Google Play Services, Android Images etc.

### iOS
Install Xcode.
Install CocoaPods.

### React Native
Install NPM
Download a Javascript Editor or IDE such as Visual Studio Code.
Setting up an Android Simulator

## Debugging

### iOS

- Press `Cmd + D` to open the Developer Tools.

### Android

- Press `Cmd + M` to open the Developer Tools.

### Device Size & Orientation

Portrait Phones.
Portrait Phone View in Tablets.


## Special Thanks
- Steve - Product Owner
- Siddhartha Baral - Software Engineer
- Ammar Ahmed - Software Engineer
